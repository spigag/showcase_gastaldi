alter table slide 
add column alternative_file text;

insert into metabase.dictionary (word, fixed_word, translations)
values ('SLIDE_ALTERNATIVE_FILE', true, '{ "text": "File associato all''immagine", "text_en": "File linked to image" }')


-- Function: banner_update_slide(bigint, json)
DROP FUNCTION banner_update_slide(bigint, json);

CREATE OR REPLACE FUNCTION banner_update_slide(
    _user_id bigint,
    _json json)
  RETURNS bigint AS
$BODY$
BEGIN 
  update slide
  set   
    image = _json->>'image',
    text = cast(_json->>'text' as json),
    position = cast(_json->>'position' as integer),
    background = _json->>'background',
    text_color = _json->>'text_color',
    image_alignment = cast(_json->>'image_alignment' as integer),
    text_alignment = cast(_json->>'text_alignment' as integer),
    slide_href = cast(_json->>'slide_href' as text),    
    alternative_file = cast(_json->>'alternative_file' as text)
    
  where 
    id = cast(_json->>'id' as bigint);
    
  RETURN cast(_json->>'id' as bigint);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION banner_update_slide(bigint, json)
  OWNER TO moxadmin;

-- Function: banner_get_slides(bigint, bigint, bigint)

DROP FUNCTION banner_get_slides(bigint, bigint, bigint);

CREATE OR REPLACE FUNCTION banner_get_slides(
    IN _user_id bigint,
    IN _banner_id bigint,
    IN _slide_id bigint)
  RETURNS TABLE(id bigint, banner_id bigint, image character varying, text json, "position" integer, background text, text_color text, image_alignment integer, text_alignment integer, slide_href text, alternative_file text) AS
$BODY$
DECLARE
  _user_is_super_admin  boolean;
  _config json;
BEGIN


    select value into _config from settings where module = 'localjs' and name = 'images';

    RETURN QUERY
        select s.id, s.banner_id, 
        (common_get_complete_image_url(s.image, 'banners', _config)),
        s.text, s.position, s.background, s.text_color, s.image_alignment, s.text_alignment, s.slide_href, s.alternative_file
        from slide s
        where (_banner_id is not null and s.banner_id = _banner_id )
            or (_slide_id is not null and s.id = _slide_id)
        order by position;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION banner_get_slides(bigint, bigint, bigint)
  OWNER TO moxadmin;


-- Function: public_homepage_find_one(bigint, json)

-- DROP FUNCTION public_homepage_find_one(bigint, json);

CREATE OR REPLACE FUNCTION public_homepage_find_one(
    _user_id bigint,
    _config json)
  RETURNS json AS
$BODY$
DECLARE 
  _result json;
  _culture_code character varying;
  _culture_id bigint;
BEGIN

  SELECT COALESCE(C.culture_code,'BASE'),USR.culture_id INTO _culture_code, _culture_id
  FROM site_user USR
  LEFT OUTER JOIN culture C on C.culture_id = USR.culture_id
  WHERE USR.user_id = _user_id;

  SELECT row_to_json(res)  INTO _result FROM (
    SELECT 
      h.top_categories_label,
      h.top_products_label,
      h.promo_products_label,
      vl.vl_name,
      vl.vl_description,
      (select array_to_json(array_agg(row_to_json(r))) as banner from (
        SELECT 
          B.id AS banner_id
          ,B.timer * 1000 as timer
          ,B.height
          ,(select common_get_complete_image_url from common_get_complete_image_url(S.image, 'banners', _config)) as slide_image
          ,S.background
          ,S.text_color
          ,S.image_alignment
          ,S.text_alignment
          ,metabase.extract_culturized_values(S.text,LOWER(_culture_code)) AS slide_text
          ,S.slide_href
          ,S.alternative_file
         ,S.position
        FROM banner B
          INNER JOIN slide S ON B.id = S.banner_id
        WHERE B.published_to_frontpage = true
        ORDER BY S.position ASC
      ) r)
    FROM homepage h
    INNER JOIN viewelement_localized vl ON vl.viewelement_id = h.viewelement_id
    WHERE vl.culture_id = _culture_id
  ) res;
	RETURN _result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public_homepage_find_one(bigint, json)
  OWNER TO moxadmin;
