﻿DO $$ 
DECLARE _view_id bigint;
BEGIN	
	if(select count(*) = 0 from documenttype where documenttype_code = 'ON') then
		insert into viewelement (last_edit_date, inserted_date, private)
		values (current_timestamp, current_timestamp, 0) 
		returning viewelement_id into _view_id;

		insert into viewelement_localized (viewelement_id, culture_id, vl_name, view_order, last_edit_date, inserted_date)
		values (_view_id, 1, 'Ordine SRL', 0, current_timestamp, current_timestamp); 

		insert into documenttype 
		(documenttype_code, is_creatable, viewelement_id, visibility_status, last_edit_date, inserted_date, purge_status, support_versioning, 
		state_closed, state_exported, state_open, support_contacts, report_template, affected_stock_id)
		values ('ON', 1, _view_id, 0, current_timestamp, current_timestamp, 0, 0, 2, 3, 1, 0, 'order-report.tpl.html', 3);
	else
		raise notice 'Document type <ON> already exists.';
	end if;
END;
$$;

-- Function: public_save_order_checkout_document(bigint, json)

-- DROP FUNCTION public_save_order_checkout_document(bigint, json);

CREATE OR REPLACE FUNCTION public_save_order_checkout_document(
    _user_id bigint,
    _json json)
  RETURNS bigint AS
$BODY$
DECLARE
	_cart_id		bigint;
	_cart_documenttype_id	bigint;
	_pending_document_id	bigint;
	
	_document_id 		bigint;
	_gamma_customer_flag	integer;	-- custom GastalDI
	_documenttype_id 	bigint;
	_username 		character varying;
	r			record;
	_count 			integer;
	_open_state_id 		bigint;
	_rejected_state_id 	bigint;
	_deliverytype_code	bigint;
	_ddt_id			bigint;
	_ship_date_required	integer; 

	_default_bankaccount_id bigint;
	_default_pricelist_number  character varying;
	_bank_id                bigint;
	
BEGIN
	select cart_documenttype_id into _cart_documenttype_id
	from firm limit 1;
	
	select open_state_id into _open_state_id
	from ecommerce_rules;
	
	select u.username into _username
	from site_user u
	where u.user_id = _user_id;

	_ship_date_required := 0;
	if(cast(_json->>'ship_date' as timestamp) is not null) then
		_ship_date_required := 1;
	end if;  
	-- ricavo la banca, listino del cliente
	select ae.default_bankaccount_id, c.default_pricelist_number into _default_bankaccount_id, _default_pricelist_number
	from addresselement ae
		inner join customer c on ae.addresselement_id = c.addresselement_id
	where c.customer_id = trim(both '"' from _json->>'customer_id')::bigint;

	select ba.bank_id into _bank_id
	from bankaccount ba 
	where ba.bankaccount_id = _default_bankaccount_id;

	select d.document_id into _cart_id
	from document d
	where d.documenttype_id = _cart_documenttype_id and d.user_id = _user_id;

	-- cerco eventuali documenti rimasti in sospeso per il carrello dell'utente corrente
	select document_id into _pending_document_id 
	from document 
	where generated_by = _cart_id;

	
	-- customization GASTALDI: nel caso in cui il cliente sia della seconda ditta (flag a 1 in tabella gamma.cg44_clifor) va generato un documento di tipo SRL
	select split_part(customer_code,'-',1)::integer into _gamma_customer_flag from customer where customer_id = trim(both '"' from _json->>'customer_id')::bigint;

	--_gamma_customer_flag = 3
	select order_documenttype_id into _documenttype_id
	from ecommerce_rules;
	if(_gamma_customer_flag is not null and _gamma_customer_flag = 1) then 
		select documenttype_id into _documenttype_id
		from documenttype
		where documenttype_code = 'ON';
	end if;
	
	-- non vi sono documenti in sospeso per l'utente corrente: procedo con la generazione del nuovo documento
	if(_pending_document_id is null) then
		insert into document (
			documenttype_id,
			document_number, 	-- come lo creo?	TODO
			document_date,
			state_id,
			customer_id, 
			currency_id,
			deliverytype_id,
			paymenttype_id,
			billing_address_id,
			shipping_address_id,
			bank_id,
			bankaccount_id,
			pricelist_number,
			urgent,
			ship_date,
			ship_date_required,
			notes,
			last_modified_date,
			last_edit_date,
			inserted_date,
			username,
			user_id,
			generated_by
		) values (
			_documenttype_id,
			(case when (_gamma_customer_flag = 1)
				then 
					'ON-' || cast((    select coalesce(max(document_id) + 1, 1) from document) as varchar)
				else 
					'ORC-' || cast((    select coalesce(max(document_id) + 1, 1) from document) as varchar)
			end
			),
			current_timestamp,
			_open_state_id,
			trim(both '"' from _json->>'customer_id')::bigint,
			1,							-- per la currency, probabilemtne il servizio mi tornerà anche l'id, verificare, intanto setto 1
			trim(both '"' from _json->>'deliverytype_id')::bigint,
			trim(both '"' from _json->>'paymenttype_id')::bigint,
			trim(both '"' from _json->>'billing_address_id')::bigint,
			trim(both '"' from _json->>'shipping_address_id')::bigint,
			_bank_id,
			_default_bankaccount_id,
			_default_pricelist_number,
			0,
			cast(_json->>'ship_date' as timestamp),	
			_ship_date_required,	
			cast(_json->>'notes' as text),
			current_timestamp,
			current_timestamp,
			current_timestamp,
			_username,
			_user_id,
			_cart_id	
		) returning document_id into _document_id;

		_count := 1;
	else
		-- esiste un documento in sospeso per il carrello dell'utente corrente: recupero il documento e lo aggiorno
		_document_id := _pending_document_id;
		-- "pulisco" la tabella documentdetail dalle righe precedenti che saranno rigenerate da capo più sotto
		delete from documentdetail where document_id = _pending_document_id;

		update document
		set 	document_date = current_timestamp,
			state_id = _open_state_id,
			customer_id = trim(both '"' from _json->>'customer_id')::bigint,
			deliverytype_id = trim(both '"' from _json->>'deliverytype_id')::bigint,
			paymenttype_id = trim(both '"' from _json->>'paymenttype_id')::bigint,
			billing_address_id = trim(both '"' from _json->>'billing_address_id')::bigint,
			shipping_address_id = trim(both '"' from _json->>'shipping_address_id')::bigint,
			bank_id = _bank_id,
			bankaccount_id = _default_bankaccount_id,
			pricelist_number = _default_pricelist_number,
			ship_date = cast(_json->>'ship_date' as timestamp),
			ship_date_required = _ship_date_required,
			notes = cast(_json->>'notes' as text),
			last_modified_date = current_timestamp,
			last_edit_date = current_timestamp,
			inserted_date = current_timestamp,
			username = _username,
			user_id = _user_id,
			generated_by = _cart_id
		where document_id = _document_id;
	end if;

	for r in (select * from json_array_elements((_json->>'products_list')::json))
	loop
		update document 
		set currency_id = trim(both '"' from ((r.value->>'priceDetail')::json)->>'currencyID')::bigint
		where document_id = _document_id;
		
		insert into documentdetail(
			document_id,
			product_id, 
			insert_order,
			quantity,
			shipped_quantity,
			um_id,
			discount,
			discount_string,
			price,
			custom_discount,
			customprice,
			is_free,
			original_discount,
			original_price,
			row_status,
			row_type,
			charge,
			last_edit_date,
			inserted_date,
			purge_status,
			username,
			user_id,
			tax_id,
			outline_number,
			outline_level,
			outline_order,
			ship_date
		)
		values (
			_document_id,
			trim(both '"' from r.value->>'id')::bigint,
			_count,
			trim(both '"' from r.value->>'quantity')::integer,
			0.0,											-- shipped_quantity
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'umID')::bigint,		-- um_id
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'discount')::numeric,		-- discount
			((r.value->>'priceDetail')::json)->>'discountString',	-- discount_string
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'grossUnitPrice')::numeric,	-- price
			null,											-- custom_discount
			null,											-- customprice
			null,											-- is_free
			null,											-- original_discount
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'grossUnitPrice')::numeric,
			0,
			0,
			0.0,
			current_timestamp,
			current_timestamp,
			0,
			_username,
			_user_id,
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'taxID')::bigint,
			(r.value->>'outline_number'),
			cast(r.value->>'outline_level' as integer),
			cast((r.value->>'outline_order') as integer),
			cast(r.value->>'ship_date' as timestamp)
		);
		_count := _count + 1;
	end loop;
	
	for r in (select * from json_array_elements((_json->>'mandatories')::json))
	loop
		insert into documentdetail(
			document_id,
			product_id, 
			insert_order,
			quantity,
			shipped_quantity,
			um_id,
			discount,
			discount_string,
			price,
			custom_discount,
			customprice,
			is_free,
			original_discount,
			original_price,
			row_status,
			row_type,
			charge,
			last_edit_date,
			inserted_date,
			purge_status,
			username,
			user_id,
			tax_id,
			outline_number,
			outline_level,
			outline_order
		)
		values (
			_document_id,
			trim(both '"' from r.value->>'id')::bigint,
			_count,
			trim(both '"' from r.value->>'quantity')::integer,
			0.0,											-- shipped_quantity
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'umID')::bigint,		-- um_id
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'discount')::numeric,		-- discount
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'discountString')::varchar,	-- discount_string
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'grossUnitPrice')::numeric,	-- price
			null,											-- custom_discount
			null,											-- customprice
			null,											-- is_free
			null,											-- original_discount
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'grossUnitPrice')::numeric,
			0,
			109, -- row_type per prodotti obbligatori
			0.0,
			current_timestamp,
			current_timestamp,
			0,
			_username,
			_user_id,
			trim(both '"' from ((r.value->>'priceDetail')::json)->>'taxID')::bigint,
			(r.value->>'outline_number'),
			cast(r.value->>'outline_level' as integer),
			cast((r.value->>'outline_order') as integer)		
		);

		_count := _count + 1;
	end loop;
	
	-- se il deliverytype corrisponde al corriere espresso aggiungo la riga corrispondente alle spese trasport
	select deliverytype_code into _deliverytype_code
	from deliverytype
	where deliverytype_id = trim(both '"' from _json->>'deliverytype_id')::bigint;

	select documentdescriptiontype_id into _ddt_id
	from documentdescriptiontype
	where ddt_code = 'ST';
	
	return _document_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public_save_order_checkout_document(bigint, json)
  OWNER TO moxadmin;
GRANT EXECUTE ON FUNCTION public_save_order_checkout_document(bigint, json) TO moxadmin;
REVOKE ALL ON FUNCTION public_save_order_checkout_document(bigint, json) FROM public;
COMMENT ON FUNCTION public_save_order_checkout_document(bigint, json) IS 'CUSTOMIZATION OF GastaldiGomme';






