/*
insert into functionality 
values (default, 'public.show_agent_references','Permit to view the agent references from the public site header (custoemrs functionality)') 
*/

create schema if not exists gastaldi;
ALTER SCHEMA gastaldi OWNER TO moxadmin;





CREATE TABLE gastaldi.imported_product (
    imported_product_id bigserial,
    sdk_excel_import_id int8 NOT NULL,
    product_code varchar NOT NULL,
    cai varchar NULL,
    ean varchar NULL,
    cai_2 varchar NULL,
    description varchar NULL,
    invernale varchar NULL,
    status varchar NULL,
    giac0306 varchar NULL,
    disp0306 varchar NULL,
    b_o varchar NULL,
    famiglia varchar NULL,
    s_famiglia varchar NULL,
    CONSTRAINT gastaldi_imported_product_pk PRIMARY KEY (imported_product_id),
    CONSTRAINT public_sdk_excel_import_id_fk FOREIGN KEY (sdk_excel_import_id) REFERENCES sdk_excel_import(sdk_excel_import_id)
)
WITH (
    OIDS=FALSE
) ;



CREATE OR REPLACE FUNCTION gastaldi.get_facets(_user_id bigint)
  RETURNS SETOF json AS
$BODY$
DECLARE _culture_id bigint;
DECLARE _culture_code character varying;
DECLARE _division_id bigint;
BEGIN
    --get user culture code
    SELECT 
        COALESCE(USR.culture_id,1)
        ,COALESCE(C.culture_code,'BASE')
    INTO _culture_id, _culture_code
    FROM site_user USR
        LEFT OUTER JOIN culture C on C.culture_id = USR.culture_id
    WHERE USR.user_id = _user_id;

    --get division id
    SELECT division_id INTO _division_id 
    FROM division d 
    WHERE d.division_code = 'GASTALDI';

    --Get facets 
    RETURN QUERY
      SELECT row_to_json(r) from (
    SELECT f.field_id, f.facet_text, mf.code, json_agg(f) AS facets
    FROM public_get_facets(_user_id, _division_id, -1,'','') f
      inner join metabase.field mf on f.field_id = mf.id 
    GROUP BY f.field_id, f.facet_text, mf.code
      ) r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION gastaldi.get_facets(bigint)
  OWNER TO moxadmin;
-- Function: gastaldi.public_get_order_document_detail(bigint, bigint, json, bigint)

-- DROP FUNCTION gastaldi.public_get_order_document_detail(bigint, bigint, json, bigint);

CREATE OR REPLACE FUNCTION gastaldi.public_get_order_document_detail(
    _user_id bigint,
    _document_id bigint,
    _config json,
    _doc_culture bigint)
  RETURNS json AS
$BODY$
DECLARE
  _culture_id   bigint;
  _customer_id  bigint;
  _result   json;
BEGIN

-- parametro opzionale che prende la precedenza sulla cultura dell'utente nel caso sia specificato
if (_doc_culture > 0) then
  _culture_id = _doc_culture;
else 
  select u.culture_id into _culture_id
  from site_user u
  where u.user_id = _user_id;
end if;

select u.customer_id into _customer_id
from site_user u
where u.user_id = _user_id;

SELECT array_to_json(array_agg(row_to_json(r))) into _result from (
  select  dt.*,
    to_char(dt.ship_date, 'dd/MM/YYYY') as formatted_ship_date,
    (select row_to_json(p) from (
      select p.*,
        cpa.product_code_alias,
      coalesce(pvl.vl_name, pbvl.vl_name) as product_name,
      coalesce(pvl.vl_description, pbvl.vl_description) as product_description,
      cast(common_get_complete_image_url(
        (case when coalesce(pvl.image_url, pbvl.image_url) is not null 
        then coalesce(pvl.image_url, pbvl.image_url)
        else 
          (
            select coalesce(l1.image_url, l2.image_url, '')
            from product p1 
            left join viewelement_localized l1 on p1.viewelement_id = l1.viewelement_id and l1.culture_id in (_culture_id)
            left join viewelement_localized l2 on p1.viewelement_id = l2.viewelement_id and l2.culture_id in (1)
            where p1.product_id = p.parent_product_id limit 1
          )
        end), 'list', _config) 
      as varchar) image_url
    ) p) as product,   
    (select row_to_json(u) from (
      select um.*,
      coalesce(umvl.vl_name, umbvl.vl_name) as vl_name,
      coalesce(umvl.vl_description, umbvl.vl_description) as vl_description
    ) u) as um
     ,(dt.quantity * (dt.net_unit_price + coalesce(dt.disposal_price, 0))) as total_row_price
  from documentdetail dt
    inner join um um on dt.um_id = um.um_id
    left outer join viewelement_localized umvl on um.viewelement_id = umvl.viewelement_id and umvl.culture_id = _culture_id
    left outer join viewelement_localized umbvl on um.viewelement_id = umbvl.viewelement_id and umbvl.culture_id = 1
    inner join product p on p.product_id = dt.product_id
    left outer join viewelement_localized pvl on p.viewelement_id = pvl.viewelement_id and pvl.culture_id = _culture_id
    left outer join viewelement_localized pbvl on p.viewelement_id = pbvl.viewelement_id and pbvl.culture_id = 1
    left outer join customer_product_alias cpa on p.product_id = cpa.product_id and cpa.customer_id = _customer_id
  where dt.document_id = _document_id
  order by (array_to_json(string_to_array(dt.outline_number, '.'))::json->>0)::integer,
    coalesce((array_to_json(string_to_array(dt.outline_number, '.'))::json->>1)::integer, -1), dt.insert_order
) r;

return _result;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gastaldi.public_get_order_document_detail(bigint, bigint, json, bigint)
  OWNER TO moxadmin;


CREATE OR REPLACE FUNCTION gastaldi.search_products(
    IN _user_id bigint,
    IN _search text,
    IN _stock_filter boolean,
    IN _facet_filter text,
    IN _page_index integer,
    IN _page_size integer,
    IN _config json)
  RETURNS TABLE(id bigint, code character varying, name character varying, image character varying, zoom_image character varying, custombuy_template character varying, dataschema_id bigint, tyre json, commercial_data json, brand character varying, brand_image character varying, public_price numeric, discount numeric, number_of_records bigint) AS
$BODY$
DECLARE
  _culture_id           bigint;
  _culture_code           text;
  _customer_id          bigint;
  _base_url           character varying;
  _base_cult_id       bigint;
  _query              text;
  _entity_type_id     bigint;
  
  _pricelists         bigint[];
  _public_price_query   text;
  _public_pricelist_number  varchar;
  _public_pricelist_id    bigint;
  
  _division_id          bigint;
  _division_group_id        bigint;
  _node_id            bigint;
  
  _visibilityrule_inclusive     integer;
  _customer_visibility_group_id   bigint;
  _market_id      bigint;
  _user_group_id        bigint;
  _customer_discount_group_id bigint;
  
  _check_availability   boolean;
  _availability_filter      text;
  _global_stock_id        bigint;
  _limit_query          text;
BEGIN
  -- determino la cultura dell'utente
  SELECT U.culture_id, C.culture_code, U.customer_id INTO _culture_id, _culture_code, _customer_id
  FROM site_user U
  INNER JOIN culture C ON C.culture_id = U.culture_id
  WHERE U.user_id = _user_id;

  select culture_id into _base_cult_id
  from culture
  where culture_code = 'BASE';

  if(_customer_id is null) then
    select default_customer_id into _customer_id from firm limit 1;
  end if;
  
  select division_id, group_id INTO _division_id, _division_group_id FROM division where division_code = 'GASTALDI';
  _node_id := -1;

  -- recupero il flag per le regole di visibilità
  SELECT COALESCE(base_url,''), global_stock_id, public_pricelist_number, coalesce(visibilityrule_inclusive, 0) 
  INTO _base_url, _global_stock_id, _public_pricelist_number, _visibilityrule_inclusive
  FROM firm;
  
  SELECT M.market_id INTO _market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = _user_id;
  IF (_market_id IS NULL ) THEN
    _market_id = -1;
  END IF;
  
  SELECT group_id INTO _user_group_id FROM user_user_group WHERE user_id = _user_id;
  if(_user_group_id is null) then 
    _user_group_id := ' -1 ';
  end if; 
  
  select cn.node_id into _customer_visibility_group_id 
  from site_user su
  inner join customer_node cn on cn.customer_id = _customer_id
  inner join node n on n.node_id = cn.node_id and n.group_id = (select group_id from groups where group_type = 'CustomerVisibilityGroup')
  where su.user_id = _user_id;

  select cn.node_id into _customer_discount_group_id 
  from customer_node cn 
  inner join node n on cn.node_id = n.node_id and n.group_id = (select group_id from groups where group_type = 'CustomerDiscount')
  where cn.customer_id = _customer_id;
  if(_customer_discount_group_id is null) then 
    _customer_discount_group_id := -1;
  end if; 

  -- determino l'id dell'entity_type
  select get_entity_type into _entity_type_id from metabase.get_entity_type('product');

  WITH customers as (
    select customer_id, default_pricelist_number from customer where customer_id = _customer_id
  ),
  pricelists as (
    select default_pricelist_number as pricelist_number from customers
    UNION
    select distinct parent_pricelist_number as pricelist_number from public.pricelist where pricelist_number in (select default_pricelist_number from customers)
  )
  select array_agg(pricelist_id) INTO _pricelists from pricelist where pricelist_number IN (
    select distinct pricelist_number from pricelists
    UNION
    select pricelist_number from customer_pricelist cp inner join customers cs on cp.customer_id = cs.customer_id
  );    

  -- notazione: se _page_size o _page_index sono negativi allora siamo nella versione stampabile nella quale vanno tornati tutti i prodotti senza limiti
  _limit_query := '';
  if(_page_size > 0 and _page_index > 0) then
    _limit_query := ' LIMIT  ' || _page_size || ' OFFSET ' || (_page_index - 1) * _page_size;
  end if;
  
  _availability_filter := '';
  if(_stock_filter) then
    _availability_filter := 'inner join stockquantity sq on P.product_id = sq.product_id and sq.incoming_date is null 
      and (sq.quantity - (
        select coalesce(sum(coalesce(dd.quantity, 0)), 0) 
        from documentdetail dd 
          inner join document d on d.document_id = dd.document_id
          inner join documentstate ds on ds.documentstate_id = d.state_id and coalesce(ds.affects_availability, 0) = 1
        where dd.product_id = P.product_id
      )) > 0 ';
  end if;

  _public_price_query := 'null::numeric as public_price';
  if(_public_pricelist_number is not null) then 
  select pricelist_id into _public_pricelist_id
  from pricelist
  where pricelist_number = _public_pricelist_number;

  if(_public_pricelist_id is not null) then 
    _public_price_query := '(case when (P.master_product = true) then
        ( 
          select pld.price
          from pricelistdetail pld
          where pld.um_id = um.um_id and pld.pricelist_id = ' || _public_pricelist_id || ' and pld.product_id = p.ref_child
          and pld.price is not null and pld.date_start <= current_timestamp and pld.date_end >= current_timestamp
          order by pld.date_start desc limit 1 
        )
       else
        (
          select pld.price
          from pricelistdetail pld
          where pld.um_id = um.um_id and pld.pricelist_id = ' || _public_pricelist_id || ' and pld.product_id = p.product_id
            and pld.date_start <= current_timestamp and pld.date_end >= current_timestamp
          order by pld.date_start desc limit 1 
        )
       end
      ) as public_price';
    end if;
  end if;
  
  IF (char_length(_search)=0 AND char_length(_facet_filter)=0) THEN 
    _query:= '
      SELECT 
        P.product_id
        ,P.product_code
        ,COALESCE(VEI.vl_name, VE.vl_name) as name
        ,(select common_get_complete_image_url from common_get_complete_image_url(COALESCE(VEI.image_url, VE.image_url), ''list'', ''' || _config || '''))
        AS image_url
        ,(select common_get_complete_image_url from common_get_complete_image_url(COALESCE(VEI.image_url, VE.image_url), ''zoom'', ''' || _config || '''))
        AS zoom_image
        ,cbm.template_name
        ,COALESCE(EDS.dataschema_id, -1) AS dataschema_id 
        ,(select array_to_json(array_agg(row_to_json(fg))) from (
          select marca,genere,settore,in_promo 
          from metadata.gomma 
          where entity_id = P.product_id
        ) fg) as gomma
        ,(select array_to_json(array_agg(row_to_json(fg))) from (
          select efficienza,aderenza,emissione,rumorosita,contributo_pfu 
          from metadata.dati_commerciali 
          where entity_id = P.product_id
        ) fg) as dati_commerciali
        ,bvl.vl_name as brand
        ,(select common_get_complete_image_url from common_get_complete_image_url(bvl.image_url, ''zoom'', ''' || _config || '''))
        ,' || _public_price_query || '
  ,(select discount 
  from pricerule 
  where customer_group_id = ' || _customer_discount_group_id || ' and product_group_id in (
    select pn.node_id
    from product_node pn 
      inner join node n on pn.node_id = n.node_id and n.group_id = (select group_id from groups where group_type = ''ProductDiscount'')
    where pn.product_id = p.product_id
  ) order by rule_level asc, date_start desc limit 1) as discount
        ,COUNT(P.product_id) OVER (range unbounded preceding) AS number_of_records
      FROM (
        SELECT P.product_id
          ,P.product_code 
          ,P.master_product
          ,P.viewelement_id
          ,P.published
          ,P.visibility_status
          ,P.umset_id
          ,P.custombuymanager_id
          ,P.brand_id
          ,P.ref_child
        FROM (
          select distinct on (P.product_id) P.product_id
            ,P.product_code 
            ,P.master_product
            ,P.viewelement_id 
            ,P.published
            ,P.visibility_status
            ,P.umset_id
            ,P.custombuymanager_id
            ,P.brand_id
      ,P.ref_child
           FROM product P ' || 
             _availability_filter || '
             INNER JOIN product_node pn on pn.product_id = p.product_id 
             INNER JOIN node N ON PN.node_id = N.node_id AND N.group_id = ' || _division_group_id || '
             /*
             INNER JOIN (
    SELECT DISTINCT ON (product_id) product_id, pricelistdetail_id
    FROM pricelistdetail
    WHERE pricelist_id IN (' || array_to_string(_pricelists,',') || ') 
    ORDER BY product_id, date_start DESC
                LIMIT 1
       ) pd ON (pd.product_id = coalesce(P.ref_child, P.product_id))*/
          WHERE   (P.published = true) AND ( (' || _node_id  || ' = -1 AND coalesce(n.parent_id, -1) = -1) OR n.node_id = ' || _node_id || ')
        ) P 
        ORDER BY P.product_code
      ) P 
      INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN (-1, ' || _market_id || ')
      INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN (-1, ' || _user_group_id || ')          
      INNER JOIN viewelement_localized VE ON VE.viewelement_id = P.viewelement_id AND VE.culture_id = ' || _base_cult_id ||'

      LEFT OUTER JOIN umset ums on ums.umset_id = p.umset_id
      LEFT OUTER JOIN um um on um.um_id = ums.default_um_id
      LEFT OUTER JOIN viewelement_localized VEI ON VEI.viewelement_id = P.viewelement_id AND VEI.culture_id = ' || _culture_id ||'
      LEFT OUTER JOIN entity_dataschema EDS ON EDS.entity_type_id = ' || _entity_type_id || ' AND EDS.entity_id=P.product_id 
      LEFT OUTER JOIN custombuymanager cbm on P.custombuymanager_id = cbm.custombuymanager_id
      left outer join brand b on P.brand_id = b.brand_id
      LEFT OUTER JOIN viewelement_localized bvl ON bvl.viewelement_id = b.viewelement_id AND bvl.culture_id = ' || _culture_id ||'  
    ORDER BY bvl.vl_name, P.product_code ' ||
    _limit_query;
  else 
    _query:= '
    WITH T AS (
            SELECT
        P.product_id,
        MAX(VE.culture_id) culture_id,
        MAX(VEI.culture_id) culture_id_image
            FROM (select public_filter_entities FROM public_filter_entities(' || _division_id || ',' || _node_id || ', ''' || _search || ''', ''' || _facet_filter || ''', ''' || _culture_code || ''' ,' || _user_id || ')) FP
        INNER JOIN product P ON P.product_id = FP.public_filter_entities
        ' || _availability_filter || '
        INNER JOIN viewelement_localized VE ON VE.viewelement_id = P.viewelement_id AND VE.culture_id IN (' || _base_cult_id || ', ' || _culture_id || ')
        INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN (-1, ' || _market_id || ')
        INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN (-1, ' || _user_group_id || ')                        
        LEFT OUTER JOIN viewelement_localized VEI ON VEI.viewelement_id = P.viewelement_id AND VEI.culture_id IN (' || _base_cult_id || ', ' || _culture_id || ') AND COALESCE(VEI.image_url,'''') <> '''' 
      WHERE coalesce(P.visibility_status, 0) <> 2
      GROUP BY P.product_id 
    ),
    pnodes as (
        select pn.product_id, pn.node_id
    from T
    inner join product_node pn on T.product_id = pn.product_id 
    inner join node n on pn.node_id = n.node_id and n.group_id = (select group_id from groups where group_type = ''ProductDiscount'')
    
   )
    SELECT 
      P.product_id
      ,P.product_code
      ,COALESCE(VE.vl_name,'''') as node_name
      ,(select common_get_complete_image_url from common_get_complete_image_url(COALESCE(VEI.image_url, VE.image_url), ''list'', ''' || _config || '''))
      AS image_url
      ,(select common_get_complete_image_url from common_get_complete_image_url(COALESCE(VEI.image_url, VE.image_url), ''zoom'', ''' || _config || '''))
      AS zoom_image
      ,cbm.template_name
      ,COALESCE(EDS.dataschema_id, -1) AS dataschema_id 
      ,(select array_to_json(array_agg(row_to_json(fg))) from (
        select marca,genere,settore,in_promo 
        from metadata.gomma 
        where entity_id = P.product_id
      ) fg) as gomma
      ,(select array_to_json(array_agg(row_to_json(fg))) from (
        select efficienza,aderenza,emissione,rumorosita,contributo_pfu 
        from metadata.dati_commerciali 
        where entity_id = P.product_id
      ) fg) as dati_commerciali
      ,bvl.vl_name as brand
      ,(select common_get_complete_image_url from common_get_complete_image_url(bvl.image_url, ''zoom'', ''' || _config || '''))
      ,' || _public_price_query || '
      ,(select discount 
  from pricerule 
  where customer_group_id = ' || _customer_discount_group_id || ' and product_group_id in (select  pnodes.node_id from pnodes where pnodes.product_id = p.product_id)
  and (product_group_id_1 is null or product_group_id_1 in (select pnodes.node_id from pnodes where pnodes.product_id = p.product_id) )
  and (product_group_id_2 is null or product_group_id_2 in (select pnodes.node_id from pnodes where pnodes.product_id = p.product_id) )
   order by rule_level asc, date_start desc limit 1) 
   as discount
      ,COUNT(P.product_id) OVER (range unbounded preceding) AS number_of_records
    FROM T
      INNER JOIN product P ON P.product_id = T.product_id ' ||
      CASE WHEN _customer_visibility_group_id IS NULL THEN '' ELSE ' inner join product_node pn on pn.product_id = p.product_id ' END || 
      CASE WHEN _customer_visibility_group_id IS NULL THEN '' ELSE CASE WHEN COALESCE(_visibilityrule_inclusive, 1) = 1 THEN 'LEFT JOIN' ELSE 'INNER JOIN' END || ' visibilityrule vr on pn.node_id = vr.product_group_id and vr.customer_group_id = ' || _customer_visibility_group_id END || '
      /*INNER JOIN (
        SELECT DISTINCT ON (product_id) product_id, pricelistdetail_id
        FROM pricelistdetail
        WHERE pricelist_id IN (' || array_to_string(_pricelists,',') || ') 
        ORDER BY product_id, date_start DESC
        LIMIT 1
       )pd ON (pd.product_id = coalesce(P.ref_child, P.product_id))*/
      INNER JOIN viewelement_localized VE ON VE.viewelement_id = P.viewelement_id AND VE.culture_id = T.culture_id
      LEFT OUTER JOIN viewelement_localized VEI ON VEI.viewelement_id = P.viewelement_id AND VEI.culture_id = T.culture_id_image
      LEFT OUTER JOIN entity_dataschema EDS ON EDS.entity_type_id = ' || _entity_type_id || ' AND EDS.entity_id=P.product_id
      LEFT OUTER JOIN custombuymanager cbm on P.custombuymanager_id = cbm.custombuymanager_id
      LEFT OUTER JOIN umset ums on ums.umset_id = p.umset_id
      LEFT OUTER JOIN um um on um.um_id = ums.default_um_id
      left outer join brand b on P.brand_id = b.brand_id
      LEFT OUTER JOIN viewelement_localized bvl ON bvl.viewelement_id = b.viewelement_id AND bvl.culture_id = ' || _culture_id ||'  
      ' ||
       CASE WHEN _customer_visibility_group_id IS NULL THEN '' ELSE CASE WHEN COALESCE(_visibilityrule_inclusive, 1) = 1 THEN ' AND vr.product_group_id IS NULL ' ELSE '' END END || '
    ORDER BY bvl.vl_name, P.product_code ' ||
    _limit_query;
  end if;

  -- raise notice '%', _query;

  RETURN QUERY
    EXECUTE _query;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION gastaldi.search_products(bigint, text, boolean, text, integer, integer, json)
  OWNER TO moxadmin;


CREATE OR REPLACE FUNCTION public_get_user_login_data(
    IN _username text)
  RETURNS SETOF json AS $BODY$
BEGIN
  RETURN QUERY
  SELECT row_to_json(r) from (
    SELECT
	s.user_id,
	c.customer_code,
	s.last_edit_date,
	s.inserted_date,
	s.culture_id,
	s.last_name,
	s.first_name,
	s.username,
	s.email,
	s.is_anonymous,
	s.active,
	s.token,
	s.tokenexpire,
	s.provider,
	s.hashed_password,
	s.salt,
	s.authtoken
	FROM site_user s
	LEFT JOIN customer c ON s.customer_id = c.customer_id 
	WHERE s.user_id = (
	select s.user_id from site_user s
	where username ilike '%admin%')
) r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public_get_user_login_data(text)
  OWNER TO moxadmin;

CREATE OR REPLACE FUNCTION gastaldi.product_extra(
    IN _user_id bigint,
    IN _product_id bigint,
    IN _config json)
  RETURNS json AS
$BODY$
DECLARE
  _result     json;
  _additional_images  json;
BEGIN 
  select array_to_json(array_agg(row_to_json(s))) from (
    select * from public_get_entity_additional_images(_user_id, ('{"product_id": ' || _product_id || '}')::json, _config)
  ) s 
  into _additional_images;

  select row_to_json(res) into _result 
  from (
    select 
      _additional_images "additionalimages"
  ) res;

  return _result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


/**********************************************************************************/
/**********************************************************************************/
/************************** CORREZIONE NECESSARIA A STANDARD **********************/
/**********************************************************************************/
/**********************************************************************************/

-- Function: public_get_facets(bigint, bigint, bigint, text, text)

DROP FUNCTION IF EXISTS public_get_facets(bigint, bigint, bigint, text, text);
DROP FUNCTION IF EXISTS public_get_facets(bigint, bigint, bigint, text, text, boolean);
CREATE OR REPLACE FUNCTION public_get_facets(
    IN _user_id bigint,
    IN _division_id bigint,
    IN _node_id bigint,
    IN _search text,
    IN _facet_filter text,
    IN _only_available boolean default false)
  RETURNS TABLE(field_id bigint, facet_id text, facet_text text, facet_value character varying, count numeric, filter_weight integer, facets_count bigint) AS
$BODY$
DECLARE _culture_id bigint;
DECLARE _culture_code character varying;
BEGIN

  --determino la cultura dell'utente
  SELECT 
    COALESCE(USR.culture_id,1)
    ,COALESCE(C.culture_code,'BASE')
  INTO _culture_id, _culture_code
  FROM site_user USR
    LEFT OUTER JOIN culture C on C.culture_id = USR.culture_id
  WHERE USR.user_id = _user_id;

  RETURN QUERY
  WITH PRD AS
  (
    SELECT public_filter_entities target_entity_id
    FROM public_filter_entities(_division_id, _node_id, _search, _facet_filter, _culture_code, _user_id, false, _only_available) 
  )
  SELECT 
  f.field_id
  , array_to_string(array_agg(f.facet_id), '|') facet_id
  , f.facet_text
  , f.facet_value
  , sum(f.count) AS count 
  , f.filter_weight AS filter_weight
  , max(f.facets_count) as facets_count
  FROM
  (
  SELECT 
    FLD.id AS field_id
    ,FLD.code as field_code
    ,F.id AS facet_id
    ,LOF.loc_text_value AS facet_text
    ,(CASE WHEN LKP.id IS NULL
    THEN U.field_value 
    ELSE LKP.description
    END)::character varying as facet_value
    ,(CASE WHEN LKP.id IS NULL
    THEN 0::int 
    ELSE LKP.sort_order
    END)::integer as sort_order
    ,F.count
    ,FW.filter_weight
    ,FLD.order_id 
    ,rank() OVER (PARTITION BY FLD.id ORDER BY F.count DESC) ranking
    ,count(FLD.id) OVER (PARTITION BY FLD.id) facets_count
  FROM 
  (
    SELECT 
      U.id
      ,count(X.entity_id )
    FROM  
      fts.product_facets_union U 
      INNER JOIN fts.product_entity_facets_xref_inv X ON U.id = X.facets_union_id
    WHERE 
      X.entity_id 
      IN 
      (
        SELECT target_entity_id  
        FROM PRD

      )
    GROUP BY U.id
    ORDER BY U.id 
    
  ) F
  INNER JOIN fts.product_facets_union U ON U.id = F.id
  INNER JOIN metabase.field FLD ON FLD.id = U.field_id
  INNER JOIN public_get_fieldname(_culture_id) LOF ON LOF.loc_id_value=FLD.id
  LEFT OUTER JOIN metabase.lookup_value LKP ON FLD.lookup_type = 1 AND LKP.lookup_id = FLD.lookup_id AND LKP.value = U.field_value AND LKP.culture_id = _culture_id
  INNER JOIN
  (
    SELECT
      FW.code
      ,MAX(FW.filter_weight) AS filter_weight
    FROM metabase.field FW
    GROUP BY FW.code
  ) FW ON FW.code = FLD.code
  -- fieldgroup visibility
  INNER JOIN metabase.field_usergroup_visibility ugv ON ugv.field_id = FLD.id AND ugv.usergroup_id IN
  (
    SELECT -1 AS usergroup_id UNION ALL
    SELECT uug.group_id FROM user_user_group uug WHERE user_id = _user_id
  )
  -- market visibility
  INNER JOIN metabase.field_market_visibility fmv ON fmv.field_id = FLD.id AND fmv.market_id IN (
    SELECT * FROM public_get_usermarket(_user_id)
  )
  ) f
  --WHERE f.ranking <= 10
  GROUP BY f.filter_weight,f.field_id, f.facet_text,f.facet_value
  ORDER BY f.filter_weight, min(f.order_id), f.facet_text, f.facet_value;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public_get_facets(bigint, bigint, bigint, text, text, boolean)
  OWNER TO postgres;


DROP FUNCTION IF EXISTS public_filter_entities(bigint, bigint, text, text, text, bigint);

CREATE OR REPLACE FUNCTION public_filter_entities(
   _division_id bigint,
   _category_id bigint,
   _search text,
   _facet_filter text,
   _culture_code text,
   _user_id bigint,
   _export boolean default false,
   _only_available boolean default false)
  RETURNS SETOF bigint AS
$BODY$
DECLARE
  _query              text;
  _product_only_child_no_filter   text;
  _product_only_child_filter      text;
  _check_availability     boolean;
  _availability_filter      text;
BEGIN
  IF (_search IS NULL) THEN 
    _search := '';
  END IF;
  IF (_facet_filter IS NULL) THEN 
    _facet_filter := '';
  END IF;

  _query := '';
  
  _product_only_child_filter := '((P.master_product = true) OR (P.parent_product_id IS NULL))'; --prodotto padre o non è child
-- nel caso in cui siamo nell'export excel i prodotti padre vanno comunque esplosi per la videata corrente, quindi "bypasso" il setting
  if(_export = true or ((select value from settings where settings.name ilike 'catalog.show_only_child') = 'true')) then
  _product_only_child_filter := '((P.master_product = false) OR (P.parent_product_id IS NOT NULL))'; --non è prodotto padre o è un child
  end if;

  select case when coalesce(s.value, 'false') = 'true' then false else true end into _check_availability
  from settings s
  where s.module = 'publicsite' and s.name = 'catalog.show_not_available';

  --filtro di default
  _availability_filter := 'inner join stockquantity sq on P.product_id = sq.product_id and sq.incoming_date is null and sq.quantity > 0';
  if(_only_available is not null) then  
  if(_only_available = false) then
    _availability_filter := ' ';
  end if;
  else 
  if(coalesce(_check_availability, false) = false) then
    _availability_filter := ' ';
  end if;
  end if;
  
  IF (char_length(_search)=0 AND char_length(_facet_filter)=0) THEN
    -- restituisco solo i prodotti visibili che sono collegati ad una categoria che a sua volta Ã¨ collegata ad una divisione
    -- prodotti non collegati a categorie.. o collegati a categorie non appartenenti a nessuna divisione non vengono restituiti
    -- ricerco solo per nodo
      _query :=
        'SELECT DISTINCT P.product_id
        FROM product P
    INNER JOIN product_node PN ON PN.product_id = P.product_id
    ' || _availability_filter || '
    INNER JOIN node N ON PN.node_id = N.node_id
    INNER JOIN division D ON d.group_id = n.group_id 
    INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
    INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      )
        WHERE (P.published = true) 
          AND ' || _product_only_child_filter || '
          AND D.division_id = ' || _division_id || ' 
          AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')';
  ELSEIF (char_length(_search)>0 AND char_length(_facet_filter)=0) THEN
    -- ricerco per nodo e ricerca fulltext
     _query :=
        'SELECT DISTINCT SRC.entity_id AS product_id
        FROM
        (
      SELECT S.entity_id 
      FROM fts.fts_search(''' || _search || ''' , ''' || _culture_code ||''', '''') S

      union

      select product_id as entity_id
      from product
      where product_code ilike ''%'||_search||'%''
        ) SRC
        INNER JOIN
        (
    SELECT DISTINCT P.product_id, P.viewelement_id
    FROM product P
      INNER JOIN product_node PN ON PN.product_id = P.product_id
    ' || _availability_filter || '
      INNER JOIN node N ON PN.node_id = N.node_id
      INNER JOIN division D ON d.group_id = n.group_id
      INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
    INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      ) 
    WHERE (P.published = true)
      AND ' || _product_only_child_filter || '
      AND D.division_id = ' || _division_id || ' AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')
        ) PN ON PN.product_id = SRC.entity_id';
  ELSEIF (char_length(_search)=0 AND char_length(_facet_filter)>0) THEN
    -- ricerco per nodo e filtri
   _query :=
        'SELECT DISTINCT SRC.entity_id AS product_id
        FROM
        (
    SELECT DISTINCT X.entity_id 
    FROM fts.product_entity_facets_xref X
    WHERE X.facets_union_id @@''' || _facet_filter || '''::query_int
        ) SRC
        INNER JOIN
        (
    SELECT DISTINCT P.product_id, P.viewelement_id
    FROM product P
      INNER JOIN product_node PN ON PN.product_id = P.product_id
      INNER JOIN node N ON PN.node_id = N.node_id
      INNER JOIN division D ON d.group_id = n.group_id
    ' || _availability_filter || '
      INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
    INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      )
    WHERE (P.published = true) 
    AND ' || _product_only_child_filter || '
    AND D.division_id = ' || _division_id || ' AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')
        ) PN ON PN.product_id = SRC.entity_id';
  ELSE
    -- ricerco per nodo, fulltext e filtri
        -- ricerco per nodo, fulltext e filtri
      _query :=
        'SELECT DISTINCT SRC1.entity_id AS product_id
        FROM
        (
      SELECT entity_id 
      FROM fts.fts_search(''' || _search || ''', ''' || _culture_code || ''', '''')

      union

      select product_id as entity_id
      from product
      where product_code ilike ''%'||_search||'%''
        ) SRC1
        INNER JOIN
        (
    SELECT DISTINCT X.entity_id 
    FROM fts.product_entity_facets_xref X
    WHERE X.facets_union_id @@''' || _facet_filter || '''::query_int
        ) SRC2 ON SRC2.entity_id = SRC1.entity_id
        INNER JOIN
        (
    SELECT DISTINCT P.product_id, P.viewelement_id
    FROM product P
      INNER JOIN product_node PN ON PN.product_id = P.product_id
      INNER JOIN node N ON PN.node_id = N.node_id
      INNER JOIN division D ON d.group_id = n.group_id
    ' || _availability_filter || '
      INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
      INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      )
    WHERE (P.published = true)
      AND ' || _product_only_child_filter || '
      AND D.division_id = ' || _division_id || ' AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')
        ) PN ON PN.product_id = SRC1.entity_id';
  END IF;

  -- raise notice '%', _query;
  RETURN QUERY
    EXECUTE _query;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public_filter_entities(bigint, bigint, text, text, text, bigint, boolean, boolean)
  OWNER TO moxadmin;
 

/**********************************************************************************/
/**********************************************************************************/
/*********************** CUSTOMIZZZAZIONE DI STORED STANDARD **********************/
/**********************************************************************************/
/**********************************************************************************/
  
CREATE OR REPLACE FUNCTION gastaldi.cart_get_cart_data(_user_id bigint, _config json)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
DECLARE
  _culture_id     bigint;
  _customer_id    bigint;
  _result     json;
  _cart_id    bigint;
  _cart_documenttype_id   bigint;
  _modified   boolean;
  _default_paymenttype_id bigint;
BEGIN
  select cart_documenttype_id into _cart_documenttype_id
  from firm limit 1;

  select d.document_id into _cart_id
  from document d
  where d.documenttype_id = _cart_documenttype_id and d.user_id = _user_id;

  select u.culture_id, u.customer_id into _culture_id, _customer_id
  from site_user u
  where u.user_id = _user_id;

  if(_customer_id is null) then
    select default_customer_id into _customer_id from firm limit 1;
  end if;

  if(_customer_id is not null)then
    select ae.default_paymenttype_id into _default_paymenttype_id
    from customer c
    inner join addresselement ae on c.addresselement_id = ae.addresselement_id 
    where c.customer_id = _customer_id;
  end if;

  select res into _modified
  from public_cart_correction_with_quantities(_user_id, _cart_id) as res;
  
  SELECT array_to_json(array_agg(row_to_json(r))) into _result from (
    select d.document_id as cart_id,
    (select array_to_json(array_agg(row_to_json(d))) from (
      select 
        cd.documentdetail_id,
        cd.insert_order,
        cd.ship_date,
        p.product_id as id,
        p.product_code as code,
        coalesce(pvl.vl_name, pbvl.vl_name) as name,
        coalesce(pvl.vl_description, pbvl.vl_description) as description,
    
        cast(common_get_complete_image_url(
          (case when coalesce(pvl.image_url, pbvl.image_url) is not null 
            then coalesce(pvl.image_url, pbvl.image_url)
            else 
              (
                select coalesce(l1.image_url, l2.image_url, '')
                from product p1 
                left join viewelement_localized l1 on p1.viewelement_id = l1.viewelement_id and l1.culture_id in (_culture_id)
                left join viewelement_localized l2 on p1.viewelement_id = l2.viewelement_id and l2.culture_id in (1)
                where p1.product_id = p.parent_product_id limit 1
              )
            end), 'list', _config) 
        as varchar) image,
        cd.quantity,
        cd.um_id,
        cast(cd.extra_data as json) as extra_data,
        
        (select array_to_json(array_agg(row_to_json(fg))) from (
          select efficienza,aderenza,emissione,rumorosita,contributo_pfu 
          from metadata.dati_commerciali 
          where entity_id = P.product_id
        ) fg) as commercial_data
      from documentdetail cd
        inner join product p on cd.product_id = p.product_id 
        left outer join viewelement_localized pvl ON pvl.viewelement_id =  p.viewelement_id AND pvl.culture_id = _culture_id
        left outer join viewelement_localized pbvl ON pbvl.viewelement_id =  p.viewelement_id AND pbvl.culture_id = 1
      where cd.document_id = _cart_id
      order by cd.insert_order
    ) d) as details,
    _modified as modified,
    _customer_id as customer_id,
    _default_paymenttype_id as default_paymenttype_id
    from document d
    where d.user_id = _user_id and d.document_id = _cart_id
  ) r;
  
  return _result;
END
$function$
  COST 100;
ALTER FUNCTION gastaldi.cart_get_cart_data(_user_id bigint, _config json) OWNER TO moxadmin;


  -- Function: gastaldi.public_get_order_document_detail(bigint, bigint, json, bigint)

-- DROP FUNCTION gastaldi.public_get_order_document_detail(bigint, bigint, json, bigint);

CREATE OR REPLACE FUNCTION gastaldi.public_get_order_document_detail(
    _user_id bigint,
    _document_id bigint,
    _config json,
    _doc_culture bigint)
  RETURNS json AS
$BODY$
DECLARE
  _culture_id   bigint;
  _customer_id  bigint;
  _result   json;
BEGIN

-- parametro opzionale che prende la precedenza sulla cultura dell'utente nel caso sia specificato
if (_doc_culture > 0) then
  _culture_id = _doc_culture;
else 
  select u.culture_id into _culture_id
  from site_user u
  where u.user_id = _user_id;
end if;

select u.customer_id into _customer_id
from site_user u
where u.user_id = _user_id;

SELECT array_to_json(array_agg(row_to_json(r))) into _result from (
  select  dt.*,
    to_char(dt.ship_date, 'dd/MM/YYYY') as formatted_ship_date,
    (select row_to_json(p) from (
      select p.*,
        cpa.product_code_alias,
      coalesce(pvl.vl_name, pbvl.vl_name) as product_name,
      coalesce(pvl.vl_description, pbvl.vl_description) as product_description,
      cast(common_get_complete_image_url(
        (case when coalesce(pvl.image_url, pbvl.image_url) is not null 
        then coalesce(pvl.image_url, pbvl.image_url)
        else 
          (
            select coalesce(l1.image_url, l2.image_url, '')
            from product p1 
            left join viewelement_localized l1 on p1.viewelement_id = l1.viewelement_id and l1.culture_id in (_culture_id)
            left join viewelement_localized l2 on p1.viewelement_id = l2.viewelement_id and l2.culture_id in (1)
            where p1.product_id = p.parent_product_id limit 1
          )
        end), 'list', _config) 
      as varchar) image_url
    ) p) as product,   
    (select row_to_json(u) from (
      select um.*,
      coalesce(umvl.vl_name, umbvl.vl_name) as vl_name,
      coalesce(umvl.vl_description, umbvl.vl_description) as vl_description
    ) u) as um
    ,(select array_to_json(array_agg(row_to_json(fg))) from (
      select efficienza,aderenza,emissione,rumorosita,contributo_pfu 
      from metadata.dati_commerciali 
      where entity_id = P.product_id
     ) fg) as commercial_data
  from documentdetail dt
    inner join um um on dt.um_id = um.um_id
    left outer join viewelement_localized umvl on um.viewelement_id = umvl.viewelement_id and umvl.culture_id = _culture_id
    left outer join viewelement_localized umbvl on um.viewelement_id = umbvl.viewelement_id and umbvl.culture_id = 1
    inner join product p on p.product_id = dt.product_id
    left outer join viewelement_localized pvl on p.viewelement_id = pvl.viewelement_id and pvl.culture_id = _culture_id
    left outer join viewelement_localized pbvl on p.viewelement_id = pbvl.viewelement_id and pbvl.culture_id = 1
    left outer join customer_product_alias cpa on p.product_id = cpa.product_id and cpa.customer_id = _customer_id
  where dt.document_id = _document_id
  order by (array_to_json(string_to_array(dt.outline_number, '.'))::json->>0)::integer,
    coalesce((array_to_json(string_to_array(dt.outline_number, '.'))::json->>1)::integer, -1), dt.insert_order
) r;

return _result;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gastaldi.public_get_order_document_detail(bigint, bigint, json, bigint)
  OWNER TO moxadmin;



CREATE OR REPLACE FUNCTION public.sdk_excel_import_product_parse_row(_user_id bigint, _import_id bigint, _j json)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
DECLARE
 
BEGIN
    INSERT INTO gastaldi.imported_product
    (sdk_excel_import_id, product_code, cai, ean, cai_2, 
    description, invernale, status, giac0306, disp0306, b_o, 
    famiglia, s_famiglia)
    VALUES
    (_import_id, _j->>'Codice', _j->>'CAI', _j->>'EAN', _j->>'CAI 2', 
    _j->>'Descrizione', _j->>'Invernale', _j->>'STATO', _j->>'GIAC0306', _j->>'DISP0306', _j->>'B.O.', 
    _j->>'Famiglia', _j->>'sFamiglia');    
    
    RETURN '{"status": "success"}';
END;
$function$
-- Function: public_filter_entities(bigint, bigint, text, text, text, bigint, boolean, boolean)

-- DROP FUNCTION public_filter_entities(bigint, bigint, text, text, text, bigint, boolean, boolean);

CREATE OR REPLACE FUNCTION public_filter_entities(
    _division_id bigint,
    _category_id bigint,
    _search text,
    _facet_filter text,
    _culture_code text,
    _user_id bigint,
    _export boolean DEFAULT false,
    _only_available boolean DEFAULT false)
  RETURNS SETOF bigint AS
$BODY$

DECLARE
  _query              text;
  _product_only_child_no_filter   text;
  _product_only_child_filter      text;
  _check_availability     boolean;
  _availability_filter      text;
BEGIN
  IF (_search IS NULL) THEN 
    _search := '';
  END IF;
  IF (_facet_filter IS NULL) THEN 
    _facet_filter := '';
  END IF;

  _query := '';
  
  _product_only_child_filter := '((P.master_product = true) OR (P.parent_product_id IS NULL))'; --prodotto padre o non è child
-- nel caso in cui siamo nell'export excel i prodotti padre vanno comunque esplosi per la videata corrente, quindi "bypasso" il setting
  if(_export = true or ((select value from settings where settings.name ilike 'catalog.show_only_child') = 'true')) then
  _product_only_child_filter := '((P.master_product = false) OR (P.parent_product_id IS NOT NULL))'; --non è prodotto padre o è un child
  end if;

  select case when coalesce(s.value, 'false') = 'true' then false else true end into _check_availability
  from settings s
  where s.module = 'publicsite' and s.name = 'catalog.show_not_available';

  --filtro di default
  _availability_filter := 'inner join stockquantity sq on P.product_id = sq.product_id and sq.incoming_date is null and sq.quantity > 0';
  if(_only_available is not null) then  
  if(_only_available = false) then
    _availability_filter := ' ';
  end if;
  else 
  if(coalesce(_check_availability, false) = false) then
    _availability_filter := ' ';
  end if;
  end if;
  
  IF (char_length(_search)=0 AND char_length(_facet_filter)=0) THEN
    -- restituisco solo i prodotti visibili che sono collegati ad una categoria che a sua volta Ã¨ collegata ad una divisione
    -- prodotti non collegati a categorie.. o collegati a categorie non appartenenti a nessuna divisione non vengono restituiti
    -- ricerco solo per nodo
      _query :=
        'SELECT DISTINCT P.product_id
        FROM product P
    INNER JOIN product_node PN ON PN.product_id = P.product_id
    ' || _availability_filter || '
    INNER JOIN node N ON PN.node_id = N.node_id
    INNER JOIN division D ON d.group_id = n.group_id 
    INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
    INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      )
        WHERE (P.published = true) 
          AND ' || _product_only_child_filter || '
          AND D.division_id = ' || _division_id || ' 
          AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')';
  ELSEIF (char_length(_search)>0 AND char_length(_facet_filter)=0) THEN
    -- ricerco per nodo e ricerca fulltext
     _query :=
        'SELECT DISTINCT SRC.entity_id AS product_id
        FROM
        (
      SELECT S.entity_id 
      FROM fts.fts_search(''' || _search || ''' , ''' || _culture_code ||''', '''') S

      union

  -- rif. ticket #1678967: customizzazione per gastaldi aggiunto per evitare problemi in fase di ricerca con parole composte
      select p.product_id as entity_id
      from product p
  inner join viewelement_localized vl on p.viewelement_id = vl.viewelement_id 
      where p.product_code ilike ''%'||_search||'%'' or vl.vl_name ilike ''%'||_search||'%''
        ) SRC
        INNER JOIN
        (
    SELECT DISTINCT P.product_id, P.viewelement_id
    FROM product P
      INNER JOIN product_node PN ON PN.product_id = P.product_id
    ' || _availability_filter || '
      INNER JOIN node N ON PN.node_id = N.node_id
      INNER JOIN division D ON d.group_id = n.group_id
      INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
    INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      ) 
    WHERE (P.published = true)
      AND ' || _product_only_child_filter || '
      AND D.division_id = ' || _division_id || ' AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')
        ) PN ON PN.product_id = SRC.entity_id';
  ELSEIF (char_length(_search)=0 AND char_length(_facet_filter)>0) THEN
    -- ricerco per nodo e filtri
   _query :=
        'SELECT DISTINCT SRC.entity_id AS product_id
        FROM
        (
    SELECT DISTINCT X.entity_id 
    FROM fts.product_entity_facets_xref X
    WHERE X.facets_union_id @@''' || _facet_filter || '''::query_int
        ) SRC
        INNER JOIN
        (
    SELECT DISTINCT P.product_id, P.viewelement_id
    FROM product P
      INNER JOIN product_node PN ON PN.product_id = P.product_id
      INNER JOIN node N ON PN.node_id = N.node_id
      INNER JOIN division D ON d.group_id = n.group_id
    ' || _availability_filter || '
      INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
    INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      )
    WHERE (P.published = true) 
    AND ' || _product_only_child_filter || '
    AND D.division_id = ' || _division_id || ' AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')
        ) PN ON PN.product_id = SRC.entity_id';
  ELSE
    -- ricerco per nodo, fulltext e filtri
        -- ricerco per nodo, fulltext e filtri
      _query :=
        'SELECT DISTINCT SRC1.entity_id AS product_id
        FROM
        (
      SELECT entity_id 
      FROM fts.fts_search(''' || _search || ''', ''' || _culture_code || ''', '''')

      union

  -- rif. ticket #1678967: customizzazione per gastaldi aggiunto per evitare problemi in fase di ricerca con parole composte
      select p.product_id as entity_id
      from product p
  inner join viewelement_localized vl on p.viewelement_id = vl.viewelement_id 
      where p.product_code ilike ''%'||_search||'%'' or vl.vl_name ilike ''%'||_search||'%''
        ) SRC1
        INNER JOIN
        (
    SELECT DISTINCT X.entity_id 
    FROM fts.product_entity_facets_xref X
    WHERE X.facets_union_id @@''' || _facet_filter || '''::query_int
        ) SRC2 ON SRC2.entity_id = SRC1.entity_id
        INNER JOIN
        (
    SELECT DISTINCT P.product_id, P.viewelement_id
    FROM product P
      INNER JOIN product_node PN ON PN.product_id = P.product_id
      INNER JOIN node N ON PN.node_id = N.node_id
      INNER JOIN division D ON d.group_id = n.group_id
    ' || _availability_filter || '
      INNER JOIN viewelement_market_visibility MV ON MV.viewelement_id = P.viewelement_id AND MV.market_id IN
      (
        SELECT -1 AS market_id UNION ALL
        SELECT M.market_id FROM user_extra UE INNER JOIN market M on M.code=UE.market_code AND UE.user_id = ' || _user_id || '
      )
      INNER JOIN viewelement_usergroup_visibility UG ON UG.viewelement_id = P.viewelement_id AND UG.usergroup_id IN
      (
        SELECT -1 AS usergroup_id UNION ALL
        SELECT group_id FROM user_user_group WHERE user_id = ' || _user_id || '
      )
    WHERE (P.published = true)
      AND ' || _product_only_child_filter || '
      AND D.division_id = ' || _division_id || ' AND ( ' || _category_id  || ' = -1 OR n.node_id = ' || _category_id || ')
        ) PN ON PN.product_id = SRC1.entity_id';
  END IF;

   -- raise notice '%', _query;
  RETURN QUERY
    EXECUTE _query;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public_filter_entities(bigint, bigint, text, text, text, bigint, boolean, boolean)
  OWNER TO moxadmin;





-- Function: visibility_list_get_rules_by_entity(bigint, json, text, integer, integer)

-- DROP FUNCTION visibility_list_get_rules_by_entity(bigint, json, text, integer, integer);

CREATE OR REPLACE FUNCTION visibility_list_get_rules_by_entity(
    _current_user_id bigint,
    _entity_id json,
    _search text,
    _offset integer,
    _limit integer)
  RETURNS json AS
$BODY$
DECLARE 
  _entity_filter  text;
  
  _culture_id bigint;
  _query      text;
  _user_data  json := null;
  _group_data json := null;
  _rules    json;

  _result   json;
BEGIN
  select culture_id into _culture_id 
  from site_user
  where user_id = _current_user_id;

  -- distinzione ricerca per utente / gruppo
  if((_entity_id->>'userID') is not null) then
    _entity_filter := 'vr.user_id = ' || cast(_entity_id->>'userID' as bigint);

    select row_to_json(r) into _user_data
    from (
      select user_id, username
      from site_user 
      where user_id = cast(_entity_id->>'userID' as bigint)
    ) r;
  else 
    _entity_filter := 'vr.group_id = ' || cast(_entity_id->>'groupID' as bigint);

    select row_to_json(r) into _group_data
    from (
      select group_id, code, description, internal, (select user_id from user_user_group where group_id = cast(_entity_id->>'groupID' as bigint) limit 1) as sample_user_id
      from user_group
      where group_id = cast(_entity_id->>'groupID' as bigint)
    ) r;
  end if;

  -- notazione: se alla stored viene passato limite negativo, allora si è in creazione e necessito solo dell'entity
  if(_limit > 0) then
    _query := '
    select json_agg(r) from (
      select *, count(*) over (range unbounded preceding) as count
      from (
        select vr.user_visibilityrule_id, vr.search_text, vr.filter_text, vr.inclusive,
          (
            select array_to_json(array_agg(row_to_json(f))) 
            from (
              select lof.loc_text_value AS facet_text,
                (case when lkp.id is null
                then u.field_value 
                else lkp.description
                end)::character varying as facet_value,
                ft.sql_datatype 
              from (
                select U.id
                from fts.product_facets_union U 
                  inner join fts.product_entity_facets_xref_inv X on U.id = X.facets_union_id
                group by U.id
                order by U.id 
              ) F
                inner join fts.product_facets_union u on u.id = f.id
                inner join metabase.field fld on fld.id = u.field_id
                inner join metabase.field_type ft on fld.field_type_id = ft.id
                inner join public_get_fieldname(' || _culture_id || ') lof on lof.loc_id_value=FLD.id
                inner join metabase.fieldgroup_dataschema_binding fdb on fdb.fieldgroup_id = fld.fieldgroup_id
                inner join metabase.dataschema ds on ds.id = fdb.dataschema_id
                left outer join metabase.lookup_value lkp ON lkp.lookup_id = fld.lookup_id and lkp.value = u.field_value
              where f.id in (select unnest(string_to_array(rtrim(ltrim(vr.filter_text, ''(''), '')''), ''&'')::bigint[]))
            ) f
          ) as filter_json
        from user_visibilityrule vr      
        where ' || _entity_filter || ' and (vr.search_text ilike ''%' || _search || '%'')
        order by vr.inclusive
      ) l
      limit ' || _limit || ' offset ' || _offset || '
    ) r;';

    -- raise notice '%', _query;

    execute _query into _rules;

  end if;
  
  SELECT row_to_json(res)  INTO _result 
  FROM (
    SELECT _rules "rules",
      _user_data "userData",
      _group_data "groupData"
  ) res;

  return _result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION visibility_list_get_rules_by_entity(bigint, json, text, integer, integer)
  OWNER TO moxadmin;

  
CREATE OR REPLACE FUNCTION gastaldi.search_products_reduct(
    IN _user_id bigint,
    IN _search text,
    IN _facet_filter text,
    IN _page_index integer,
    IN _page_size integer,
    IN _config json)
  RETURNS TABLE(id bigint, code character varying, name character varying, brand character varying, brand_image character varying, published boolean, number_of_records bigint) AS
$BODY$
DECLARE
  _culture_id           bigint;
  _culture_code           text;
  _customer_id          bigint;
  _base_url           character varying;
  _base_cult_id       bigint;
  _query              text;
  
  _division_id          bigint;
  _division_group_id        bigint;
  _node_id            bigint;
  
  _limit_query          text;
BEGIN
  -- determino la cultura dell'utente
  SELECT U.culture_id, C.culture_code, U.customer_id INTO _culture_id, _culture_code, _customer_id
  FROM site_user U
  INNER JOIN culture C ON C.culture_id = U.culture_id
  WHERE U.user_id = _user_id;

  select culture_id into _base_cult_id
  from culture
  where culture_code = 'BASE';

  select division_id, group_id INTO _division_id, _division_group_id FROM division where division_code = 'GASTALDI';
  _node_id := -1;

  -- notazione: se _page_size o _page_index sono negativi allora siamo nella versione stampabile nella quale vanno tornati tutti i prodotti senza limiti
  _limit_query := '';
  if(_page_size > 0 and _page_index > 0) then
    _limit_query := ' LIMIT  ' || _page_size || ' OFFSET ' || (_page_index - 1) * _page_size;
  end if;
  
  _query:= '
  WITH T AS (
    SELECT FP.public_filter_entities as product_id
    FROM (select public_filter_entities FROM public_filter_entities(' || _division_id || ',' || _node_id || ', ''' || _search || ''', ''' || _facet_filter || ''', ''' || _culture_code || ''' ,' || _user_id || ')) FP
  )
  SELECT P.product_id
    ,P.product_code
    ,COALESCE(VE.vl_name,'''') as node_name
    ,bvl.vl_name as brand
    ,null::varchar -- (select common_get_complete_image_url from common_get_complete_image_url(bvl.image_url, ''zoom'', ''' || _config || '''))
    ,P.published
    ,COUNT(P.product_id) OVER (range unbounded preceding) AS number_of_records
  FROM T
    INNER JOIN product P ON P.product_id = T.product_id  and coalesce(P.visibility_status, 0) <> 2
    INNER JOIN viewelement_localized VE ON VE.viewelement_id = P.viewelement_id AND VE.culture_id = ' || _base_cult_id || '  
    left outer join brand b on P.brand_id = b.brand_id
    LEFT OUTER JOIN viewelement_localized bvl ON bvl.viewelement_id = b.viewelement_id AND bvl.culture_id = ' || _base_cult_id || '
  ORDER BY bvl.vl_name, P.product_code ' ||
  _limit_query;

  raise notice '%', _query;

  RETURN QUERY
    EXECUTE _query;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION gastaldi.search_products_reduct(bigint, text, text, integer, integer, json)
  OWNER TO moxadmin;

CREATE OR REPLACE FUNCTION get_user_data(_user_id bigint)
  RETURNS json AS
$BODY$
DECLARE
  _user     json;
  _functionalities  json;
  _json       json;
  tokens      text[];
  _customer_id    bigint;
  _culture_id     bigint;
  _base_culture_code  character varying;
BEGIN

  select f.base_culture_code into _base_culture_code
  from firm f
  limit 1;

  select customer_id into _customer_id from site_user where user_id = _user_id;
    
  select row_to_json(r) into _user from (
    SELECT 
      u.user_id, 
      u.culture_id, 
      u.username, 
      u.email, 
      u.active,
      cus.customer_code as analytics_id,
      cus.company_name,
      (select array_to_json(array_agg(row_to_json(r))) from user_role ur inner join role r on r.role_id = ur.role_id where ur.user_id = u.user_id) as role_list,
      (CASE WHEN c.culture_code = 'BASE' THEN _base_culture_code
           ELSE c.culture_code
      END) AS url_culture,
      u.agent_id,
      (select row_to_json(a) from agent a where a.agent_id = u.agent_id) as agent,
      u.customer_id,
      u.default_customer_id,
      u.is_anonymous,
      (select array_to_json(array_agg(row_to_json(up)))  from user_policy_agreement up where up.user_id = u.user_id) as policy_agreement,
      coalesce(u.is_sub_agent, 0) as is_sub_agent,          
      (select array_to_json(array_agg(row_to_json(a))) from (
        select a.*, replace(coalesce(a.cellular_phone, a.phone), '/', '') as link_phone
        from customer_agent ca
          inner join agent a on ca.agent_id = a.agent_id
        where ca.customer_id = _customer_id and a.agent_code <> 'super_agent'
      ) a) as agents_references
    FROM site_user u
      INNER JOIN culture c ON c.culture_id = u.culture_id
      LEFT JOIN customer cus ON u.customer_id = cus.customer_id
    WHERE
      u.user_id = _user_id
  ) r;

  select array_to_json(array_agg(row_to_json(f))) into _functionalities from (
    select * from get_user_enabled_functionalities(_user_id)
  ) f;
  
  SELECT row_to_json(F) 
  INTO _json 
  FROM (
    SELECT 
      _user "user", 
      _functionalities "functionalities"
  ) F;

  RETURN _json;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION get_user_data(bigint)
  OWNER TO moxadmin;
COMMENT ON FUNCTION get_user_data(bigint) IS 'CUSTOMIZATION OF GastaldiGomme (related agents information for customer users)';
  -- Function: public_get_active_user_customers_list(bigint, integer, integer, text, text, bigint)

-- DROP FUNCTION public_get_active_user_customers_list(bigint, integer, integer, text, text, bigint);

CREATE OR REPLACE FUNCTION public_get_active_user_customers_list(
    IN _user_id bigint,
    IN _limit integer,
    IN _offset integer,
    IN _where text,
    IN _order text,
    IN _accounttype_id bigint)
  RETURNS TABLE(customer_id bigint, customer_code character varying, company_name character varying, blocked integer, vat_code character varying, address character varying, zip_code character varying, city character varying, province character varying, region character varying, country character varying, is_billing boolean, is_shipping boolean, billing_customer_id bigint, total_count bigint, statuselement_icon character varying, statuselement_color character varying, allowed_documenttype_id json, css_class character varying, statuselement_name character varying, status_type integer, accounttype_id bigint, accounttype_code character varying, local_customer_code character varying, btb_active boolean, releated_agents json) AS
$BODY$
DECLARE
  _query            text := '';
  _grants_filter      text := '';
  _agent_where        text := '';
  _local_code_field     text := '';
  _has_local_code     integer := 0;
  _agent_id         bigint;
  _customer_id        bigint;
  _use_statuselement    boolean;
  _avoid_active_check boolean;
  _active_check   text;
  _statuselement_query  text;
  _accounttype_filter   text;
  _btb_profile_id   bigint;
BEGIN 
  select u.agent_id, u.customer_id into _agent_id, _customer_id
  from site_user u
  where u.user_id = _user_id;

  select profile_id into _btb_profile_id from site_user_profile where profile_code = 'CST';

  if(_agent_id is not null) then
    select has_local_code into _has_local_code from agent where agent_id = _agent_id;
  end if;
  
  if(can_execute(_user_id,'customer.list.all') = true) then
    _grants_filter := '';
  else  


    if(_customer_id is null) then
      _customer_id := -1;
    end if;
    
    if(_agent_id is not null) then 
      -- utente agente
      _agent_where := 'inner join customer_agent ca on c.customer_id = ca.customer_id and ca.agent_id = ' || _agent_id;
      _grants_filter := ' ';
      

    else
      -- utente customer
      _grants_filter := 'and c.customer_id = ' || _customer_id;
    end if;
  end if;

  _accounttype_filter := ' and (coalesce(c.accounttype_id, 1) = 1)'; --di default estrae i clienti
  if(_accounttype_id is not null) then
    _accounttype_filter := ' and c.accounttype_id = '|| _accounttype_id ||' ';
  end if;

  if(coalesce(_where, '') <> '')then
    _where := replace(_where, '''', ''''''); --dato che la query è una stringa è necessario eseguire l'escape dell'apice in modo che nella stringa sia '' 
  end if;

  select case when coalesce(value, 'false') = 'true' then true else false end into _use_statuselement 
  from settings 
  where environment = 'showcase' and module = 'public_document' and name = 'create.use_statuselement'; 
  
  if(coalesce(_use_statuselement, false) = true) then
      _statuselement_query := 'left join (  
        select statuselement_id, status_code,
            (select json_agg(d.documenttype_id)
            from documenttype_statuselement d 
            where coalesce(purge_status, 0) = 0 and d.statuselement_id = st.statuselement_id
            ) as allowed_documenttype_id,
            vl.icon,
            vl.color,
            vl.css_class,
            vl.vl_name,
            st.status_type::integer
        from statuselement st
        inner join viewelement_localized vl on vl.viewelement_id = st.viewelement_id
      ) se on se.statuselement_id = cs.status_element_id';
  else
        _statuselement_query := 'left join (  
        select '''' as status_code,
            null::json as allowed_documenttype_id,
            ''''::varchar as icon,
            ''''::varchar as color,
            ''''::varchar as css_class,
            ''''::varchar as vl_name,
            null as statuselement_id,
            0::integer as status_type
        ) se on 1=1'; --con questa condizione viene attaccato a tutti i customer
  end if;  
  
  select case when coalesce(value, 'false') = 'true' then true else false end into _avoid_active_check 
  from settings 
  where environment = 'showcase' and module = 'publicsite' and name = 'use_customer.avoid_check';

  _active_check := ' inner join site_user u on u.customer_id = c.customer_id and coalesce(u.active, 0) = 1 ';
  if(coalesce(_avoid_active_check, false) = true) then
  _active_check := '';
  end if;
  
  _query := 
    'with customers as(
      (
        select c.customer_id as customer_id,
          c.customer_code as customer_code,
          c.company_name as company_name,
          c.blocked,
          a.vat_code,
          a.address,
          a.zip_code,
          a.city,
          a.province,
          a.region,
          a.country,
          c.is_billing,
          c.is_shipping,
          c.billing_customer_id,
          c.status_element_id,
          ct.accounttype_id,
          ct.accounttype_code
      from customer c ' ||
  _active_check || '
  left join accounttype ct on ct.accounttype_id = c.accounttype_id
  left outer join addresselement a on c.addresselement_id = a.addresselement_id  ' ||
      _agent_where || ' 
      where ( c.customer_code ilike ''%' || _where || '%'' 
            or c.company_name ilike ''%' || _where || '%'' or a.address_name ilike ''%' || _where || '%'' or a.address ilike ''%' || _where || '%'' 
            or a.zip_code ilike ''%' || _where || '%'' or a.city ilike ''%' || _where || '%'' or a.province ilike ''%' || _where || '%'' 
            or a.region ilike ''%' || _where || '%'' '|| 
            case when _agent_where <> '' and _has_local_code <> 0 then ' or ca.local_customer_code ilike ''%'||_where||'%'' ' else '' end
            ||')
        and c.visibility_status < 2 ' || _grants_filter || ' ' || _accounttype_filter || ' 
      )
    )
    select
        cs.customer_id, customer_code, company_name, blocked, vat_code, address, zip_code, city, province, region, country, is_billing, is_shipping, billing_customer_id,
        count(cs.*) over (range unbounded preceding) as total_count,
        se.icon as statuselement_icon, se.color as statuselement_color, se.allowed_documenttype_id, se.css_class, se.vl_name, se.status_type::integer,
        accounttype_id, accounttype_code, ' || case when coalesce(_has_local_code, 0) = 1 then 'ca.local_customer_code' else '''''::varchar'end || ',
        -- Gastaldi Customization
        (u.user_id is not null) as btb_active,          
  (select array_to_json(array_agg(row_to_json(a))) from (
    select a.agent_name
    from customer_agent ca
      inner join agent a on ca.agent_id = a.agent_id
    where ca.customer_id = cs.customer_id and a.agent_code <> ''super_agent''
  ) a) as releated_agents
    from customers cs 
  left join (select distinct on (customer_id) customer_id, user_id from site_user where coalesce(active, 0) = 1 and profile_id = ' || _btb_profile_id || ') u on u.customer_id = cs.customer_id 
' ||
    case when coalesce(_has_local_code, 0) = 1 then 
      ' left join customer_agent ca on ca.customer_id = cs.customer_id and ca.agent_id = ' ||_agent_id|| ' '
    else
      ''
    end
    || _statuselement_query ||'
    order by ' || _order || '
    limit ' || _limit || ' offset ' || _offset;

  raise notice '%', _query;

  RETURN QUERY
    EXECUTE _query;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public_get_active_user_customers_list(bigint, integer, integer, text, text, bigint)
  OWNER TO moxadmin;
COMMENT ON FUNCTION public_get_active_user_customers_list(bigint, integer, integer, text, text, bigint) IS 'CUSTOMIZATION OF gastaldi';
