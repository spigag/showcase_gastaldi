angular.module('public_seo_extra', [])

.run(['fctSeo','$rootScope', function(fctSeo,$rootScope) {

    $rootScope.$on('fastSearch', function(event, data) { 
        fctSeo.initTracking('event', { type: 'search', category: 'fast_search', data: data});
    });
    
    fctSeo.trackingType = {
      page_view: 'page_view',
      event: {  
        search: {
          label: "Ricerca",
          category: {
            catalog: { label : 'Ricerca catalogo'}, 
            fast_search: { label : 'Ricerca veloce'}, 
          }
        }, 
      }
    },
    
    fctSeo.parseEventData = function (data){
        if(typeof data === 'string')
          return data;
        var searchString = "";
        if(data.main){
          searchString += "Principale: ";
          searchString += fctSeo.createSearchString(data.main);
        }
        if(data.secondary){
          searchString += "Accoppiata: ";
          searchString += fctSeo.createSearchString(data.secondary);
        }
        searchString = searchString.replace(/,\s*$/, "");
        return searchString;
    };

    fctSeo.createSearchString = function(item){
        var obj = {};
        var searchString = "";
        if(item.size)
          searchString += ("misura: " + item.size + ", ");
        if(item.facets){
          if(item.facets.brand == -1){
            searchString += ("Marca : tutte, ");
          } else {
            obj = JSON.parse(item.facets.brand);
            searchString += (obj.facet_text + ":" + obj.facet_value + ", ");
          }
          if(item.facets.genre == -1){
            searchString += ("Genere: tutti, ");
          } else {
            obj = JSON.parse(item.facets.genre);
            searchString += (obj.facet_text + ":" + obj.facet_value + ", ");
          }
          if(item.facets.sector == -1){
            searchString +=  ("Settore: tutti, ");
          } else {
            obj = JSON.parse(item.facets.sector);
            searchString += (obj.facet_text + ":" + obj.facet_value + ", ");
          }
          if(item.facets.flap){
            searchString += "camera/flap, ";
          }
          if(item.facets.promo){
            searchString += "in promo, ";
          }
          if(item.facets.reinforced){
            searchString += "rinforzato, ";
          }
          if(item.facets.run_on_flat){
            searchString += "run on flat, ";
          }
          if(item.facets.seasonal){
            searchString += "quattro stagioni, ";
          }
          if(item.facets.summer){
            searchString += "estivo, ";
          }
          if(item.facets.winter){
            searchString += "invernale, ";
          }
          if(item.facets.wheel){
            searchString += "cerchio, ";
          }
        }
        if(item.stock){
          searchString += "solo articoli disponibili, ";
        }
        return searchString;
      };
}]);