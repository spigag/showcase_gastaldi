angular.module('visibility_list-module',['ngRoute','services.crud','services.i18nNotifications','directives.gravatar','ui.directives','module-paging'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  /****  ROUTES  ****/ 
  .when('/admin/visibility_list/', {
    templateUrl:'visibility_list/visibility_list.tpl.html',
    controller:'visibilityListCtrl'
  }) 
  .when('/admin/visibility_list/user_:uid', {
    templateUrl:'visibility_list/visibility_list-user.tpl.html',
    controller:'userListCtrl',
    resolve: {
      data: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return fctVisibilityList.getUserVisibilityRules($route.current.params.uid, true);
      }]
    },
    reloadOnSearch: false
  })
  .when('/admin/visibility_list/user_:uid/new', {
    templateUrl:'visibility_list/visibility_list-edit.tpl.html',
    controller:'visibilityRuleEditCtrl',
    resolve: {
      data: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return fctVisibilityList.getUserVisibilityRules($route.current.params.uid, false, true);
      }],
      item: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return {};
      }],
      facets: ['fctGastaldiFacets', function(fctGastaldiFacets) {
        return fctGastaldiFacets.getFacets();
      }]
    },
    reloadOnSearch: false
  }) 
  .when('/admin/visibility_list/user_:uid/:id', {
    templateUrl:'visibility_list/visibility_list-edit.tpl.html',
    controller:'visibilityRuleEditCtrl',
    resolve: {
      item: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return fctVisibilityList.getRuleByID($route.current.params.id);
      }],
      facets: ['fctGastaldiFacets', function(fctGastaldiFacets) {
        return fctGastaldiFacets.getFacets();
      }]
    },
    reloadOnSearch: false
  })
  .when('/admin/visibility_list/group_:gid', {
    templateUrl:'visibility_list/visibility_list-usergroup.tpl.html',
    controller:'groupListCtrl',
    resolve: {
      data: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return fctVisibilityList.getUsergroupVisibilityRules($route.current.params.gid, true);
      }]
    },
    reloadOnSearch: false
  })
  .when('/admin/visibility_list/group_:gid/new', {
    templateUrl:'visibility_list/visibility_list-edit.tpl.html',
    controller:'visibilityRuleEditCtrl',
    resolve: {
      data: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return fctVisibilityList.getUsergroupVisibilityRules($route.current.params.gid, false, true);
      }],
      item: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return {};
      }],
      facets: ['fctGastaldiFacets', function(fctGastaldiFacets) {
        return fctGastaldiFacets.getFacets();
      }]
    },
    reloadOnSearch: false
  }) 
  .when('/admin/visibility_list/group_:gid/:id', {
    templateUrl:'visibility_list/visibility_list-edit.tpl.html',
    controller:'visibilityRuleEditCtrl',
    resolve: {
      item: ['$route', 'fctVisibilityList', function($route, fctVisibilityList) {
        return fctVisibilityList.getRuleByID($route.current.params.id);
      }],
      facets: ['fctGastaldiFacets', function(fctGastaldiFacets) {
        return fctGastaldiFacets.getFacets();
      }]
    },
    reloadOnSearch: false
  })
}])

.factory('fctVisibilityList', ["$http", "fctGastaldiFacets", function ($http, fctGastaldiFacets) {
  var fct = {
    currentUser: null,
    currentGroup: null,

    paging: {
      totalSize : 0,
      size: 20,
      current: 0
    },

    list: [],

    reset: function() {   
      fct.list = [];
      fct.paging.current = 0;
      fct.noMoreData = false;
    },

    getUserVisibilityRules: function(userID, reset, onlyEntity) {
      if(reset) fct.reset();

      // evito il caricamento inutile controllando i flag di sistema
      if(fct.loadingData || fct.noMoreData)
        return;

      fct.loadingData = true;

      // parametro per richista
      var data = {
        "userID": userID || fct.currentUser.user_id,
        "search": fct.search,
        "offset": fct.paging.current,
        "limit": fct.paging.size
      };

      if(onlyEntity)
        data.limit = -1;

      var promise = $http.post("/api/v1/vibility_list/rules_by_entity", data)
      .then(function(response){
        fct.paging.current += 1;
        var data = response.data;
        fct.currentUser = data.userData;
        if(data && !onlyEntity) {
          if(!data.rules || data.rules.length == 0) {
            data.rules = [];
            // se l'ultimo caricamento non ha tornato risultati so che non devo più fare caricamenti
            fct.noMoreData = true;
          } else {
            fct.paging.totalSize = data.rules[0].count;
          }

          fct.list = fct.list.concat(data.rules);
        }
        fct.loadingData = false;
      });
      return promise;
    },

    getUsergroupVisibilityRules: function(groupID, reset, onlyEntity) {
      if(reset) fct.reset();

      // evito il caricamento inutile controllando i flag di sistema
      if(fct.loadingData || fct.noMoreData)
        return;

      fct.loadingData = true;

      // parametro per richista
      var data = {
        "groupID": groupID || fct.currentGroup.group_id,
        "search": fct.search,
        "offset": fct.paging.current,
        "limit": fct.paging.size
      };

      if(onlyEntity)
        data.limit = -1;  

      var promise = $http.post("/api/v1/vibility_list/rules_by_entity", data)
      .then(function(response){
        fct.paging.current += 1;
        var data = response.data;
        fct.currentGroup = data.groupData;
        if(data && !onlyEntity) {
          if(!data.rules || data.rules.length == 0) {
            data.rules = [];
            // se l'ultimo caricamento non ha tornato risultati so che non devo più fare caricamenti
            fct.noMoreData = true;
          } else {
            fct.paging.totalSize = data.rules[0].count;
          }
          fct.list = fct.list.concat(data.rules);
        }      
        fct.loadingData = false;
      });
      return promise;
    },

    getRuleByID: function(ruleID) {
      var promise = $http.get("/api/v1/vibility_list/rule_by_id/" + ruleID)
      .then(function(response) {
        var data = response.data;
        return data;
      });
      return promise;
    },

    saveVisibilityRule: function(item, callback) {      
      if(!fctGastaldiFacets.lastPayload.freeSearch && !fctGastaldiFacets.lastPayload.facetFilter)
        return;

      item.freeSearch = fctGastaldiFacets.lastPayload.freeSearch;
      item.facetFilter = fctGastaldiFacets.lastPayload.facetFilter;

      var data = {
        "item": item
      };

      var endPoint = "/api/v1/vibility_list/create_rule";
      if(item.user_visibilityrule_id)
        endPoint = "/api/v1/vibility_list/update_rule";

      var promise = $http.post(endPoint, data)
      .then(function(response){
        if(callback)
          callback(response.data);
        else 
          return response.data;
      });
      return promise;
    },

    deleteVisibilityRule: function(ruleID, callback) {
      var promise = $http.get('/api/v1/vibility_list/delete_rule/' + ruleID)
      .then(function(response) {
        if(callback)
          callback(response.data);
        else 
          return response.data;
      });
      return promise;
    },

    resetVisibilityList: function(callback) {
      var data = {};
      if(fct.currentUser)
        data.userID = fct.currentUser.user_id;
      else
        data.groupID = fct.currentGroup.group_id;

      var promise = $http.post('/api/v1/vibility_list/reset_visibility_list', data)
      .then(function(response) {
        if(callback)
          callback(response.data);
        else 
          return response.data;
      });
      return promise;
    },
  };
  return fct;
}])

.factory('fctGastaldiFacets', ["$http", "moxutils", "fctFixedWords", function ($http, moxutils, fctFixedWords){
  var fct = {
    list: {},

    getFacets: function(){
      var promise = $http.get("/api/v1/gastaldi/facets")
        .success(function(data){  
          fct.list = data;
          return data;
        })
        .error(function(err){
            console.log(err);
        });
      return promise;
    },

    getSearchInput: function(data, model) {
      var result = facetFilter = "";

      angular.forEach(data, function(value, key) {
        if(value) {
          obj = angular.fromJson(value);
          if(typeof obj === "boolean") {
            facetFilter += model[key].field_id + '&';
          } else {
            if(obj && obj.length > 0) {
              if(obj.length == 1) {
                facetFilter += obj[0];
              }

              if(obj.length > 1) {
                facetFilter += "(";
                for (var i = 0; i < obj.length; i++) {
                  facetFilter += obj[i];
                  if(i < (obj.length-1)) facetFilter += '|';
                };
                facetFilter += ")";
              } 
              facetFilter += '&'; 
            }

          } 
        }
      }); 
      if(facetFilter.length)
        result += '(' + facetFilter.substring(0, facetFilter.length-1) + ')';
      return result;
    },

    paging: {
      totalSize : 0,
      size: 10,
      current: 1,
      noMoreData: false,
      loadingData: false
    },

    doSearch: function(userID, payload, reset, cb) {
      if(reset) {
        fct.paging.current = 1;
        fct.paging.noMoreData = false;
        fct.paging.loadingData = false;
        fct.lastPayload = null;
      }
      if(fct.paging.loadingData || fct.paging.noMoreData) {
        return;
      }

      if(userID)
        fct.searchUserID = userID;

      fct.paging.loadingData = true;
      fct.searched = true;

      if(payload)
        fct.lastPayload = payload;

      var data = {
        "userID": fct.searchUserID,
        "user_search" : fct.lastPayload.freeSearch,
        "selected_facets" : fct.lastPayload.facetFilter,
        "page_index" : fct.paging.current,
        "page_size" : fct.paging.size
      };

      var promise = $http.post("/api/v1/gastaldi/user_search", data)
      .success(function(response) {
        if(response) {
          fct.paging.current += 1;
          if(response.length) {
            fct.paging.totalSize = response[0].number_of_records;
          } else {
            // se l'ultimo caricamento non ha tornato risultati so che non devo più fare caricamenti
            fct.paging.noMoreData = true;
            fct.paging.totalSize = 0;
          }

          if(response.length < fct.paging.size) {
            fct.paging.noMoreData = true;
          }
          fct.paging.loadingData = false;
        }
        if(cb) cb(response);
        else return response;
      })
      .error(function(err){
        fct.paging.loadingData = false;
        return false;
      });
      return promise; 
    }
  };
  return fct;
}])

/* controller per la lista, al momento non prevista in quanto si passa direttamente al dettaglio da impostare */
.controller('visibilityListCtrl',['$scope','$location', function ($scope, $location) {
  $location.path('/admin');
}])

.controller('userListCtrl', ['$scope','$route','i18nNotifications','$http','$location','fctVisibilityList','data',
  function ($scope, $route, i18nNotifications, $http, $location, fctVisibilityList, data) {
    //recupero gli elementi da rappresentare
    $scope.visibilityList = fctVisibilityList;  

    $scope.resetVisibilityList = function() {
      $scope.visibilityList.resetVisibilityList(function(data) { 
        i18nNotifications.pushForCurrentRoute('crud.visibility_list.reset.success', 'success');  
        fctVisibilityList.getUserVisibilityRules($route.current.params.uid, true);
      });
    };
  }
])

.controller('groupListCtrl', ['$scope','$route','i18nNotifications','$http','$location','fctVisibilityList','data',
  function ($scope, $route, i18nNotifications, $http, $location, fctVisibilityList, data) {
    //recupero gli elementi da rappresentare
    $scope.visibilityList = fctVisibilityList;    

    $scope.resetVisibilityList = function() {
      $scope.visibilityList.resetVisibilityList(function(data) { 
        i18nNotifications.pushForCurrentRoute('crud.visibility_list.reset.success', 'success');
        fctVisibilityList.getUserVisibilityRules($route.current.params.gid, true);
      });
    };
  }
])

.controller('visibilityRuleEditCtrl', ['$scope','$rootScope','$timeout','i18nNotifications','$http','$location','$filter','fctVisibilityList','fctGastaldiFacets','item','facets',
  function ($scope, $rootScope, $timeout, i18nNotifications, $http, $location, $filter, fctVisibilityList, fctGastaldiFacets, item, facets) {
    //recupero gli elementi da rappresentare
    $scope.visibilityList = fctVisibilityList;
    $scope.item = item;
    $scope.facets = fctGastaldiFacets;

    $scope.searched = false;

    if(!$scope.item.user_visibilityrule_id) {
      $scope.isNew = true;
      // default
      $scope.item.inclusive = true;
      if(fctVisibilityList.currentUser) {
        $scope.item.user_id = fctVisibilityList.currentUser.user_id;        
        $scope.item.username = fctVisibilityList.currentUser.username;
      } else {
        $scope.item.group_id = fctVisibilityList.currentGroup.group_id;   
        $scope.item.sample_user_id = fctVisibilityList.currentGroup.sample_user_id;        
        $scope.item.group_description = fctVisibilityList.currentGroup.description;
      }
    }

    var data = {};
    for(var i = 0; i < fctGastaldiFacets.list.length; i++){
      if(fctGastaldiFacets.list[i].facets.length == 2){
        var positiveId = 0;
        angular.forEach(fctGastaldiFacets.list[i].facets, function(value, key) {
          if(value.facet_value == "true"){ 
            positiveId = value.facet_id;
          }
        });
        data[fctGastaldiFacets.list[i].code] = { 'field_id' : positiveId, 'facet_text' : fctGastaldiFacets.list[i].facet_text };
      } else {
        data[fctGastaldiFacets.list[i].code] = fctGastaldiFacets.list[i];
      }
    } 
    $scope.formFacets = {
      'brands' : data["MARCA"],
      'genre' : data["GENERE"],
      'sector' : data["SETTORE"],
      'reinforced': data["RINFORZATO"],
      'summer': data["ESTIVO"],
      'winter': data["INVERNALE"],
      'seasonal': data["QUATTRO_STAGIONI"],
      'promo': data["IN_PROMO"],
      'wheel': data["CERCHIO"],
      'run_on_flat': data["RUN_ON_FLAT"],
      'flap': data["CAMERA_FLAP"],
    };

    $scope.model = {
      'size': '',
      'facets': {
        'brand': [],
        'sector': [],
        'genre': [],
      },
      'stock': false
    };

    if(!$scope.isNew) {
      // caricamento nella view dei filti salvati
      $scope.model.size = item.search_text;

      for (var i = 0; $scope.item.filter_json && i < $scope.item.filter_json.length; i++) {
        switch($scope.item.filter_json[i].code) {
          case 'RINFORZATO': $scope.model.facets.reinforced = true; break;
          case 'ESTIVO': $scope.model.facets.summer = true; break;
          case 'INVERNALE': $scope.model.facets.winter = true; break;
          case 'QUATTRO_STAGIONI': $scope.model.facets.seasonal = true; break;
          case 'IN_PROMO': $scope.model.facets.promo = true; break;
          case 'CERCHIO': $scope.model.facets.wheel = true; break;
          case 'RUN_ON_FLAT': $scope.model.facets.run_on_flat = true; break;
          case 'CAMERA_FLAP': $scope.model.facets.flap = true; break;

          case 'MARCA': $scope.model.facets.brand.push($scope.item.filter_json[i].facet_id); break;
          case 'GENERE': $scope.model.facets.genre.push($scope.item.filter_json[i].facet_id); break;
          case 'SETTORE': $scope.model.facets.sector.push($scope.item.filter_json[i].facet_id); break;
        }
      };
    }


    $scope.onSubmit = function() {
      var params = {};
      $scope.products = [];
      $scope.searched = true;

      params.facetFilter = fctGastaldiFacets.getSearchInput($scope.model.facets, $scope.formFacets);

      if($scope.model.size.length){
        params.freeSearch = $scope.model.size;
      }

      var userID = null;
      if(item.user_id) {
        userID = item.user_id;
      } else {
        // se sto modificando la visibilità di un gruppo utilizzo l'utente di esempio per fare la query
        userID = item.sample_user_id;
      }

      fctGastaldiFacets.doSearch(userID, params, true, function(result) {
        if(result.length) {
          $scope.products = $scope.products.concat(result);
          $scope.productsFound = false;
        } else {
          $scope.productsFound = true;
        }
      });
    };

    $scope.nextPage = function() { 
      fctGastaldiFacets.doSearch(null, null, false, function(result) {
        if(result.length) {
          $scope.products = $scope.products.concat(result);
        }
      });
    };

    $scope.confirmVisibilityRules = function() {
      $scope.visibilityList.saving = true;

      $.gritter.add({
        title: 'Salvataggio del filtro in corso!',
        text: "La procedura potrebbe richiedere un po' di tempo. Aal termine del salvataggio il filtro sarà applicato nel catalogo.",
        time: 5000,
        class_name: 'gritter-success'
      });

      var href = '/admin/visibility_list/'
      if($scope.item.user_id)
        href = href + 'user_' + $scope.item.user_id;
      else 
        href = href + 'group_' + $scope.item.group_id;

      // aggiornamento lastPayload
      fctGastaldiFacets.lastPayload = {};
      fctGastaldiFacets.lastPayload.facetFilter = fctGastaldiFacets.getSearchInput($scope.model.facets, $scope.formFacets);

      if($scope.model.size.length){
        fctGastaldiFacets.lastPayload.freeSearch = $scope.model.size;
      }

      fctVisibilityList.saveVisibilityRule($scope.item, function(data) {        
        i18nNotifications.pushForNextRoute('crud.visibility_list.save.success', 'success');   
        $location.path(href);
        $scope.visibilityList.saving = false;
      });   
    };

    $scope.deleteVisibilityRule = function() {
      var href = '/admin/visibility_list/'
      if($scope.item.user_id)
        href = href + 'user_' + $scope.item.user_id;
      else 
        href = href + 'group_' + $scope.item.group_id;

      fctVisibilityList.deleteVisibilityRule($scope.item.user_visibilityrule_id, function(data) {       
        i18nNotifications.pushForNextRoute('crud.visibility_list.remove.success', 'success');   
        $location.path(href);
      });   
    };
  }
]);
