angular.module('public_document_extra', [])

// SellMore Web settings customizations for Gastaldi
.run(['fctDocumentData', function(fctDocumentData) {  
  fctDocumentData.settings.allowCreateShippingAddress = false;
  fctDocumentData.settings.allowEmailSend = true;
  fctDocumentData.settings.allowViewReportPreview = false;
  fctDocumentData.settings.use_promo = false;
}]);