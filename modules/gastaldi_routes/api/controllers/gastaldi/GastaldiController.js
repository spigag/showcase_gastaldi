var sequelize  = require('../../../config/databases.js').databases.sequelize,
    camunda = require('../../services/camunda'),
    config = require('../../../config/local.js');
var utils = require('../../services/utils');
var document_utils = require('../../services/public_document/utils.js');
var path = require('path');
var fs = require("fs");
var http = require('http');

// routes customizzate Gastaldi
module.exports = {
  get_user_cart: function(req, res, next) {
    var rawCart = req.param('cookie');
    var division = req.param('division');

    // utente anonimo: ritorno il flag al client che si occuperà di gestire il carrello tramite cookie
    if(req.user.is_anonymous) {
      var result = {
        is_anonymous: req.user.is_anonymous
      };

      // se l'utente è anonimo devo caricare i dati dei prodotti presenti nel cookie
      // stored che a partire dal json del carrello salvato nel cookie ritorna i dati dei prodotti relativi (compresi prezzi e quantità)
      rawCart = JSON.stringify(rawCart).replace(/'/g, "''");

      sequelize
      .query("select * from gastaldi.cart_get_cookie_products_list(" + req.user.user_id + ", '" + rawCart  + "', '" + JSON.stringify(config.images) + "')")
      .success(function(cart){
        result.cart = cart;
        res.json(result);
      })
      .error(function(error) { 
        utils.logError(res, error);   
      });
    } else {  
      var divisionParam = '';
      if(division)
        divisionParam = ', ' + division;

      var returnCartFromDB = function() {
        sequelize
        .query("select * from gastaldi.cart_get_cart_data(" + req.user.user_id + ",'" + JSON.stringify(config.images) + "'" + divisionParam + ") as cart;")
        .success(function(result) {
          if(result && result.length > 0 && result[0].cart && result[0].cart.length > 0)
            res.json(result[0].cart[0]);
          else 
            res.json({});
        })
        .error(function(error) {
          utils.logError(res, error);   
        });
      };

      // nel caso in cui il cookie non sia impostato posso caricare i dati relativi al carrello dell'utente direttamente dal db 
      if(!rawCart || rawCart.length == 0) {
        returnCartFromDB();
      } else {      
        rawCart = JSON.stringify(rawCart).replace(/'/g, "''");
        sequelize
        .query("select * from public_cart_reset_cart_from_cookie(" + req.user.user_id + ", '" + rawCart + "') as cart;")
        .success(function(result) {
          returnCartFromDB();
        })
        .error(function(error) {
          utils.logError(res, error);   
        });
      }
    }
  },

  load_document: function(req, res, next) {
    // customizzata per tornare il pfu per i vari dettagli
    var document_id = req.param('document_id');

    // parametro opzionale che indica la cultura con cui vogliamo il documento. se non specificato ritornerà la cultura dell'utente
    var document_culture = req.param('document_culture') || -1;

    sequelize
    .query("select * from public_get_order_document(" + req.user.user_id + "," + document_id + ") as doc")
    .success(function(result) {
      if(result && result.length > 0) {
        if(result[0].no_access) {
          res.json(result[0]);
          return;
        } else {
          result = result[0].doc;

          if(!result) {
            res.json({document_id: false});
            return;
          }


          sequelize
          .query("select * from gastaldi.public_get_order_document_detail(" + req.user.user_id + "," + document_id + ", '" + JSON.stringify(config.images) + "', " + document_culture + ") as dt")
          .success(function(detail) {
            if(detail && detail.length > 0) {
              result.detail = detail[0].dt;
            } 
            res.json(result);
          })
          .error(function(error) {
            console.log(error);
            res.json({document_id: false});
          });
        }
      } else {
        res.json(result);
        return;
      }
    })
    .error(function(error) {
      console.log(error);
      res.json({document_id: false});
    });
  },


  userSearch: function(req, res) {
    var userID = req.param('userID') == undefined ? req.user.user_id : req.param('userID');
    var page_index = req.param('page_index') == undefined ? 1 : req.param('page_index');
    var page_size = req.param('page_size') == undefined ? 10 : req.param('page_size');
    var search = req.param('user_search') == undefined ? '' : req.param('user_search');
    var selected_facets = req.param('selected_facets') == undefined ? '' : req.param('selected_facets');
    
    sequelize
    .query("SELECT * FROM gastaldi.search_products_reduct("+ userID + ", '" + search + "', '"+selected_facets+"', "+page_index+ " ,"+page_size+", '" + JSON.stringify(config.images) + "')")
    .success(function(result) {
      res.send(result);
    })
    .error(function(error) {
      utils.logError(res, error);   
    });
  },

  _config: {}

};
    