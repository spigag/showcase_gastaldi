var config = require('../../config/utils').utils;
var locals = require('../../config/local.js');
var theme_map = require('../../config/theme_map.js').theme_map;
var utils = require('../services/utils');

module.exports = {
    // customizzazione GASTALDI per avere sempre il redirect alla ricerca veloce
    initializeView: function(req, res) {
        return res.redirect('/IT/fast-search');
    }
};
