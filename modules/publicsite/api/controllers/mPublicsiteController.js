var config = require('../../config/utils').utils;
var locals = require('../../config/local.js');
var utils = require('../services/utils');
var theme_map = require('../../config/theme_map.js').theme_map;
var sequelize  = require('../../config/databases.js').databases.sequelize;

module.exports = {

    divisions: function(req, res) {

        var user_id = req.user.user_id;

        sequelize
        .query("select * from public_get_user_divisions(" + user_id + ", null)")
        .success(function(divisions) {
            if(divisions.length == 1) {
                // vado direttamente in quella divisione
                res.redirect('/' + locals.base_culture_code + '/division/' + divisions[0].division_id);
            } else {
                // query per ottenere le culture non attive, da passare poi all'ejs
                sequelize
                .query("select * from public_get_selectable_cultures(" + user_id + ")")
                .success(function(cultures) {
                    return res.view('emptyView.ejs', {
                        moduleName          :   'publicsite',
                        layout              :   'publicsite/listLayout.ejs',
                        techsheetViewPath   :   'EMPTY',
                        dyn_version         :   config.dyn_version,
                        stat_version        :   config.stat_version,
                        appName             :   locals.appName,
                        themeName           :   theme_map['publicsite'],
                        userCulture         :   req.user.culture_id,
                        cultures            :   cultures,
                        testEnvironment     :   locals.testEnvironment,
                        embedded            :   req.user.embedded,
                        seo                 :   locals.seo,
                        seoTags: {
                            title: locals.appName,
                            metadescription : locals.appName,
                        }
                    });
                }).error(function(error) {
                    utils.logError(res, error);
                });
            }
        }).error(function(error) {
            utils.logError(res, error);
        });
    },


    list: function(req, res) {
        var product_id = req.param('pid');
        var divid = req.param('divid');
        var user_id = req.user.user_id;

        var url = require('url');
        var url_parts = url.parse(req.url, true);
        var query = url_parts.query;

        console.log(query);
        console.log("*********************************************");

        sequelize
        .query("select * from public_get_user_divisions(" + user_id + ", null)")
        .success(function(divisions) {
            divisions = divisions.map(function(x){return x.division_id;});
            console.log('** divisione' + divid);
            if(divid && divisions.indexOf(divid) < 0) { 
                res.redirect('/' + locals.base_culture_code + '/fast-search/' + divisions[0]);
                return;
            } 

            sequelize.query("select * from public_get_selectable_cultures(" + user_id + ")")
            .success(function(cultures) {
                // se ho il product_id allora sto entrando in dettaglio proodtto
                if(product_id) {
                    sequelize
                    .query("select * from public_get_productdataschema(" + product_id + ") AS techsheet")
                    .success(function(result) {
                        if (result != null && result.length > 0) {
                            var techsheetName = result[0].techsheet;
                            if (!techsheetName || techsheetName.length == 0)
                                techsheetName = 'EMPTY';
                            return res.view('emptyView.ejs', {
                                moduleName          :   'publicsite',
                                layout              :   'publicsite/detailLayout.ejs',
                                techsheetViewPath   :   techsheetName,
                                dyn_version         :   config.dyn_version,
                                stat_version        :   config.stat_version,
                                appName             :   locals.appName,
                                themeName           :   theme_map['publicsite'],
                                userCulture         :   req.user.culture_id,
                                cultures            :   cultures,
                                testEnvironment     :   locals.testEnvironment,
                                embedded            :   req.user.embedded,
                                seo                 :   locals.seo,
                                seoTags: {
                                    title: locals.appName,
                                    metadescription : locals.appName,
                                }
                            });  
                        }
                    }).error(function(error) {
                        utils.logError(res, error);
                    });
                } else {
                    // parametri per altri moduli che non necessitano della scheda tecnica
                    return res.view('emptyView.ejs', {
                        moduleName          :   'publicsite',
                        layout              :   'publicsite/listLayout.ejs',
                        techsheetViewPath   :   'EMPTY',
                        dyn_version         :   config.dyn_version,
                        stat_version        :   config.stat_version,
                        appName             :   locals.appName,
                        themeName           :   theme_map['publicsite'],
                        userCulture         :   req.user.culture_id,
                        cultures            :   cultures,
                        testEnvironment     :   locals.testEnvironment,
                        embedded            :   req.user.embedded,
                        seo                 :   locals.seo,
                        seoTags: {
                            title: locals.appName,
                            metadescription : locals.appName,
                        }
                    });
                }
            })
            .error(function(error) {
                utils.logError(res, error);
            });
        }).error(function(error) {
            utils.logError(res, error);
        });
    }
};
