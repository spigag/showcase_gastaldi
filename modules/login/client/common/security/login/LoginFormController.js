angular.module('security.login.form', ['services.localizedMessages', 'ngCookies'])

// The LoginFormController provides the behaviour behind a reusable form to allow users to authenticate.
// This controller and its template (login/form.tpl.html) are used in a modal dialog box by the security service.
.controller('LoginFormController', ['$scope', 'security', 'localizedMessages', 'i18nNotifications', '$cookies','$location', 
 function($scope, security, localizedMessages, i18nNotifications, $cookies, $location) {
  $scope.notifications = i18nNotifications;
  // The model for this form 
  $scope.user = {};
  $scope.expired = false;
  $scope.askHostName = true;
  if($location.search() && $location.search().status &&  $location.search().status == 'ok'){
    var msg = ExtractFixedWordTranslation('USER_NOTIF_SIGNUP_SELF_COMPLETE');
    $scope.notifications.pushForCurrentRoute('signup.success', 'success', {in_language_message : msg});
  }
  if($location.search() && $location.search().status &&  $location.search().status == 'expired'){
    var msg = ExtractFixedWordTranslation('USER_NOTIF_SIGNUP_SELF_TOKEN_EXPIRED');
    $scope.notifications.pushForCurrentRoute('signup', 'danger', {in_language_message : msg});
    $scope.expired = true;
  }

  // Any error message from failing to login
  $scope.authError = null;

  // The reason that we are being asked to login - for instance because we tried to access something to which we are not authorized
  // We could do something diffent for each reason here but to keep it simple...
  $scope.authReason = null;
  if ( security.getLoginReason() ) {
    $scope.authReason = ( security.isAuthenticated() ) ?
      ExtractFixedWordTranslation('GENERAL_NOTIF_NO_AUTHORIZED') :
      ExtractFixedWordTranslation('GENERAL_NOTIF_NO_AUTH');
  }

  $scope.getFixedWord = function(word) {
    return ExtractFixedWordTranslation(word);
  };
  $scope.disableDoubleClick = false;
  // Attempt to authenticate the user specified in the form's model
  $scope.login = function() {
    if($scope.disableDoubleClick)
      return;
    $scope.disableDoubleClick = true;
    // Clear any previous security errors
    $scope.authError = null;

    // Try to login
    security.login($scope.user.username, $scope.user.password, $scope.user.remember_me, $scope.user.host_name)
    .then(function(ans) {
      $scope.disableDoubleClick = false;
      var loggedIn = security.isAuthenticated();
      //resetto il filtro usato dall'entity catalog. Uso l'operazione di login per resettarlo per evitare che venga visualizzato all'utente
      //successivo (che esegue il login sulla stessa macchina)
      //writeCookie('entitycatalog_user_search', '/admin/entitycatalog', '');
      $.cookie('entitycatalog_user_search', '', {path : '/admin/entitycatalog' });
      $.cookie('fvpphomepage', '', { expires: -1, path : '/' });

      if ( !loggedIn ) {
        // If we get here then the login failed due to bad credentials
        $scope.authError = ExtractFixedWordTranslation('GENERAL_NOTIF_INVALID_CRED');
      }else{
        $scope.authReason = ExtractFixedWordTranslation('GENERAL_SUCCESS_LOG_IN');
        
      }
    }, function(x) {
      // If we get here then there was a problem with the login request to the server
      $scope.authError = localizedMessages.get('login.error.serverError', { exception: x });
    });
  };

  $scope.clearForm = function() {
    $scope.user = {};
  };

  $scope.cancelLogin = function() {
    security.cancelLogin();
  };

  $scope.goTo = function(location){
    switch(location){
      case 'back':{
        window.location = $cookies.redirectTo;
        break;
      }
      default:
        window.location = location;
        break;
    }  
  }

}]);
