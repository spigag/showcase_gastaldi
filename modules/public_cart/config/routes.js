module.exports.routes = {
	'/cart': {
		controller: 'mCartController',
		action: 'list',
		functionalities: ['public.cart.access']
	},
	'/cart/:divid': {
		controller: 'mCartController',
		action: 'list',
		functionalities: ['public.cart.access']
	},
	'/cart_order_confirmed/:token': {
		controller: 'mCartController',
		action: 'list',
		functionalities: ['public.cart.access']
	},
	/* ********** publicsite routes ********** */
	'/:lang/cart': {
		controller: 'mCartController',
		action: 'list',
		functionalities: ['public.cart.access']
	},
	'/:lang/cart/:divid': {
		controller: 'mCartController',
		action: 'list',
		functionalities: ['public.cart.access']
	},
	'/:lang/cart_order_confirmed/:token': {
		controller: 'mCartController',
		action: 'list',
		functionalities: ['public.cart.access']
	},
	/* generic routes */
	'post /api/v1/public/ask_parent_product_price': {
		controller: 'CartMiscController',
		action: 'ask_parent_product_price'
	},
	'post /api/v1/public/ask_product_price': {
		controller: 'CartMiscController',
		action: 'ask_product_price'
	},
	'post /api/v1/public/get_product_step_quantity': {
		controller: 'CartMiscController',
		action: 'get_product_step_quantity'
	},
	/* cart data */
	'post /api/v1/public/add_product_to_cart': {
		controller: 'CartMiscController',
		action: 'add_product_to_cart'
	},
	'post /api/v1/public/update_cart_product_quantity': {
		controller: 'CartMiscController',
		action: 'update_cart_product_quantity'
	},
	'post /api/v1/public/add_product_list_to_cart': {
		controller: 'CartMiscController',
		action: 'add_product_list_to_cart'
	},
	'post /api/v1/public/remove_product_from_cart': {
		controller: 'CartMiscController',
		action: 'remove_product_from_cart'
	},
	'get /api/v1/public/destroy_cart/:division_id': {
		controller: 'CartMiscController',
		action: 'destroy_cart'
	},
	/* cart checkout */
	'get /api/v1/public/get_user_customer': {
		controller: 'CartMiscController',
		action: 'get_user_customer'
	},
	'post /api/v1/public/get_customer_delivery_addresses': {
		controller: 'CartMiscController',
		action: 'get_customer_delivery_addresses'
	},
	
	'post /api/v1/public/get_customer_shipmenttypes_list': {
		controller: 'CartMiscController',
		action: 'get_customer_shipmenttypes_list'
	},
	'get /api/v1/public/get_customer_deliverytypes_list': {
		controller: 'CartMiscController',
		action: 'get_customer_deliverytypes_list'
	},
	'post /api/v1/public/create_customer_delivery_address': {
		controller: 'CartMiscController',
		action: 'create_customer_delivery_address'
	},
	'post /api/v1/public/order_checkout': {
		controller: 'CartMiscController',
		action: 'order_checkout'
	},
	'post /api/v1/public/confirm_order': {
		controller: 'CartMiscController',
		action: 'confirm_order'
	},
	'get /api/v1/public/public_has_product_to_open_buy/:pid': {
		controller: 'CartMiscController',
		action: 'public_has_product_to_open_buy'
	},
	'post /api/v1/public/get_custombuy_data': {
		controller: 'CartMiscController',
		action: 'get_custombuy_data'
	},
	'get /api/v1/public/get_cart_mandatory_list/:cid': {
		controller: 'CartMiscController',
		action: 'get_cart_mandatory_list'
	},
	'post /api/v1/public/direct_document_generation': {
		controller: 'CartMiscController',
		action: 'direct_document_generation'
	},
	'get /api/v1/public/get_creatable_document_types': {
		controller: 'CartMiscController',
		action: 'get_creatable_document_types'
	},
	'post /api/v1/public/get_user_open_documents': {
		controller: 'CartMiscController',
		action: 'get_user_open_documents'
	},
	'post /api/v1/public/direct_document_update': {
		controller: 'CartMiscController',
		action: 'direct_document_update'
	},
	'post /api/v1/public/agent_set_current_customer': {
		controller: 'CartMiscController',
		action: 'agent_set_current_customer'
	},
	'post /api/v1/public_cart/customer_addresses_management': {
		controller: 'CartMiscController',
		action: 'customer_addresses_management'
	},
	'get /api/v1/public/get_cart_legal_terms': {
		controller: 'CartMiscController',
		action: 'get_cart_legal_terms'
	},
	'get /api/v1/public/load_anonymous_user_document/:token': {
		controller: 'CartMiscController',
		action: 'load_anonymous_user_document'
	},

	/************ routes customizzate *************/
	'post /api/v1/public/get_user_cart': {
		controller: 'gastaldi/GastaldiController',
		action: 'get_user_cart'
	},
	'post /api/v1/public/load_document': {
		controller: 'gastaldi/GastaldiController',
		action: 'load_document'
	}
}