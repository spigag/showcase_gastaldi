angular.module('header-module',
  ['ngRoute',
  'ngCookies',
  'services.i18nNotifications',
  'directives.gravatar',
  'ui.directives',
  'security.login.toolbar',
  'mgcrea.ngStrap',
  'ngAnimate',
  'ui.bootstrap',
  'public-menu'
])

.controller('headerCtrl',['$scope','$rootScope','$window','$route','i18nNotifications','$http','$location','fctDivisionHeader','fctUserDivisions','security', 'moxutils','publicMenuService',
    function ($scope, $rootScope, $window, $route, i18nNotifications, $http, $location, fctDivisionHeader, fctUserDivisions, security, moxutils,publicMenuService) {
  
      $scope.menu = null;     
      $scope.isMenuLoading = true;

      publicMenuService.getMenu('HEADER')
        .then(function(result){
          $scope.menu = result;
          $scope.showLogin = true;
          $scope.isMenuLoading = false;
        });

      $scope.$on('$routeChangeSuccess', function() {
        // se nel path l'ultimo elemento non è il divisione id allora sono nel detail/compare/dwnload
        var splittedPath = $location.path().split('/');
        //recupero la divisione corrente
        $rootScope.divisionid = readCookie('active_division', '/', '', false);

        if(!$scope.divisionid || $scope.divisionid == '') {
          $scope.divisionid = -1;
        }      

        //carico i dati della divisione
        fctUserDivisions.loadDivision($scope.divisionid).then(function(ans) {
          if(ans.data) {
            $rootScope.division_data = ans.data;
            fctDivisionHeader.getDivisionFaqsCount($scope.divisionid).then(function(result) {
              $scope.divisionFaqsCount = parseInt(result.count);
            });
          }
          else
            $rootScope.division_data = {};
        });

        $scope.isAuthenticated = security.isAuthenticated;
        $scope.login = security.showLogin;
        $scope.logout = security.logout;
        $scope.$watch(function() {
          return security.currentUser;
        }, function(currentUser) {
          $scope.currentUser = currentUser;
        });
      });
      
      $scope.userDivisions = fctUserDivisions;
      $scope.isAuthenticated = security.isAuthenticated;

      $scope.toggleDivisionChange = function(ev) {
        //ev.preventDefault(),
        $("#legor-divisions-menu-container").slideToggle();
      };

      $scope.can_change_culture = function(){
        return security.currentUser == null;
      };

      $scope.change_culture = function(culture_code, ev){ 
        ev.preventDefault();  
        var userCookie = readCookie('client_culture', '/');
        if (userCookie != culture_code) {
          writeCookie('client_culture', '/', culture_code);
          security.change_culture(culture_code.toUpperCase());

          var current = $location.url();
          $location.url(current.replace(current.substr(1, 2), culture_code));
        }
      };

      $scope.get_new_culture_url = function(culture_code){
        var current = $location.url();
        return current.replace(current.substr(1, 2), culture_code);
      };

      $scope.user_loaded = function(){
          return security.userRequested;
      };

      // funzioni che ridirezionano al login/admin: è necessario un reload della pagina (tramite window.location)
      // in quanto angular 'confonde' la route della homepage (/:lang) con le route /admin e /login
      // (settando il parametro lang erroneamente). l'utilizzo di queste funzioni risolve il problema
      $scope.goToLogin = function(ev) {
        ev.preventDefault();
        $window.location = '/login';
      };
      $scope.goToAdministration = function(ev) {
        ev.preventDefault();
        $window.location = '/admin';
      };

      $scope.changeDivision = function(new_division_id, ev) {
        $scope.divisionid = new_division_id;

        // reset dei facet
        writeCookie('publicsite_facets_status', '/', '');
        writeCookie('publicsite_facets_filter', '/', '');

        // reset ricerca
        writeCookie('user_search', '/', '');
        
        fctUserDivisions.loadDivision(new_division_id)
        .then(function(ans) {
          if(ans.data)
            $scope.division_data = ans.data;
          else
            $scope.division_data = {};

          $scope.toggleDivisionChange(ev);

          if($scope.isDetail) {
            ev.preventDefault();
            $window.location = '/' + $scope.logged_user.user.url_culture + '/fast-search/' + new_division_id;
          }
        });
      };

      $scope.getDivisionCssClass = function() {
        return "division-standard " + $scope.divisionid;
      };

      $scope.getWebSiteLink = function() {
        var url = $scope.firm.addresselement.web;
        if(url.substring(0, 7) == "http://")
          return url;
        else
          return "http://" + url;
      };

      /* Apre e chiude il filtri lista in modalità phone */
      $scope.toggleFilter = function(){
        $('.row-offcanvas').toggleClass('active');
        window.scrollTo(0,0); 
        if($scope.isComparison) $(".bs-docs-sidebar .nav .nav").show();
      };

      $scope.getCategoryName = function() {
        if(fctCategoryList.activeCategory)
          return ' - ' + fctCategoryList.activeCategory.name;
      };
            
  }]
);