var config = require('../../config/utils').utils;
var locals = require('../../config/local.js');
var utils = require('../services/utils');
var theme_map = require('../../config/theme_map.js').theme_map;
var sequelize  = require('../../config/databases.js').databases.sequelize;

module.exports = {
    list: function(req, res) {
        var user_id = req.user.user_id;
        var divid = req.param('divid');

        sequelize
        .query("select * from public_get_user_divisions(" + user_id + ", null)")
        .success(function(divisions) {
            divisions = divisions.map(function(x){return x.division_id;});
            if(divisions.indexOf(divid) < 0) {
                res.redirect('/' + locals.base_culture_code + '/fast-search/' + divisions[0]);
                return;
            } 

            // query per ottenere le culture non attive, da passare poi all'ejs
            sequelize
            .query("select * from public_get_selectable_cultures(" + user_id + ")")
            .success(function(cultures) {
                // parametri per altri moduli che non necessitano della scheda tecnica
                return res.view('emptyView.ejs', {
                    moduleName          :   'public_cart',
                    layout              :   'public_cart/layout.ejs',
                    techsheetViewPath   :   'EMPTY',
                    dyn_version         :   config.dyn_version,
                    stat_version        :   config.stat_version,
                    appName             :   locals.appName,
                    themeName           :   theme_map['public_cart'],
                    userCulture         :   req.user.culture_id,
                    cultures            :   cultures,
                    testEnvironment     :   locals.testEnvironment,
                    seo                 :   locals.seo
                });
            })
            .error(function(error) {
                utils.logError(res, error);
            });
        }).error(function(error) {
            utils.logError(res, error);
        });
    }
};
