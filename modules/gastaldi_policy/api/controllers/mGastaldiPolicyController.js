var config = require('../../config/utils').utils;
var locals = require('../../config/local.js');
var utils = require('../services/utils');
var theme_map = require('../../config/theme_map.js').theme_map;
var sequelize  = require('../../config/databases.js').databases.sequelize;

module.exports = {
  home: function(req, res) {
    
    return res.view('emptyView.ejs', {
        moduleName          :   'gastaldi_policy',
        layout              :   'gastaldi_policy/layout.ejs',
        techsheetViewPath   :   'EMPTY',
        dyn_version         :   config.dyn_version,
        stat_version        :   config.stat_version,
        appName             :   locals.appName,
        themeName           :   'front',
        userCulture         :   req.user.culture_id,
        testEnvironment     :   locals.testEnvironment,
        seo                 :   locals.seo,
        seoTags: {
            title: locals.appName,
            metadescription : locals.appName,
        }
    });
  }

};
