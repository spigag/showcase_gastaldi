angular.module('app', [
  'ngRoute',
  'ngCookies',
  'ngAnimate',
  'ngSanitize',
  'templates.gastaldi_policy.app',
  'public-seo',
  'gastaldi-policy-module'
])

.constant('I18N.MESSAGES', {
  //customers resources
  'crud.customer.message':"{{message}}",
})

angular.module('app').config([
    '$routeProvider',
    '$locationProvider',
    '$controllerProvider',
    '$compileProvider',
    '$filterProvider',
    '$provide',

    function($routeProvider, $locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide)
    {
      $locationProvider.html5Mode(true);
      $provide.decorator('$sniffer', function($delegate) {
        $delegate.history = true;
        return $delegate;
      });

      $routeProvider.otherwise({
          redirectTo: function(routeParams, path, search){
            window.location = path;
          }
      });
    }
]);



angular.module('app').config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  /***** vecchie routes senza lingua: necessarie per il redirect post login alla route con il lang corretto *****/
  .when('/gastaldi_policy', 
  {
    templateUrl: 'gastaldi_policy/gastaldi_policy.tpl.html',
    controller: 'gastaldiPolicyCtrl',
    reloadOnSearch: false
  })
}])

.directive('actionSinglePopovers', function($document,$rootScope,$timeout,$popover) {
  return {
    restrict: 'EA',
    link : function(scope, element, attrs) {
      var $element = $(element);
      $element.click(function() {
        $('.popover').each(function() {
          var $this = $(this);
          if($this.attr('id') != $element.attr('id')){
            $this.scope().$hide();
          }
        });
      });
    }
  }
})

.directive('fallbackPlaceholder', function () {
  var fallbackPlaceholder = {
    link: function postLink(scope, element, attrs) {
      element.bind('error', function() {
        $(this).hide(); 
        $('#' + attrs.fallbackPlaceholder + '-img-placeholder').show();
      });
    }
   }
   return fallbackPlaceholder;
})
.controller('AppCtrl', ['$scope', function($scope) {

}]);