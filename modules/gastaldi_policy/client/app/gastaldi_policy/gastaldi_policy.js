angular.module('gastaldi-policy-module',['ngRoute','ngCookies'])

.directive('scrollSpyCompare', function($timeout){
  return function(scope, elem, attr) {
    $timeout(function() { 
      $('body').scrollspy({ target: '#compare-tab' });
      var tab = $('#compare-tab');
      tab.affix({
        offset: {
            top: function() {
                var c = tab.parent().offset().top, d = 0, e = 60;
                return this.top = c - e - d;
            },
            bottom: function() {
                return this.bottom = 0;
            }
        }
      });
    }, 1000);
  }
})

.directive('repeatStarted', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      if (scope.$index > 0) {
        $timeout(function () {
          scope.$emit('ngCompareRepeatStarted');
        });
      }
    }
  }
})

.controller('gastaldiPolicyCtrl', ['$scope',  function ($scope) {
  
  $scope.files = [
    {name: "Privacy policy", url:"https://sk-gastaldigomme.s3-eu-west-1.amazonaws.com/policy/20190402-wwwgastaldigommeservicecom-privacypolicy.pdf"},
    {name: "Cookie policy", url:"https://sk-gastaldigomme.s3-eu-west-1.amazonaws.com/policy/20190402-wwwgastaldigommeservicecom-cookiepolicy.pdf"},
    {name: "Informativa clienti", url:"https://sk-gastaldigomme.s3-eu-west-1.amazonaws.com/policy/20190402-gastaldigommeservicesrl-informativaclienti.pdf"},
    {name: "Codice condotta", url:"https://sk-gastaldigomme.s3-eu-west-1.amazonaws.com/policy/20190402-gastaldigommeservicesrl-codicecondotta.pdf"},
    {name: "Informativa www.gastaldigommeservice.com", url:"https://sk-gastaldigomme.s3-eu-west-1.amazonaws.com/policy/20190402-gastaldi-gomme-service-srl-informativawwwgastaldigommeservicecom.pdf"},
  ]


}])