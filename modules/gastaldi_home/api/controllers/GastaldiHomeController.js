var sequelize  = require('../../config/databases.js').databases.sequelize
, utils = require('../services/utils')
, config = require('../../config/local.js')
, path = require('path')
, fs = require('fs');

module.exports = {
  get_facets: function(req, res){
    sequelize
    .query("select * from gastaldi.get_facets(" + req.user.user_id + ") as c")
    .success(function(result) {
       if(result.length){
        res.json(result.map(function(x){return x.c;}));
       }
       else
        res.json({});
    })
    .error(function(error) {
      utils.logError(res, error);   
    });
  },

  do_search: function(req, res) {
      var division_id = req.param('division_id');
      var page_index = req.param('page_index') == undefined ? 1 : req.param('page_index');
      var page_size = req.param('page_size') == undefined ? 10 : req.param('page_size');
      var search = req.param('user_search') == undefined ? '' : req.param('user_search');
      var stock = req.param('stock_filter') == undefined ? 'null' : req.param('stock_filter');
      var selected_facets = req.param('selected_facets') == undefined ? '' : req.param('selected_facets');
      
      search = search.replace(/'/g, " ");

      sequelize
      .query("SELECT * FROM gastaldi.search_products("+req.user.user_id+ ", " + division_id + ", '" +search+ "', " +stock+ ", '"+selected_facets+"', "+page_index+ " ,"+page_size+", '" + JSON.stringify(config.images) + "')")
      .success(function(result) {
        if(result.length > 0) {
          if(config.commerce && config.commerce.active) {
            // caricamento prezzi per prodotti
            var pids = [];
            for (var i = 0; i < result.length; i++) {
              pids.push(result[i].id);
              result[i].discount1 = result[i].discount;
              result[i].discount2 = 0;
              result[i].price = Math.round((result[i].public_price * ((100 - result[i].discount) / 100))*100)/100;
            };

            sequelize
            .query("select * from public_get_prices_settings(" + req.user.user_id + ")")
            .success(function(prices_settings){
              prices_settings = prices_settings[0];

              if(prices_settings.customer_id && prices_settings.customer_id != undefined && prices_settings.customer_id > 0) {
                par = JSON.stringify({ 
                  "products": pids, 
                  "customerID": prices_settings.customer_id,
                  "priceListID": prices_settings.pricelist_id,
                  "currencyID": prices_settings.currency_id 
                });

                var headers = {
                  'Content-Type': 'application/json',
                  'Content-Encoding': 'utf-8',
                  'Content-Length': par.length
                };

                var options = {
                  host: config.pricesService.host,
                  port: config.pricesService.port,
                  path: config.pricesService.endPoint.list,
                  method: 'POST',
                  headers: headers
                };

                var req1 = http.request(options, function(ans) {
                  var answer = '';
                  
                  ans.setEncoding('utf-8');

                  ans.on('data', function(chunk, error){
                    answer += chunk;
                  });

                  ans.on('end', function(error) {
                    try {
                      var data = JSON.parse(answer);
                      var priceKey = 'netTotalPriceBeforeDisposal';
                      for (var i = 0; i < result.length; i++) {
                        var key = result[i].id.toString();

                        if(data[key]) {
                          result[i].promoPrice = Math.round((data[key][priceKey]) * 100) / 100;
                          result[i].disposalPrice = data[key].disposalPrice;
                          
                          
                          // result[i].netTotalPriceWithVAT = data[key].netTotalPriceWithVAT;  Nota: la pl calcola l'iva senza il pfu
                          result[i].netTotalPriceWithVAT = Number(data[key].netTotalPrice * (1 + data[key].VAT));

                          result[i].currency = data[key].currencyCode;
                          result[i].currency_format_string = data[key].currencyFormatString;

                          if(data[key].productInfo) {
                            result[i].availability_status = data[key].productInfo.availabilityStatus;
                            result[i].stockQuantity = data[key].productInfo.stockQuantity;
                            result[i].stockIncomingQuantity = data[key].productInfo.stockIncomingQuantity;
                          }
                        }
                      };

                      res.json(result);
                    } catch (err) {
                      console.log('**** ERRORE NEL CARICAMENTO DEI PREZZI ****');
                      console.log(err);
                      res.json(result);
                    }
                  });
                });

                req1.on('error', function(e) {
                  console.log("pricelogic servis error: %j", e);
                  res.json(result);
                  return;
                });

                req1.write(par);
                req1.end();
              } else {
                res.json(r);
              }
            }).error(function(error) { // Fine prodotti
              utils.logError(res, error);   
            });
          } else {
            res.send(result);
          }
        } else {
          res.json([]);
        }
      })
      .error(function(error) {
        utils.logError(res, error);   
      });
  },

  do_search_v2: function(req, res) {
    var division_id = req.param('division_id');
    var page_index = req.param('page_index') == undefined ? 1 : req.param('page_index');
    var page_size = req.param('page_size') == undefined ? 10 : req.param('page_size');
    var search = req.param('user_search') == undefined ? '' : req.param('user_search');
    var stock = req.param('stock_filter') == undefined ? 'null' : req.param('stock_filter');
    var selected_facets = req.param('selected_facets') == undefined ? '' : req.param('selected_facets');
    
    search = search.replace(/'/g, " ");

    sequelize
    .query("SELECT * FROM gastaldi.search_products_cached("+req.user.user_id+ ", " + division_id + ", '" +search+ "', " +stock+ ", '"+selected_facets+"', "+page_index+ " ,"+page_size+", '" + JSON.stringify(config.images) + "')")
    .success(function(result) {
      if(result.length > 0) {
        if(config.commerce && config.commerce.active) {

          res.send(result);
        }
      } else {
        res.json([]);
      }
    })
    .error(function(error) {
      utils.logError(res, error);   
    });
  },

  product_extra: function(req, res) {
    var productID = req.param('id');

    sequelize
    .query("select * from gastaldi.product_extra(" + req.user.user_id + ", " + productID + ", '" + JSON.stringify(config.images) + "') as e")
    .success(function(result) {
      if(result && result[0])
        res.json(result[0].e);
      else
        res.json(result);
    })
    .error(function(error) {
      utils.logError(res, error);   
    });
  },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to ClientsController)
   */
  _config: {}
};
