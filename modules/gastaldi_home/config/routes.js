module.exports.routes = {
	'/fast-search': {
		controller: 'mGastaldiHomeController',
		action: 'divisions'
	},
	'/:lang/fast-search': {
		controller: 'mGastaldiHomeController',
		action: 'divisions'
	},
	'/fast-search/:divid': {
		controller: 'mGastaldiHomeController',
		action: 'home'
	},
	'/:lang/fast-search/:divid': {
		controller: 'mGastaldiHomeController',
		action: 'home'
	},

	'get /api/v1/gastaldi/facets': {
		controller: 'GastaldiHomeController',
		action: 'get_facets'
	},

	'post /api/v1/home/search': {
		controller: 'GastaldiHomeController',
		action: 'do_search'
	},

	'post /api/v2/home/search': {
		controller: 'GastaldiHomeController',
		action: 'do_search_v2'
	},

	'get /api/v1/home/product_extra/:id': {
		controller: 'GastaldiHomeController',
		action: 'product_extra'
	}
};