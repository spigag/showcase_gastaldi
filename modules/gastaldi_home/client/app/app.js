angular.module('app', [
  'ngRoute',
  'ngCookies',
  'ngAnimate',
  'ngSanitize',
  'oc.lazyLoad',
  'directives.crud',
  'services.breadcrumbs',
  'services.menus',
  'services.i18nNotifications',
  'services.httpRequestTracker',
  'security',
  'header-module',
  'gastaldi-home-module',
  'templates.gastaldi_home.app',
  'templates.common.app',
  'templates.common.common',
  'module-status_bar_utils',
  'module-popover_compare',
  'module-popover_download',
  'module-popover_wishlist',
  'module-popover_cart',
  'module-paging',
  'techsheet-module',
  'templates.techsheet',
  'templates.public_common.app',
  'buy-module',
  'public-menu',
  'public-seo'
])

.constant('I18N.MESSAGES', {
  'errors.route.changeError': ExtractFixedWordTranslation('GENERAL_NOTIF_ROUTE_CHANGE_ERROR'),

  //login resources
  'login.reason.notAuthorized': ExtractFixedWordTranslation('GENERAL_NOTIF_NO_AUTHORIZED'),
  'login.reason.notAuthenticated': ExtractFixedWordTranslation('GENERAL_NOTIF_NO_AUTH'),
  'login.error.invalidCredentials': ExtractFixedWordTranslation('GENERAL_NOTIF_INVALID_CRED'),
  'login.error.serverError': ExtractFixedWordTranslation('GENERAL_NOTIF_AUTH_ERR'),

  //customers resources
  'crud.customer.message':"{{message}}",
})

angular.module('app').config([
    '$routeProvider',
    '$locationProvider',
    '$controllerProvider',
    '$compileProvider',
    '$filterProvider',
    '$provide',

    function($routeProvider, $locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide)
    {
      $locationProvider.html5Mode(true);
      $provide.decorator('$sniffer', function($delegate) {
        $delegate.history = true;
        return $delegate;
      });

      $routeProvider.otherwise({
          redirectTo: function(routeParams, path, search){
            window.location = path;
          }
      });
    }
]);

angular.module('app').run(['security', function(security) {
  // Get the current user when the application starts
  // (in case they are still logged in from a previous session)
  security.requestCurrentUser();

}]);



angular.module('app').config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  /***** vecchie routes senza lingua: necessarie per il redirect post login alla route con il lang corretto *****/
  .when('/fast-search', 
  {
    templateUrl: 'gastaldi_home/divisions.tpl.html',
    controller: 'noLangCtrl',
    reloadOnSearch: false
  })
  .when('/:lang/fast-search', 
  {
    templateUrl: 'gastaldi_home/divisions.tpl.html',
    controller: 'gastaldiDivisionsCtrl',
    resolve:{
      divisions: ['fctUserDivisions', function(fctUserDivisions) {return fctUserDivisions.load();}]
    },
    reloadOnSearch: false
  })
  .when('/fast-search/:divid', 
  {
    templateUrl: 'gastaldi_home/gastaldi_home.tpl.html',
    controller: 'noLangCtrl',
    reloadOnSearch: false
  })
  .when('/:lang/fast-search/:divid', 
  {
    templateUrl: 'gastaldi_home/gastaldi_home.tpl.html',
    controller: 'gastaldiHomeCtrl',
    resolve: {
      facets: ['fctGastaldiFacets', 'fctHomeFactory', function(fctGastaldiFacets, fctHomeFactory) {
        // reset flag ricerca
        fctHomeFactory.searched = false;
        return fctGastaldiFacets.getFacets();
      }],
      homepageData: ['fctHomeConfig', function(fctHomeConfig) {
        return fctHomeConfig.loadData();
      }],
      cart: ['fctCart', '$route', function(fctCart, $route) {
        return fctCart.loadCurrentList($route.current.params.divid);
      }],
      division: ['$route','fctUserDivisions', function($route, fctUserDivisions) {
        return fctUserDivisions.loadDivision($route.current.params.divid);
      }]
    },
    reloadOnSearch: false
  })
}])

// interfaccia per gestione taglia colore da parte front
.factory('fctBuyInterface', ["fctCart", "fctProduct", '$route' ,'$http', '$location', '$window', '$rootScope', 'i18nNotifications', 'moxutils',
    function(fctCart, fctProduct, $route, $http, $location, $window, $rootScope, i18nNotifications, moxutils) {
  var fct = {
    settings:{
      considerateStock: true,
      show_code_on_grid: true
    },

    currentItem: null,
    modalClass: "buy-standard-modal",
    
    modalHeaders: {
       "catalog": "buy_templates/standard_header.tpl.html",
       "smw":     "buy_templates/standard_header.tpl.html"
    },
    
    modalFooters: {
       "catalog":  "buy_templates/catalog_footer.tpl.html",
       "smw":     "buy_templates/standard_footer_smw.tpl.html"
    },
    environment: "catalog",
    
    showLinkToCart: true,

    onlyPublicPriceMode: (readCookie('publicsite_only_public_price', '/', '', false) == "true" ? true : false),

    askPrice: function(product, cb) {
      return fctProduct.askPrice(product, null, cb);
    },

    selectCurrentItem: function(product) {
      if(fct.selectingItem)
        return;
      
      fct.selectingItem = true;

      // gestione oggetti sostitutivi
      if(product.substituted_by) {
        product = product.substituted_by;
      }

      if(!product.id) product.id = product.product_id;

      var openBuy = function(product) {
        product.quantity = fctCart.getProductQuantity(product.id);
        product.ship_date = getDateFromDB(fctCart.getProductShipDate(product.id));
        fctProduct.askPrice(product).then(function(data) {
          fct.currentItem = product;

          if(fct.currentItem.priceDetail)
            fct.currentItem.startPrice = fct.currentItem.priceDetail.netTotalPrice;

          fct.currentItem.currentTotal = fctCart.getTotalPrice();

          if(!fct.currentItem.custombuy_template || fct.currentItem.custombuy_template == undefined) {
            fct.currentItem.custombuy_template = 'buy_templates/standard.tpl.html';
          }
          // apertura modale
          $('#buyModal').on('shown.bs.modal', function (e) {
            fct.selectingItem = false;
            $('#buyModal').off('shown.bs.modal');
          });

          $('#buyModal').modal('show');
        });
      };

      fctProduct.hasToOpenBuy(product.id).then(function(response) {
        if(response) {
          openBuy(product);
        } else {
          $window.location = '/item/' + product.id;
        }
      });
    },

    showSizeColorStock: function() {
      return true;
    },

    detailStockTemplate: 'buy_templates/size_color-detail-stock.tpl.html',
    cellStockTemplate: 'buy_templates/size_color-cell-stock.tpl.html',
    stock: null,


    // funzione (n giorno custom per decidere se tenere in considerazione gli stock nell'acquisto)
    considerateStock: function() {
        return fct.settings.considerateStock;
    },

    openStockDetailModal: function(product, event) {
      if(event) event.stopPropagation();

      var productID = product.id;
      if(!productID) productID = product.priceDetail.productID;
      
      var params = {
          pid: productID,
          did: null
      };
      fct.currentStockDetail = product;
      $http.post("/api/v1/public_document/get_product_stock_details" , params)
      .then(function(ans) {
        fct.stock = ans.data;
        moment.locale('it');

        for (var i = 0; fct.stock.global && i < fct.stock.global.length; i++) {
          fct.stock.global[i].incoming_date_formatted = moment(fct.stock.global[i].incoming_date).format('mmmm yyyy'.toUpperCase());
        };

        for (var i = 0; fct.stock.customer && i < fct.stock.customer.length; i++) {
          fct.stock.customer[i].incoming_date_formatted = moment(fct.stock.customer[i].incoming_date).format('mmmm yyyy'.toUpperCase());
        };

        $('#stockDetailModal').modal('show');

        $('#stockDetailModal').on('hidden.bs.modal', function (e) {
            // reset stock
            fct.currentStockDetail = null;
            fct.stock = null;
            $('#stockDetailModal').off('hidden.bs.modal');
        });  
      });  
    },
    
    getProductCurrentQuantity: function(product) {     
      return fctCart.getProductQuantity(product.id);
    },

    getProductShipDate: function(productID) {     
      return fctCart.getProductShipDate(productID);
    },

    addSelectedProducts: function(list, removed, cb) {
      // in questo caso è la stored che capisce se un detail è da aggiungere o da togliere in base alla quantità
      var totalList = list;
      if(removed)
        var totalList = list.concat(removed);

      fctCart.addProductListToCart(totalList, cb);
    },
    getModalHeader: function(){
      return fct.modalHeaders[fct.environment];
    },
    getModalFooter: function(){
      return fct.modalFooters[fct.environment];
    },

    isValidShipDate: function(date) {
        var now = new Date();
        if(!date) date = now;
        if(date) {
            
            var ship_date = new Date(setDateForDB(date));
            if(ship_date <= now) {
                return 'Data inserita non valida!';
            } else {
                return 'VALID';
            }
        }
    },

    hideMassiveDiscount: true,

    setShipDateError: function(err) {
      fct.shipDateError = err;
    },
      
    sizeColorData: {
      items: null,
      ship_date: null,
      selectedItem: null
    },

    canUserBuy: function() {
      return ($rootScope.getSettingBooleanValue('catalog.anonymous_cart') || !$rootScope.logged_user.user.is_anonymous)
               && !$rootScope.logged_user.hasFunctionality('hide.catalog.commerce');
    },

    cartDisabled: function() {
      return !$rootScope.logged_user.hasFunctionality('public.cart.access') && (!$rootScope.logged_user.user.customer_id);
    },

    openNewDocumentWizard: function(event) {
      $('#newDocumentWizard').on('hidden.bs.modal', function (e) {
        fct.newDocModal = false;
        $('#newDocumentWizard').off('hidden.bs.modal');
      });

      fct.newDocumentStep = 1;
      $http.get("/api/v1/public/get_creatable_document_types")
      .then(function(types) {
        if(types && types.data)
          fct.documentTypes = types.data;

        if(fct.documentTypes && fct.documentTypes.length > 1) {
          fct.newDocModal = true;
          $("#newDocumentWizard").modal('show');
        } else {
          if(fct.documentTypes && fct.documentTypes.length == 1) {
            fct.documenttype_id = fct.documentTypes[0].documenttype_id;
            // se l'utente è un agente deve avere la possibilità di scegliere il customer
            if($rootScope.logged_user.hasFunctionality('document.choose_customer')) {
              fct.newDocModal = true;
              $("#newDocumentWizard").modal('show');
              fct.newDocumentStep = 2;
            } else {
              fct.getOpenedDocuments();
            }
          } else {
            // errore
          }
        }
      });
    },

    prevHidden: function() {
      switch(fct.newDocumentStep) {
        case 1:
          return true;
          break;
        case 2:
          if(fct.documentTypes.length > 1)
            return false;
          break;
        case 3:
          if($rootScope.logged_user.hasFunctionality('document.choose_customer'))
            return false;
          break;
      }
      return true;
    },

    prevDocumentStep: function() {
      if(fct.newDocumentStep == 2 && fct.documentTypes.length > 1) {
        fct.newDocumentStep--;
        return;
      }
      if(fct.newDocumentStep == 3 && $rootScope.logged_user.hasFunctionality('document.choose_customer')) {
        fct.newDocumentStep--;
        return;
      }
    },

    selectedDocumentCustomer: function(f) {
      // selezione cliente
      if(f) 
        customerID = f.customer_id;

      if(f && f.blocked == 1) {
        $('#customerBlocked').modal('show');
      } else {
          fct.customerID = customerID;
          fct.getOpenedDocuments();
      }
    },

    getOpenedDocuments: function() {
      // se il customer non è selezionato da interfaccia allora l'utente è già un cliente e sarà usato quello associato
      var customerID = fct.customerID;
      if(!fct.customerID || fct.customerID == undefined) 
        customerID = -1;

      var par = {
        documenttype_id: fct.documenttype_id,
        customer_id: customerID
      };
      $http.post("/api/v1/public/get_user_open_documents", par)
      .then(function(response) {
        // se l'utente non ha già documenti aperti creo direttamente un nuovo documento
        if(!response || !response.data || response.data.length == 0) {
          fct.directDocumentGeneration();
        } else {
          fct.openedDocuments = response.data;
          fct.newDocumentStep = 3;

          if(!fct.newDocModal)
            $("#newDocumentWizard").modal('show');
        }
      });
    },

    directDocumentGeneration: function() {
      for (var i = 0; i < fctCart.list.length; i++) {
        fctCart.list[i].outline_number = i.toString();
      };

      var customerID = fct.customerID;
      if(!fct.customerID || fct.customerID == undefined) 
        customerID = -1;

      var par = {
        'products': fctCart.list,
        'customer_id': customerID
      };

      var promise = $http.post("/api/v1/public/direct_document_generation" , par)
      .then(function(response) {
        if(response.data && response.data.document_id) {
          // documento creato con successo, reindirizzo a sellmore web
          $location.path('/' + $route.current.params.lang + '/documents/' + response.data.document_id)
        } else {
          // segnalazione errore
          fctCart.openCartAlert('GENERAL_DOCUMENT_CREATION_ERROR', 'Si è verificato un errore durante la creazione del documento...', true);
        }
      });
    },

    selectDocument:  function(doc) {
      if(!fct.selectedDocument){
        fct.selectedDocument = doc;
        doc.selected = true;
        return;
      }

      if(fct.selectedDocument.document_id == doc.document_id) {
        doc.selected = false;
        fct.selectedDocument = null;
      } else {
        for (var i = 0; fct.openedDocuments && i < fct.openedDocuments.length; i++) {
          fct.openedDocuments[i].selected = false;
        };
        fct.selectedDocument = doc;
        doc.selected = true;
      }

      //$("#documentSelectionConfirm").modal('show');
    },

    chosenDocument: function() {
      var par = {
        'products': fctCart.list,
        'documentID': fct.selectedDocument.document_id
      };

      var promise = $http.post("/api/v1/public/direct_document_update" , par)
      .then(function(response) {
        if(response.data && response.data.document_id) {
          // documento creato con successo, reindirizzo a sellmore web
          $location.path('/' + $route.current.params.lang + '/documents/' + response.data.document_id);
        } else {
          // segnalazione errore
          fctCart.openCartAlert('GENERAL_DOCUMENT_CREATION_ERROR', 'Si è verificato un errore durante la creazione del documento...', true);
        }
      });
    },

    toggleOnlyPublicPrice: function() {
      fct.onlyPublicPriceMode = !fct.onlyPublicPriceMode;
      writeCookie('publicsite_only_public_price', '/', fct.onlyPublicPriceMode);
    }
  }; 
  return fct;
}])

.directive('actionSinglePopovers', function($document,$rootScope,$timeout,$popover) {
  return {
    restrict: 'EA',
    link : function(scope, element, attrs) {
      var $element = $(element);
      $element.click(function() {
        $('.popover').each(function() {
          var $this = $(this);
          if($this.attr('id') != $element.attr('id')){
            $this.scope().$hide();
          }
        });
      });
    }
  }
})

.directive('fallbackPlaceholder', function () {
  var fallbackPlaceholder = {
    link: function postLink(scope, element, attrs) {
      element.bind('error', function() {
        $(this).hide(); 
        $('#' + attrs.fallbackPlaceholder + '-img-placeholder').show();
      });
    }
   }
   return fallbackPlaceholder;
})
