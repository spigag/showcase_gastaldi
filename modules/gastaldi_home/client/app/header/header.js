angular.module('header-module',
  ['ngRoute',
  'ngCookies',
  'services.i18nNotifications',
  'directives.gravatar',
  'ui.directives',
  'security.login.toolbar',
  'mgcrea.ngStrap',
  'ngAnimate',
  'ui.bootstrap',
  'public-menu'
])

.controller('headerCtrl',['$scope','$window','$route','i18nNotifications','$location', 'security', 'moxutils', 'publicMenuService', 'fctUserDivisions',
    function ($scope, $window, $route, i18nNotifications, $location, security, moxutils, publicMenuService, fctUserDivisions) {
      $scope.menu = null;
      
      $scope.userDivisions = fctUserDivisions;
      
      publicMenuService.getMenu('HEADER').then(function(result){
        $scope.menu = result;
        $scope.showLogin = true;
      });

      $scope.changeDivision = function(new_division_id, ev) {
        ev.preventDefault();

        writeCookie('active_division', '/', new_division_id);

        $window.location = '/' + $scope.logged_user.user.url_culture + '/fast-search/' + new_division_id;

        /*
        $scope.divisionid = new_division_id;

        fctUserDivisions.loadDivision(new_division_id)
        .then(function(ans) {
          if(ans.data)
            $scope.division_data = ans.data;
          else
            $scope.division_data = {};

          writeCookie('active_division', '/', new_division_id);

          $window.location = '/' + $scope.logged_user.user.url_culture + '/fast-search/' + new_division_id;
        });
*/
      };
      
      $scope.$on('$routeChangeSuccess', function() {
        $scope.isAuthenticated = security.isAuthenticated;
        $scope.login = security.showLogin;
        $scope.logout = security.logout;
        $scope.$watch(function() {
          return security.currentUser;
        }, function(currentUser) {
          $scope.currentUser = currentUser;
        });
      });

      $scope.get_new_culture_url = function(culture_code){
        var current = $location.url();
        return current.replace(current.substr(1, 2), culture_code);
      };
        
      $scope.isAuthenticated = security.isAuthenticated;

      $scope.user_loaded = function(){
          return security.userRequested;
      };

      // funzioni che ridirezionano al login/admin: è necessario un reload della pagina (tramite window.location)
      // in quanto angular 'confonde' la route della homepage (/:lang) con le route /admin e /login
      // (settando il parametro lang erroneamente). l'utilizzo di queste funzioni risolve il problema
      $scope.goToLogin = function(ev) {
        ev.preventDefault();
        $window.location = '/login';
      };
      $scope.goToAdministration = function(ev) {
        ev.preventDefault();
        $window.location = '/admin';
      };

      $scope.getWebSiteLink = function() {
        var url = $scope.firm.addresselement.web;
        if(url.substring(0, 7) == "http://")
          return url;
        else
          return "http://" + url;
      };
  }]
);