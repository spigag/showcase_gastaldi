angular.module('gastaldi-home-module',['ngRoute','ngCookies','services.i18nNotifications','directives.gravatar','ui.directives','module-paging', 'slickCarousel'])

.directive('disableNgAnimate', ['$animate', function($animate) {
  return {
    restrict: 'A',
    link: function(scope, element) {
      $animate.enabled(false, element);
    }
  };
}])

.directive('scrollSpyCompare', function($timeout){
  return function(scope, elem, attr) {
    $timeout(function() { 
      $('body').scrollspy({ target: '#compare-tab' });
      var tab = $('#compare-tab');
      tab.affix({
        offset: {
            top: function() {
                var c = tab.parent().offset().top, d = 0, e = 60;
                return this.top = c - e - d;
            },
            bottom: function() {
                return this.bottom = 0;
            }
        }
      });
    }, 1000);
  }
})

.directive('repeatStarted', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      if (scope.$index > 0) {
        $timeout(function () {
          scope.$emit('ngCompareRepeatStarted');
        });
      }
    }
  }
})

/* controller per la home con la selezione divisione */
.controller('gastaldiDivisionsCtrl',['$scope','$location','divisions',
  function ($scope, $location, divisions) {
    $scope.divisions = divisions;
    
    $scope.getDivisionCssClass = function(divid) {
      return "division-standard";
    };

    $scope.getDivisionCode = function(name) {
      return name.toLowerCase().replace(' division', '');
    }
    
    $scope.$on('$viewContentLoaded', function() {
      // risoluzione problema zoom su passaggio di pagine senza reload
      $('.zoomContainer').remove();
    });
}])

.controller('gastaldiHomeCtrl',
  ['$scope',
  '$route',
  '$window',
  '$rootScope',
  'i18nNotifications',
  '$http',
  '$timeout',
  '$location',
  'moxutils',
  'fctFixedWords',
  'fctHomeConfig',
  'fctProduct',
  'fctCart',
  'fctBuyInterface',
  'fctHomeFactory',
  'fctGastaldiFacets',
  'facets',
  'homepageData',
  'cart',
  'division',
  '$sce',
  function ($scope,
    $route,
    $window,
    $rootScope,
    i18nNotifications,
    $http,
    $timeout,
    $location,
    moxutils,
    fctFixedWords,
    fctHomeConfig, 
    fctProduct,
    fctCart,
    fctBuyInterface, 
    fctHomeFactory, 
    fctGastaldiFacets,
    facets,
    homepageData,
    cart,
    division,
    $sce
  ) {
    $scope.onLoading = false;
    $scope.cart = fctCart;

    $scope.fctBuyInterface = fctBuyInterface;
    $scope.fctHomeFactory = fctHomeFactory;
    $scope.fctGastaldiFacets = fctGastaldiFacets;

    $scope.getFixedWord = fctFixedWords.getFixedWord;

    $scope.showSecondaryForm = false;
    $scope.dataLoaded = false;
    $scope.formFacets = {};
    $scope.selectedProduct = {};
    $scope.sliderCollapsed = false;

    $scope.homepageData = homepageData;
    $scope.divisionData = division.data;

    $scope.showVDBMessage = false;
    if($scope.divisionData.division_code == "VDB") {
      $scope.showVDBMessage = true;
    }

  

    $scope.toggleSlider = function($event){
      $scope.sliderCollapsed = !$scope.sliderCollapsed;
      sessionStorage.setItem("sliderCollapsed", $scope.sliderCollapsed);
    }

    writeCookie('active_division', '/', $route.current.params.divid);
    $rootScope.divisionid = $route.current.params.divid;

    $scope.$on('$viewContentLoaded', function() {
      var isSliderCollapsed = sessionStorage.getItem('sliderCollapsed');
      $scope.sliderCollapsed = isSliderCollapsed && (isSliderCollapsed == 'true') ? true : false;

      if (!$scope.sliderCollapsed && !$('#banner-container').hasClass('in')) {
        $('#banner-container').collapse('show');
      }

      if($scope.homepageData.banner && $scope.homepageData.banner.length){
        $scope.slickConfig = {
          lazyLoad: 'progressive',   
          dots: true,
          infinite: true,
          speed: 300,
          autoplay: true,
          autoplaySpeed: $scope.homepageData.banner[0].timer,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [
            {
              breakpoint: 1170,
              settings: {
                slidesToShow: 4, 
                slidesToScroll: 4
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        };
        
        if($rootScope.logged_user.hasFunctionality("gastaldi.new_home")){
          $scope.slickConfig.slidesToShow = 7;
          $scope.slickConfig.slidesToScroll = 7;
        }
      }

      $timeout(function() {
        $('#mainSearchInput').focus();
      }, 100);  
    });

    $scope.sce = $sce;
    $scope.slideCurrentAltFile = '';
    $scope.setCurrentAltFile = function(event, file){
      event.preventDefault();
      $scope.slideCurrentAltFile = file;
      $('#slideAltFileModal').modal('show');
    }

    $scope.mainForm = {
      'size': '',
      'facets': {
        'brand': -1,
        'sector': -1,
        'genre': -1,
      },
      'stock': true
    };

    $scope.secondaryForm = {
      'size': '',
      'facets': {
        'brand': -1,
        'sector': -1,
        'genre': -1,
      },
      'stock': true
    };

    $scope.productListOne = [];
    $scope.productListTwo = [];

    $scope.toggleSecondaryForm = function(){
      if($scope.productListTwo.length || $scope.listTwoNotFound)
        return;

      $scope.showSecondaryForm = !$scope.showSecondaryForm;
    };
    
    var data = {};
    for(var i = 0; i < fctGastaldiFacets.list.length; i++){
      if(fctGastaldiFacets.list[i].facets.length == 2){
        var positiveId = 0;
        angular.forEach(fctGastaldiFacets.list[i].facets, function(value, key) {
          if(value.facet_value == "true"){ 
            positiveId = value.facet_id;
          }
        });
        data[fctGastaldiFacets.list[i].code] = { 'field_id' : positiveId, 'facet_text' : fctGastaldiFacets.list[i].facet_text };
      } else {
        data[fctGastaldiFacets.list[i].code] = fctGastaldiFacets.list[i];
      }
    } 
    $scope.formFacets = {
      'brands' : data["MARCA"],
      'genre' : data["GENERE"],
      'sector' : data["SETTORE"],
      'reinforced': data["RINFORZATO"],
      'summer': data["ESTIVO"],
      'winter': data["INVERNALE"],
      'seasonal': data["QUATTRO_STAGIONI"],
      'promo': data["IN_PROMO"],
      'wheel': data["CERCHIO"],
      'run_on_flat': data["RUN_ON_FLAT"],
      'flap': data["CAMERA_FLAP"],
    };

    $scope.stockLabel = 'Solo articoli disponibili';
    $scope.dataLoaded = true;

    // genero un dizionario di marchi per associare il nome al codice che mi arriva dal database
    $scope.brandDictionary = {};
    for (var i = 0; i < $scope.formFacets.brands.facets.length; i++) {
      $scope.brandDictionary[$scope.formFacets.brands.facets[i].facet_id] = $scope.formFacets.brands.facets[i].facet_value;
    };

    $('#splashModal').appendTo("body");
    if(!$rootScope.firm) {
      $rootScope.$on('showcaseLoadedFirm', function() {
        if($rootScope.firm.splash_screen && $rootScope.firm.splash_screen.full_image_url) {
          $timeout(function() {
            $('#splashModal').firstVisitPopup({
              modalID: '#splashModal',
              cookieName : 'homepage',
              showAgainSelector : ''
            });
          }, 1200);
        }
      });
    } else {
      if($rootScope.firm.splash_screen && $rootScope.firm.splash_screen.full_image_url) {
        $timeout(function() {
          $('#splashModal').firstVisitPopup({
            modalID: '#splashModal',
            cookieName : 'homepage',
            showAgainSelector : ''
          });
        }, 1200);//1 secondo è necessario perché altrimenti il modale (per una qualche ignota ragione, probabilmente un'altro caricamento dati, non viene visualizzato)
      }
    }

    $scope.onSubmit = function() {
      if(fctHomeFactory.loadingSearch)
        return;
      
      fctHomeFactory.loadingSearch = true;
      $scope.resetLists();
      var params = {};
      fctGastaldiFacets.resetSearchLabels('mainForm');
      fctGastaldiFacets.resetSearchLabels('secondaryForm');


      /*Richiesta di Matteo il 08-06-2022 --> usare sempre la modalità stampa*/
      if($rootScope.logged_user.hasFunctionality("gastaldi.new_home") && fctHomeFactory.alwaysPrintForNewSearch){
        $scope.printModeOne = true;
        $scope.printModeTwo = true;
        fctHomeFactory.mainPaging.current = -1;
        fctHomeFactory.secondaryPaging.current = -1;
      }
      /**
       * Form 1 search
       */
      var searchData = {
        main : $scope.mainForm
      };

      params.facetFilter = fctGastaldiFacets.getSearchInput('mainForm', $scope.mainForm.facets, $scope.formFacets);

      if($scope.mainForm.size.length){
        params.freeSearch = $scope.mainForm.size;
        fctGastaldiFacets.searchLabels.mainForm.push({'type': 'free', 'value' : 'Misura: ' + params.freeSearch});
      }

      if($scope.mainForm.stock){
        params.stockFilter = $scope.mainForm.stock;
        fctGastaldiFacets.searchLabels.mainForm.push({'type': 'check', 'value' : $scope.stockLabel});
      }

      fctHomeFactory.mainPayload = params;

      fctHomeFactory.doSearch(fctHomeFactory.mainPaging, fctHomeFactory.mainPayload , true)
      .success(function(result) {
        if(result.length) {
          $scope.productListOne = $scope.productListOne.concat(result);
          $scope.listOneNotFound = false;
          fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
        } else {
          $scope.listOneNotFound = true;
          fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
        }
      })
      .error(function(err) {
        $scope.listOneNotFound = true;
        fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
      });

      /**
       * Form 2 search
       */
      if($scope.showSecondaryForm){
        params = {};
        params.facetFilter = fctGastaldiFacets.getSearchInput('secondaryForm', $scope.secondaryForm.facets, $scope.formFacets);
        if($scope.secondaryForm.size.length){
          params.freeSearch = $scope.secondaryForm.size;
          fctGastaldiFacets.searchLabels.secondaryForm.push({'type': 'free', 'value' : 'Misura: ' + params.freeSearch});
        }
        if($scope.secondaryForm.stock){
          params.stockFilter = $scope.secondaryForm.stock;
          fctGastaldiFacets.searchLabels.secondaryForm.push({'type': 'check', 'value' : $scope.stockLabel});
        }
        
        fctHomeFactory.secondaryPayload = params;

        searchData.secondary = $scope.secondaryForm;

        fctHomeFactory.doSearch(fctHomeFactory.secondaryPaging, fctHomeFactory.secondaryPayload , true)
        .success(function(result){
          if(result.length){
            $scope.productListTwo = $scope.productListTwo.concat(result);
            $scope.listTwoNotFound = false;
            fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
          } else {
            $scope.listTwoNotFound = true;
            fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
          }
        })
        .error(function(err){
          $scope.listTwoNotFound = true;
          fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
        });
      } 
      $rootScope.$broadcast('fastSearch', searchData);
    };

    $scope.nextPage = function(list, paging, payload) { 
      if(list == 'main') {        
        if(fctHomeFactory.mainPaging.noMoreData) return;
      } else {
        if(fctHomeFactory.secondaryPaging.noMoreData) return;
      }

      fctHomeFactory.doSearch(paging, payload, false)
      .success(function(result){
        if(list == 'main')
          $scope.productListOne = $scope.productListOne.concat(result);
        else
          $scope.productListTwo = $scope.productListTwo.concat(result);
      });
    }; 

    $scope.getCurrentList = function(list) {
      if(list == 'main')
        return $scope.productListOne;
      else
        return $scope.productListTwo;
    };

    $scope.resetLists = function() {
      $scope.productListOne = [];
      $scope.productListTwo = []; 
      fctHomeFactory.searched = false;

      $scope.printModeOne = false;
      fctHomeFactory.mainPaging.totalSize = 0;
      fctHomeFactory.mainPaging.current = 1;
      fctHomeFactory.mainPaging.noMoreData = false;
      fctHomeFactory.mainPaging.loadingData = false;

      $scope.printModeTwo = false;
      fctHomeFactory.secondaryPaging.totalSize = 0;
      fctHomeFactory.secondaryPaging.current = 1;
      fctHomeFactory.secondaryPaging.noMoreData = false;
      fctHomeFactory.secondaryPaging.loadingData = false;

      if($rootScope.logged_user.hasFunctionality("gastaldi.new_home") && fctHomeFactory.alwaysPrintForNewSearch){
        $scope.printModeOne = true;
        $scope.printModeTwo = true;
      }
    };
    
    $scope.toggleSearch = function() {
      $scope.listOneNotFound = false; 
      $scope.listTwoNotFound = false; 
      $scope.resetLists();

      $timeout(function() {
        $('#mainSearchInput').focus();
      }, 10);  
    };


    $scope.togglePrintMode = function(formID) {
      switch(formID) {
        case 1: 
          $scope.printModeOne = !$scope.printModeOne;

          if($scope.productListOne.length < fctHomeFactory.mainPaging.totalSize) {
            fctHomeFactory.loadingSearch = true;
            fctHomeFactory.mainPaging.current = -1;
            fctHomeFactory.doSearch(fctHomeFactory.mainPaging, fctHomeFactory.mainPayload)
            .success(function(result) {
              if(result.length) {
                $scope.productListOne = result;
                $scope.listOneNotFound = false;
                fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
              } else {
                $scope.listOneNotFound = true;
                fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
              }
            });
          }
          break;
        case 2: 
          $scope.printModeTwo = !$scope.printModeTwo;

          if($scope.productListTwo.length < fctHomeFactory.secondaryPaging.totalSize) {
            fctHomeFactory.loadingSearch = true;
            fctHomeFactory.secondaryPaging.current = -1;
            fctHomeFactory.doSearch(fctHomeFactory.secondaryPaging, fctHomeFactory.secondaryPayload)
            .success(function(result){
              if(result.length){
                $scope.productListTwo = result;
                $scope.listTwoNotFound = false;
                fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
              } else {
                $scope.listTwoNotFound = true;
                fctHomeFactory.loadingSearch = fctHomeFactory.mainPaging.loadingData || fctHomeFactory.secondaryPaging.loadingData;
              }
            });
          }
          break;
        default:
        break;
      }
    };

    $scope.forceReload = function (ev, id) {
      ev.preventDefault();
      $window.location = '/' + $scope.logged_user.user.url_culture + '/item/' + id;
    };

    fctHomeConfig.load().then(function(data) {
      $scope.homeConfig = data;
    });

    $scope.getIcon = function(item, tp) {
      if(item){
        if(isNaN(item)){
          switch(tp){
            case 'consumo':
              return "/static/images/consumo " + item + ".jpg"; 
              break;
            case 'aderenza':
              return "/static/images/aderenza " + item + ".jpg"; 
              break;
            default:
              return "/static/images/" + item + ".gif"; 
              break
          }
          
        } else {
          return "/static/images/etich3_" + item + ".gif";
        }
      } 
    };

    $scope.openProductImage = function(product){
      $scope.currentImage = {
        "zoom_image": product.zoom_image
      };
      $("#productImageModal").modal("show");
    }

    $scope.closeProductImage = function(product){
      $scope.currentImage = null;
      $("#productImageModal").modal("hide");
    }

    $scope.openProductDetail = function(product){
      $scope.currentProduct = product;
      $("#productDetailModal").modal("show");
      $scope.loadingPrices = true;
      fctHomeFactory.setSelectedProduct(product, function() {
        $scope.loadingPrices = false;
      });
    }

    $scope.closeProductDetail = function(product){
      $scope.currentProduct = null;
      $("#productDetailModal").modal("hide");
    }

    $scope.setSelectedProduct = function(product) {
      $scope.currentImage = {};
      fctHomeFactory.setSelectedProduct(product, function() {
        $scope.currentImage.image = $scope.getProductImage(fctHomeFactory.selectedProduct);
        $scope.currentImage.zoom_image = $scope.getProductZoomModalImage(fctHomeFactory.selectedProduct);


        $("#product-image").bind('click', function(e) { 
          var gallery = [{
            href: $scope.currentImage.zoom_image
          }];
          for (var i = 0; fctHomeFactory.selectedProduct.extra && fctHomeFactory.selectedProduct.extra.additionalimages && i < fctHomeFactory.selectedProduct.extra.additionalimages.length; i++) {
            gallery.push(fctHomeFactory.selectedProduct.extra.additionalimages[i].zoom_image);
          };
          $.fancybox(gallery);
        });

        $('#productModal').modal('show');
      });
    }; 

    /*** immagine prodotto in dettaglio / immagini aggiuntive */
    $scope.getProductImage = function(product) {
      if (product == undefined || product == null || product.image == undefined || product.image == null || product.image.length == 0) {
        return "/static/img/publicsite/product_placeholder.jpg";
      }
      return product.image;
    };

    $scope.getProductZoomModalImage = function(product) {
      return product.zoom_image?product.zoom_image:product.image;
    };

    $('#productModal').on('hidden.bs.modal', function() {  
      $scope.currentImage = {};
      fctHomeFactory.selectedProduct = null;
      $scope.$digest();
    });
  
    $scope.setCurrentImage = function(image) {
      $scope.currentImage.image = image.image;
      $scope.currentImage.zoom_image = image.zoom_image;
    };

    $scope.resetCurrentImage = function() {
      $scope.currentImage.image = $scope.getProductImage(fctHomeFactory.selectedProduct);
      $scope.currentImage.zoom_image = $scope.getProductZoomModalImage(fctHomeFactory.selectedProduct);
    };
    
    $scope.checkProductImageVisibility = function() {
      return ($scope.currentImage.image != $scope.getProductImage(fctHomeFactory.selectedProduct)) 
            && ($scope.currentImage.zoom_image != $scope.getProductZoomModalImage(fctHomeFactory.selectedProduct));
    };

    $scope.checkAdditionalImageVisibility = function(image) {
      return ($scope.currentImage.image != image.image) 
            && ($scope.currentImage.zoom_image != image.zoom_image);
    };

    $scope.hasImage = function(product) {
      if (product == undefined || product == null || product.image == undefined || product.image == null || product.image.length == 0) {
        return false;
      } else return true;
    };  
    
    var detectIE = function() {
      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          // Internet explorer fino alla versione 10;
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {
          // Internet explorer dalla versione 11
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      }
      
      return false;
    };    

    $scope.showPDFSliderPreview = function(pdfURL) {
      $('#slidePDFpreviewModal').modal('show');  

      $timeout(function(){
        var param = {id:"pdfEmbed", forcePDFJS: true, PDFJS_URL: pdfURL};
        $scope.currentPdfUrl = pdfURL;
        //per poter stampare su tutti i browser devo caricare il pdf in modi differenti: 
        //per chrome dentro un iframe (quindi con i parametri definiti nell'inizializzazione di parm)
        //per ie dentro un elemento embed
        if(detectIE()) {
            delete param.forcePDFJS;
            delete param.PDFJS_URL;
        } 
        $timeout(PDFObject.embed(pdfURL, "#sliderPreviewPdf", param));
      }); 
    };  
  }]
)

.factory('fctHomeFactory', ["$http", "$route", "fctGastaldiFacets", "$rootScope", function ($http, $route, fctGastaldiFacets, $rootScope) {
  var fct = {
      selectedProduct: null,
      alwaysPrintForNewSearch: false,
      mainPaging: {
        totalSize : 0,
        size: 10,
        current: 1,
        noMoreData: false,
        loadingData: false
      },
      secondaryPaging: {
        totalSize : 0,
        size: 10,
        current: 1,
        noMoreData: false,
        loadingData: false
      },

      setSelectedProduct: function(product, cb){
        var promise = $http.get("/api/v1/home/product_extra/" + product.id)
        .success(function(extra){
          product.extra = extra;
          fct.selectedProduct = product;

          if(cb) cb();

          return extra;
        })
        .error(function(err){
          console.log(err);
        });
        return promise;
      },

      searched: false,
      /*
      load: function() {
        var url = "/api/v1/home/search";
        if($rootScope.logged_user.hasFunctionality("gastaldi.new_home")){
          url = "/api/v2/home/search";
        }
        var promise = $http.get(url)
          .success(function(response){
            return response.data;
          })
          .error(function(err){
              console.log(err);
          });
        return promise;
      },*/

      doSearch: function(paging, payload, reset) {
        if(reset) {
          paging.current = 1;
          paging.noMoreData = false;
          paging.loadingData = false;
        }

        // evito il caricamento inutile controllando i flag di sistema
        if(paging.loadingData || paging.noMoreData) {
          return;
        }

        paging.loadingData = true;
        fct.searched = true;

        var url = "/api/v1/home/search";
        if($rootScope.logged_user.hasFunctionality("gastaldi.new_home")){
          url = "/api/v2/home/search";

          if(reset && fct.alwaysPrintForNewSearch){
            paging.current = -1;
          }
        }
        
        var data = {
          "division_id": $route.current.params.divid,
          "user_search" : payload.freeSearch,
          "stock_filter" : payload.stockFilter,
          "selected_facets" : payload.facetFilter,
          "page_index" : paging.current,
          "page_size" : paging.size
        };

        var promise = $http.post(url, data)
        .success(function(response){
          if(response) {
            // gestione versione stampabile: se current negativo allora ho caricato tutti i dati (noMoreData va a true), altrimenti procedo con la normale gestione
            if(paging.current > 0) {
              paging.current += 1;
            } else {
              paging.noMoreData = true;
            }

            if(response.length) {
              if(+response[0].number_of_records)//il numero di record viene restituito solo nella prima pagina
              paging.totalSize = response[0].number_of_records;
            } else {
              // se l'ultimo caricamento non ha tornato risultati so che non devo più fare caricamenti
              paging.noMoreData = true;
              paging.totalSize = 0;
            }

            if(response.length < paging.size) {
              paging.noMoreData = true;
            }
            paging.loadingData = false;

            if($rootScope.getSettingBooleanValue('splash.show_on_search') && paging.current <= 2)
              $('#splashModal').modal('show');
              
            return response;
          }
        })
        .error(function(err){
          paging.loadingData = false;
          return false;
        });
        return promise; 
      },

  };
  return fct;
}])


.factory('fctProductList', [function() {
  var fct = {
    noMoreData: false,
    loadingData: false
  };
  return fct;
}])

.factory('fctGastaldiFacets', ["$http", "moxutils", "fctFixedWords", function ($http, moxutils, fctFixedWords){
  var fct = {
    list: {},

    onLoadFacets : {
      "useOnLoadData" : true,
      "facetsStatus" : "[]",
      "facetsFilter" : ""
    },

    useCookies: true,
    searchLabels: {
      'mainForm' : [],
      'secondaryForm': []
    },

    getFacets: function(){
      var promise = $http.get("/api/v1/gastaldi/facets")
        .success(function(data){  
          fct.list = data;
          return data;
        })
        .error(function(err){
            console.log(err);
        });
      return promise;
    },

    resetSearchLabels: function(formId){
      fct.searchLabels[formId] = [];
    },

    getSearchInput: function(formId, data, model){
    
      var result = facetFilter = "";
      var index = 0;
      var objLength = Object.keys(data).length;
      fct.resetSearchLabels(formId);

      angular.forEach(data, function(value, key) {
        if(value){
          obj = angular.fromJson(value);
          if(typeof obj === "boolean"){
            facetFilter += model[key].field_id +'&';
            fct.searchLabels[formId].push({'type': 'check', 'value' : model[key].facet_text});
          } else if(obj != -1) {
            facetFilter += obj.facet_id +'&'; 
            fct.searchLabels[formId].push({'type': 'select', 'value' : obj.facet_value});
          }
        }
      }); 
      if(facetFilter.length)
        result += '(' + facetFilter.substring(0, facetFilter.length-1) + ')';
      return result;
    },
  };
  return fct;
}]);
