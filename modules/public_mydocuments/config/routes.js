module.exports.routes = {
	/* routes senza parametro lang: l'utente sarà ridirezionato alle nuove routes: necessarie per il redirect post login */
	'/mydocuments': {
		controller: 'mPublicMyDocumentsController',
		action: 'initializeView'
	},
	'/mydocuments/:did': {
		controller: 'mPublicMyDocumentsController',
		action: 'initializeView'
	},
	'/mydocuments/:did/edit': {
		controller: 'mPublicMyDocumentsController',
		action: 'initializeView'
	},
	/* ********** publicsite routes ********** */
	'/:lang/mydocuments': {
		controller: 'mPublicMyDocumentsController',
		action: 'initializeView'
	},
	'/:lang/mydocuments/:did': {
		controller: 'mPublicMyDocumentsController',
		action: 'initializeView'
	},
	'/:lang/mydocuments/:did/edit': {
		controller: 'mPublicMyDocumentsController',
		action: 'initializeView'
	},
	/* generic routes */
	'post /api/v1/public/get_mydocuments_list': {
		controller: 'PublicMyDocumentsMiscController',
		action: 'get_mydocuments_list'
	},
	// customization for document load end-point
	'post /api/v1/public/load_my_document': {
		controller: 'gastaldi/GastaldiController',
		action: 'load_document'
	},
	'post /api/v1/public/copy_document_in_cart': {
		controller: 'PublicMyDocumentsMiscController',
		action: 'copy_document_in_cart'
	}
}