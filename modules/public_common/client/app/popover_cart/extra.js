angular.module('public_cart_extra', [])

.run(['fctCart', 'fctFixedWords', '$rootScope', '$location', 'fctUserAuthorization', '$timeout',
function(fctCart, fctFixedWords, $rootScope, $location, fctUserAuthorization, $timeout) {
	// modulo per settings/customizzazioni cart
  fctCart.back = function($event) {
    $event.preventDefault();

    var lastURL = readCookie('referrer', '/');
    if(lastURL && lastURL.split('/').indexOf('cart') < 0) {
    	window.history.back();
    } else {
      $location.path('/' + $rootScope.logged_user.user.url_culture + '/fast-search');
    }
  };

  fctCart.getBackLabel = function() {
    var lastURL = readCookie('referrer', '/').split('/');

    if(lastURL.indexOf('division') >= 0 || lastURL.indexOf('divisions') >= 0) {
      return fctFixedWords.getFixedWord('CART_BACK_TO_CATALOG'); 
    }
    if(lastURL.indexOf('fast-search') >= 0) {
      return fctFixedWords.getFixedWord('CART_BACK_TO_SEARCH', 'Torna alla ricerca'); 
    }
    return fctFixedWords.getFixedWord('GENERAL_BACK', 'Indietro'); 
  };
 
  fctCart.customConfirm = function() {
    $timeout(function() {
      fctUserAuthorization.load();
      fctCart.processingStep = false;
    }, 100);
  };
}]);