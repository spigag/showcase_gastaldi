angular.module('catalog_sorting_rule-module',['ngRoute','services.crud','services.i18nNotifications','directives.gravatar','ui.directives','module-paging'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  /****  ROUTES  ****/ 
  .when('/admin/catalog_sorting_rule', {
    templateUrl:'catalog_sorting_rule/catalog_sorting_rule-list.tpl.html',
    controller:'sortingRuleListCtrl',
    resolve: {
      data: ['fctSortingRule', function(fctSortingRule) {
        return fctSortingRule.getAll();
      }]
    },
    reloadOnSearch: false
  }) 
  .when('/admin/catalog_sorting_rule/new', {
    templateUrl:'catalog_sorting_rule/catalog_sorting_rule-edit.tpl.html',
    controller:'sortingRuleEditCtrl',
    resolve: {
      item: ['$route', 'fctSortingRule', function($route, fctSortingRule) {
        return {};
      }],
      facets: ['fctGastaldiFacets', function(fctGastaldiFacets) {
        return fctGastaldiFacets.getFacets();
      }]
    },
    reloadOnSearch: false
  }) 
  .when('/admin/catalog_sorting_rule/:id', {
    templateUrl:'catalog_sorting_rule/catalog_sorting_rule-edit.tpl.html',
    controller:'sortingRuleEditCtrl',
    resolve: {
      item: ['$route', 'fctSortingRule', function($route, fctSortingRule) {
        return fctSortingRule.getByID($route.current.params.id, true);
      }],
      facets: ['fctGastaldiFacets', function(fctGastaldiFacets) {
        return fctGastaldiFacets.getFacets();
      }]
    },
    reloadOnSearch: false
  })
}])

.factory('fctSortingRule', ["$http", "fctGastaldiFacets", function ($http, fctGastaldiFacets) {
  var fct = {
    paging: {
      totalSize : 0,
      size: 20,
      current: 0
    },

    list: [],

    reset: function() {   
      fct.list = [];
      fct.paging.current = 0;
      fct.noMoreData = false;
    },

    getAll: function(reset) {
      if(reset) fct.reset();

      // evito il caricamento inutile controllando i flag di sistema
      if(fct.loadingData || fct.noMoreData)
        return;

      fct.loadingData = true;

      var data = {
        "search": fct.search,
        "offset": fct.paging.current,
        "limit": fct.paging.size
      };

      var promise = $http.post("/api/v1/catalog_sorting_rule/list", data)
      .then(function(response){
        fct.paging.current += 1;
        var data = response.data;
        if(data) {
          if(!data.rules || data.rules.length == 0) {
            data.rules = [];
            // se l'ultimo caricamento non ha tornato risultati so che non devo più fare caricamenti
            fct.noMoreData = true;
          } else {
            fct.paging.totalSize = data.rules[0].count;
          }

          fct.list = fct.list.concat(data.rules);
        }
        fct.loadingData = false;
      });
      return promise;
    },

    getByID: function(ruleID) {
      var promise = $http.get("/api/v1/catalog_sorting_rule/get_by_id/" + ruleID)
      .then(function(response) {
        var data = response.data;
        return data;
      });
      return promise;
    },

    saveOrUpdate: function(item, callback) {      
      if(!fctGastaldiFacets.lastPayload.freeSearch && !fctGastaldiFacets.lastPayload.facetFilter)
        return;

      item.freeSearch = fctGastaldiFacets.lastPayload.freeSearch;
      item.facetFilter = fctGastaldiFacets.lastPayload.facetFilter;

      var data = {
        "item": item
      };

      var endPoint = "/api/v1/catalog_sorting_rule/create_rule";
      if(item.catalog_sorting_rule_id)
        endPoint = "/api/v1/catalog_sorting_rule/update_rule";

      var promise = $http.post(endPoint, data)
      .then(function(response){
        if(callback)
          callback(response.data);
        else 
          return response.data;
      });
      return promise;
    },

    deleteRule: function(ruleID, callback) {
      var promise = $http.get('/api/v1/catalog_sorting_rule/delete_rule/' + ruleID)
      .then(function(response) {
        if(callback)
          callback(response.data);
        else 
          return response.data;
      });
      return promise;
    },

    resetAll: function(callback) {
      var data = {};
      var promise = $http.post('/api/v1/catalog_sorting_rule/reset_all', data)
      .then(function(response) {
        if(callback)
          callback(response.data);
        else 
          return response.data;
      });
      return promise;
    },
  };
  return fct;
}])

.factory('fctGastaldiFacets', ["$http", "moxutils", "fctFixedWords", function ($http, moxutils, fctFixedWords){
  var fct = {
    list: {},

    getFacets: function(){
      var promise = $http.get("/api/v1/gastaldi/facets")
        .success(function(data){  
          fct.list = data;
          return data;
        })
        .error(function(err){
            console.log(err);
        });
      return promise;
    },

    getSearchInput: function(data, model) {
      var result = facetFilter = "";

      angular.forEach(data, function(value, key) {
        if(value) {
          obj = angular.fromJson(value);
          if(typeof obj === "boolean") {
            facetFilter += model[key].field_id + '&';
          } else {
            if(obj && obj.length > 0) {
              if(obj.length == 1) {
                facetFilter += obj[0];
              }

              if(obj.length > 1) {
                facetFilter += "(";
                for (var i = 0; i < obj.length; i++) {
                  facetFilter += obj[i];
                  if(i < (obj.length-1)) facetFilter += '|';
                };
                facetFilter += ")";
              } 
              facetFilter += '&'; 
            }

          } 
        }
      }); 
      if(facetFilter.length)
        result += '(' + facetFilter.substring(0, facetFilter.length-1) + ')';
      return result;
    },

    paging: {
      totalSize : 0,
      size: 10,
      current: 1,
      noMoreData: false,
      loadingData: false
    },

    doSearch: function(userID, payload, reset, cb) {
      if(reset) {
        fct.paging.current = 1;
        fct.paging.noMoreData = false;
        fct.paging.loadingData = false;
        fct.lastPayload = null;
      }
      if(fct.paging.loadingData || fct.paging.noMoreData) {
        return;
      }

      if(userID)
        fct.searchUserID = userID;

      fct.paging.loadingData = true;
      fct.searched = true;

      if(payload)
        fct.lastPayload = payload;

      var data = {
        "userID": fct.searchUserID,
        "user_search" : fct.lastPayload.freeSearch,
        "selected_facets" : fct.lastPayload.facetFilter,
        "page_index" : fct.paging.current,
        "page_size" : fct.paging.size
      };

      var promise = $http.post("/api/v1/gastaldi/user_search", data)
      .success(function(response) {
        if(response) {
          fct.paging.current += 1;
          if(response.length) {
            fct.paging.totalSize = response[0].number_of_records;
          } else {
            // se l'ultimo caricamento non ha tornato risultati so che non devo più fare caricamenti
            fct.paging.noMoreData = true;
            fct.paging.totalSize = 0;
          }

          if(response.length < fct.paging.size) {
            fct.paging.noMoreData = true;
          }
          fct.paging.loadingData = false;
        }
        if(cb) cb(response);
        else return response;
      })
      .error(function(err){
        fct.paging.loadingData = false;
        return false;
      });
      return promise; 
    }
  };
  return fct;
}])

.controller('sortingRuleListCtrl', ['$scope','$route','i18nNotifications','$http','$location','fctSortingRule','data',
  function ($scope, $route, i18nNotifications, $http, $location, fctSortingRule, data) {
    $scope.fctSortingRule = fctSortingRule;  

    $scope.resetSortingRule = function() {
      $scope.fctSortingRule.resetAll(function(data) { 
        i18nNotifications.pushForCurrentRoute('crud.catalog_sorting_rule.reset.success', 'success');  
        fctSortingRule.getAll(true);
      });
    };
  }
])

.controller('sortingRuleEditCtrl', ['$scope','$rootScope','$sce','i18nNotifications','$http','$location','$filter','fctSortingRule','fctGastaldiFacets','item','facets',
  function ($scope, $rootScope, $sce, i18nNotifications, $http, $location, $filter, fctSortingRule, fctGastaldiFacets, item, facets) {
    //recupero gli elementi da rappresentare
    $scope.fctSortingRule = fctSortingRule;
    $scope.item = item;
    $scope.facets = fctGastaldiFacets;

    $scope.searched = false;

    if(!$scope.item.catalog_sorting_rule_id) {
      $scope.isNew = true;
    } 

    var data = {};
    for(var i = 0; i < fctGastaldiFacets.list.length; i++){
      if(fctGastaldiFacets.list[i].facets.length == 2){
        var positiveId = 0;
        angular.forEach(fctGastaldiFacets.list[i].facets, function(value, key) {
          if(value.facet_value == "true"){ 
            positiveId = value.facet_id;
          }
        });
        data[fctGastaldiFacets.list[i].code] = { 'field_id' : positiveId, 'facet_text' : fctGastaldiFacets.list[i].facet_text };
      } else {
        data[fctGastaldiFacets.list[i].code] = fctGastaldiFacets.list[i];
      }
    } 
    $scope.formFacets = {
      'brands' : data["MARCA"],
      'genre' : data["GENERE"],
      'sector' : data["SETTORE"],
      'reinforced': data["RINFORZATO"],
      'summer': data["ESTIVO"],
      'winter': data["INVERNALE"],
      'seasonal': data["QUATTRO_STAGIONI"],
      'promo': data["IN_PROMO"],
      'wheel': data["CERCHIO"],
      'run_on_flat': data["RUN_ON_FLAT"],
      'flap': data["CAMERA_FLAP"],
    };

    $scope.model = {
      'size': '',
      'facets': {
        'brand': [],
        'sector': [],
        'genre': [],
      },
      'stock': false
    };

    if(!$scope.isNew) {
      // caricamento nella view dei filti salvati
      $scope.model.size = item.search_text;

      for (var i = 0; $scope.item.filter_json && i < $scope.item.filter_json.length; i++) {
        switch($scope.item.filter_json[i].code) {
          case 'RINFORZATO': $scope.model.facets.reinforced = true; break;
          case 'ESTIVO': $scope.model.facets.summer = true; break;
          case 'INVERNALE': $scope.model.facets.winter = true; break;
          case 'QUATTRO_STAGIONI': $scope.model.facets.seasonal = true; break;
          case 'IN_PROMO': $scope.model.facets.promo = true; break;
          case 'CERCHIO': $scope.model.facets.wheel = true; break;
          case 'RUN_ON_FLAT': $scope.model.facets.run_on_flat = true; break;
          case 'CAMERA_FLAP': $scope.model.facets.flap = true; break;

          case 'MARCA': $scope.model.facets.brand.push($scope.item.filter_json[i].facet_id); break;
          case 'GENERE': $scope.model.facets.genre.push($scope.item.filter_json[i].facet_id); break;
          case 'SETTORE': $scope.model.facets.sector.push($scope.item.filter_json[i].facet_id); break;
        }
      };
    }


    $scope.onSubmit = function() {
      var params = {};
      $scope.products = [];
      $scope.searched = true;

      params.facetFilter = fctGastaldiFacets.getSearchInput($scope.model.facets, $scope.formFacets);

      if($scope.model.size.length){
        params.freeSearch = $scope.model.size;
      }

      var userID = null;
      if(item.user_id) {
        userID = item.user_id;
      } else {
        // se sto modificando la visibilità di un gruppo utilizzo l'utente di esempio per fare la query
        userID = item.sample_user_id;
      }

      fctGastaldiFacets.doSearch(userID, params, true, function(result) {
        if(result.length) {
          $scope.products = $scope.products.concat(result);
          $scope.productsFound = false;
        } else {
          $scope.productsFound = true;
        }
      });
    };

    $scope.nextPage = function() { 
      fctGastaldiFacets.doSearch(null, null, false, function(result) {
        if(result.length) {
          $scope.products = $scope.products.concat(result);
        }
      });
    };

    $scope.confirmSortingRule = function() {
      $scope.fctSortingRule.saving = true;

      $.gritter.add({
        title: 'Salvataggio della regola in corso!',
        text: "La procedura potrebbe richiedere un po' di tempo. al termine del salvataggio l'ordinamento sarà applicato nel catalogo.",
        time: 5000,
        class_name: 'gritter-success'
      });

      // aggiornamento lastPayload
      fctGastaldiFacets.lastPayload = {};
      fctGastaldiFacets.lastPayload.facetFilter = fctGastaldiFacets.getSearchInput($scope.model.facets, $scope.formFacets);

      if($scope.model.size.length){
        fctGastaldiFacets.lastPayload.freeSearch = $scope.model.size;
      }

      fctSortingRule.saveOrUpdate($scope.item, function(data) {
        fctSortingRule.getAll(true);
        i18nNotifications.pushForNextRoute('crud.catalog_sorting_rule.save.success', 'success');   
        $location.path('/admin/catalog_sorting_rule');
        $scope.fctSortingRule.saving = false;
      });   
    };

    $scope.deleteSortingRule = function() {
      fctSortingRule.deleteRule($scope.item.catalog_sorting_rule_id, function(data) {   
        fctSortingRule.getAll(true); 

        $('#deleteWarning').on('hidden.bs.modal', function (e) {
          i18nNotifications.pushForNextRoute('crud.catalog_sorting_rule.remove.success', 'success');   
          $location.path('/admin/catalog_sorting_rule');
          $scope.$apply();
        });        
        $('#deleteWarning').modal('hide'); 
      });   
    };
  }
])

.directive('eventOnRender', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      if (scope.$last === true) {
        $timeout(function () {
          scope.$emit('listRendered');
        });
      }
    }
  }
});

var sortableTableHelper = function(e, tr) {
  var $originals = tr.children();
  var $helper = tr.clone();
  $helper.children().each(function(index) {
    $(this).width($originals.eq(index).width())
  });
  return $helper;
},
sortableTableUpdateIndex = function(e, ui) {
  //renumero sull'html
  $('td.index', ui.item.parent()).each(function (i) {
    $(this).html(i + 1);
  });
  //renumero lato server
  var element = $('td.ruleID',ui.item);
  if(element != undefined && element.length > 0) {
    var ruleID = element.html();
    var newPos = ui.item.index()+1;
    $.get("/api/v1/catalog_sorting_rule/change_position/" + ruleID + "/" + newPos, null, function(data) {
      return;
    });
  }
};

angular.module('app').directive('sortableDirective', function() {
  return function(scope, element, attrs) {
    if (scope.$last) {
      $(".sortable tbody").sortable({
        helper: sortableTableHelper,
        stop: sortableTableUpdateIndex
      }).disableSelection();
    }
  };
});