angular.module('app', [
  'ngRoute',
  'directives.crud',
  'services.breadcrumbs',
  'services.menus',
  'services.i18nNotifications',
  'services.httpRequestTracker',
  'security',
  'templates.quicksight.app',
  'quicksight-module',
  'templates.common.app',
  'templates.common.common'
])

.constant('I18N.MESSAGES', {
  'errors.route.changeError': ExtractFixedWordTranslation('GENERAL_NOTIF_ROUTE_CHANGE_ERROR'),
})

angular.module('app').config(['$routeProvider', '$locationProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
  function($routeProvider, $locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
    $locationProvider.html5Mode(true);
    $provide.decorator('$sniffer', function($delegate) {
      $delegate.history = true;
      return $delegate;
    });

    $routeProvider.otherwise({
      redirectTo: function(routeParams, path, search){
        window.location = path;
      }
    });
  }
]);

angular.module('app').run(['security', function(security) {
  // Get the current user when the application starts
  // (in case they are still logged in from a previous session)
  security.requestCurrentUser();
}]);

angular.module('app').controller('AppCtrl', ['$scope', 'i18nNotifications', '$rootScope', '$route',function($scope, i18nNotifications, $rootScope, $route) {
  $scope.notifications = i18nNotifications;

  $scope.removeNotification = function (notification) {
    i18nNotifications.remove(notification);
  };

  $scope.$on('$routeChangeError', function(event, current, previous, rejection){
    i18nNotifications.pushForCurrentRoute('errors.route.changeError', 'error', {}, {rejection: rejection});
  });

  $scope.$on('$routeChangeSuccess', function(event, current, previous, rejection){
    window.scrollTo(0,0);
  });
}]);