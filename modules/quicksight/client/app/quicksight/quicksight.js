angular.module('quicksight-module',['ngRoute','services.crud','services.i18nNotifications','directives.gravatar','ui.directives'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  /****  ROUTES  ****/ 
  .when('/admin/quicksight', {
    templateUrl:'quicksight/quicksight.tpl.html',
    controller:'quicksightReportCtrl',
    resolve: {
      data: ['fctQuicksight', function(fctQuicksight) {
        return fctQuicksight.getReport();
      }]
    },
    reloadOnSearch: false
  }) 
}])

.factory('fctQuicksight', ["$http", function ($http) {
  var fct = {
    getReport: function(ruleID) {
      var promise = $http.post("/api/v2/aws/quicksight")
      .then(function(response) {
        var data = response.data;
        return data;
      });
      return promise;
    },

    getReportByID: function(reportID) {
      var promise = $http.post("/api/v2/aws/quicksight/" + ruleID)
      .then(function(response) {
        var data = response.data;
        return data;
      });
      return promise;
    }
  };
  return fct;
}])

.controller('quicksightReportCtrl', ['$scope','$rootScope','$sce','fctQuicksight','data',
  function ($scope, $rootScope, $sce, fctQuicksight, data) {
    //recupero gli elementi da rappresentare
    $scope.fctQuicksight = fctQuicksight;
    
    $scope.reportsURL = null;
    if(data.error) {      
      $scope.reportsURL = null;
    } else {
      $scope.rawReportsURL = data['EmbedUrl'];
      console.log($scope.rawReportsURL); 
      $scope.reportsURL = $sce.trustAsResourceUrl($scope.rawReportsURL + '&printEnabled=false');
    }
  }
]);