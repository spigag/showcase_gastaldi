const AWS = require('aws-sdk');

var utils = require('../../../config/utils').utils,
    sequelize = require('../../../config/databases.js').databases.sequelize,
    config = require('../../../config/local.js');

var s = config.aws ? config.aws.quicksight : {};

var fs = require('fs');
var qsConfig = JSON.parse(fs.readFileSync('config/local/AwsConfigQS.json', 'utf8'));

var quicksight = new AWS.Service({
    apiConfig: require('../../../node_modules/aws-sdk/apis/quicksight-2018-04-01.min.json'),
    region: s.region,
    accessKeyId: qsConfig.accessKeyId,
    secretAccessKey: qsConfig.secretAccessKey
});

var QuicksightController = {
  'generate-embedded': function(req, res) {
    var dashboardID = req.body.id;

    var generateEmbedded = function(dashboardID, user_arn) {      
        console.log('User ARN: ' + user_arn);      
        quicksight.generateEmbedUrlForRegisteredUser({
            'AwsAccountId': s.account_id,
            'ExperienceConfiguration': { 
                'Dashboard': {
                    'InitialDashboardId': dashboardID
                }
            },
            'UserArn': user_arn,
            'SessionLifetimeInMinutes': 100
        }, function(err, data) {
            if(err) {
                console.log('Errors: ');
                console.log(err);
                res.json({error: err});
            }else{
                console.log('Response: ');
                console.log(data);
                res.json(data);
            }
        });
    }; 


    sequelize.query("select * from crm.report_get_account_id(" + req.user.user_id + ", 'quicksight') user_arn")
    .success(function(r) {
        var user_arn = s.user_arn;
        if (r.length > 0){
            user_arn = r[0].user_arn;
        }

        if(dashboardID) {
            return generateEmbedded(dashboardID, user_arn);
        } else {  
            var jsonParam = JSON.stringify({
                report_id: req.param('id')  
            }).replace(/'/g, "''");

            sequelize.query("select * from crm.customers_report_by_id(" + req.user.user_id + ", '" + jsonParam + "') c")
            .success(function(result) {
                if(result.length > 0) {
                    var data = result[0].c;
                    return generateEmbedded(data.details.id, user_arn);
                } else {
                    res.json({error: 'NOT_FOUND'});
                }
            })
            .error(function(error) {
                console.log('Errors: ');
                console.log(error);
                res.send(404);
            });
        }
    })
    .error(function(error) {
        console.log(error);
    });
  },

  _config: {}
}
module.exports = QuicksightController;



