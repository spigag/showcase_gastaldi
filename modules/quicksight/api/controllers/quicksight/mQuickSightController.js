var utils = require('../../../config/utils').utils,
	config = require('../../../config/local.js');

module.exports = {
    index: function(req, res) {
        return res.view('emptyView.ejs', {
    		moduleName 		    : 	'quicksight',
    		layout 				: 	'quicksight/baseLayout.ejs',
    		dyn_version 		: 	utils.dyn_version,
    		stat_version 	    : 	utils.stat_version,
    		appName				: 	config.appName,
            userCulture         :   req.user.culture_id,
            themeName           :   'backoffice',
            testEnvironment     :   config.testEnvironment
        });
    }
};