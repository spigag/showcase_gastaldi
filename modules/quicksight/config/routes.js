module.exports.routes = {
	/*********************  routes *********************/
  '/admin/quicksight': {
    controller: 'quicksight/mQuickSightController',
    action: 'index',
		functionalities: ['quicksight_report.access']
	},
	/*********************** APIs ***********************/
  'post /api/v2/aws/quicksight': {
    controller: 'quicksight/QuicksightController',
    action: 'generate-embedded'
  },
  'post /api/v2/aws/quicksight/:id': {
    controller: 'quicksight/QuicksightController',
    action: 'generate-embedded'
  }
}