 /**
 * Local environment settings
 *
 * While you're developing your app, this config file should include
 * any settings specifically for your development computer (db passwords, etc.)
 * When you're ready to deploy your app in production, you can use this file
 * for configuration options on the server where it will be deployed.
 *
 *
 * PLEASE NOTE: 
 *    This file is included in your .gitignore, so if you're using git 
 *    as a version control solution for your Sails app, keep in mind that
 *    this file won't be committed to your repository!
 *
 *    Good news is, that means you can specify configuration for your local
 *    machine in this file without inadvertently committing personal information
 *    (like database passwords) to the repo.  Plus, this prevents other members
 *    of your team from commiting their local configuration changes on top of yours.
 *
 *
 * For more information, check out:
 * http://sailsjs.org/#documentation
 */

module.exports = {


  // The `port` setting determines which TCP port your app will be deployed on
  // Ports are a transport-layer concept designed to allow many different
  // networking applications run at the same time on a single computer.
  // More about ports: http://en.wikipedia.org/wiki/Port_(computer_networking)
  // 
  // By default, if it's set, Sails uses the `PORT` environment variable.
  // Otherwise it falls back to port 1337.
  //
  // In production, you'll probably want to change this setting 
  // to 80 (http://) or 443 (https://) if you have an SSL certificate

  port: process.env.PORT || 8000,

  serviceRoot: 'localhost:8000',//'http://34.251.84.172:' + (process.env.PORT || 8000),

  serviceName: 'Gastaldi',
  
  serviceHost: 'localhost:8000',//'http://34.251.84.172:8002', // ATTENZIONE: in produzione inserire il nome del dominio per ogni sito (es: 'products.legor.com' per Legor Showcase)


  appName:"Gastaldi",

  stat_version: 155,

  //il tipo di upload da usare può essere.
  //"FS" - upload su questo server usando il file system
  //"AWS" - upload su AWS
  upload_file_type: 'AWS',
  awsBucket: 'gastaldigomme',
  // tipo di routing del sito parte front
  // 0 -> solo divisioni: la pagina home ('/') presenta direttamente la scelta delle divisioni
  // 1 -> completo: la pagina home ('/') presenta il menu
  //                clienti,catalogo,documenti con relative routes /people,/divisions,/documents
  complete_front_routing: 2,

  base_culture_code: 'IT',

  // The runtime "environment" of your Sails app is either 'development' or 'production'.
  //
  // In development, your Sails app will go out of its way to help you
  // (for instance you will receive more descriptive error and debugging output)
  //
  // In production, Sails configures itself (and its dependencies) to optimize performance.
  // You should always put your app in production mode before you deploy it to a server-
  // This helps ensure that your Sails app remains stable, performant, and scalable.
  // 
  // By default, Sails sets its environment using the `NODE_ENV` environment variable.
  // If NODE_ENV is not set, Sails will run in the 'development' environment.

  environment: process.env.NODE_ENV || 'development',


  database:{
      nosql:{
        connectionString: 'mongodb://localhost/gastaldi',
        userId: '',
        password: ''
      },
      relational:{
        server: 'gastaldi.cwjrrgek51vt.eu-west-1.rds.amazonaws.com',//'192.168.1.11',
        port: 5432,
        userId: 'moxadmin', //'moxadmin',
        password: 'moxsol123',
        database: 'gastaldi'
      }
  },
  templates:{
    path: 'public/sheets_templates/',
    work_path: 'public/sheets_work/'
  },
  pdfService: {
    host: '192.168.1.31',
    port: 80,
    path: '/Mox.Pdf/api/pdf/generatePdf',
    sheetBaseUrl: '192.168.1.32:8000'
  },
  pricesService:{
    host: "ec2-52-213-118-51.eu-west-1.compute.amazonaws.com",
    port: 8081,

    endPoint: {
      list: '/gastaldi/priceForProducts',
      detail: '/gastaldi/priceForProductAndPricelist',
      document: '/gastaldi/calculateDocumentTotals',
      document_data: '/gastaldi/serializeDocument',
      check_document_conditions: '/gastaldi/checkDocumentCondition',
      send_email: '/sendEmail' //non aggiungere il nome del war
    },
    appName: "/gastaldi" //nome del file war (senza estensione). Se viene usato il file jar (ovvero con tomcat integrato) allora lasciare a stringa vuota
  },

  paths:{
    closed_document_zip: "upload/",//cartella in cui viene memorizzato il file zip con pdf ed allegati del documento chiuso
    temp_local: "public/temp",
    pdf_preview_path: "public/temp", //percorso relativo al file system (per salvare i file)
    pdf_preview_url:  "/temp", //percorso web per accedere ai file
    importer_process: "ls >> /Users/moxdevelop/progetti/showcase/prova.txt",
    ckEditorUploadedFile: "upload/ckeditor/", //percorso in cui saranno caricati i file inseriti all'interno del ckeditor
    uploadAreaRoot: "", //Il percorso della rootPath dell'area di upload. Se definito, questo  percorso deve essere inserito nella base url della firm
    attachmentElementRelPath: "attachment_element", //Percorso di upload degli attachmentelement. Il percorso indicato qui parte da "uploadAreaRoot" e viene salvato nel nome del file
    splashScreenRelPath:"splash", //Percorso di upload per i file splash screen. Il percorso indicato qui parte da "uploadAreaRoot" e viene salvato nel nome del file
    jobschedulerLogs:"/home/ec2-user/customer-apps/gastaldi/gastaldi_importer"
  },
  
  httpPdfVirtualDir:{
    host: '192.168.1.31',
    virtualDir: 'techSheetPdf'
  },

  commerce:{
    wishlist_active: true,
    active:true
  },

  images:{
    //   resize_service_type : 'static_resized',
    //   resize_service_path: '{width}x{height}',
    resize_service_type: 'static_resized',
    resize_service_path: 'images/lambdaresized/{width}x{height}',
  
    path: 'upload', //percorso completo su cui deve essere salvato il file
    document_web_path: 'resized/',
    document_web_path_prefix: 'upload', //il previsso è l'eventuale percorso relativo al percorso della base_url della firm in cui è memorizzato il file

    brands_web_path: 'images/original/brands/',
    brands_prefix: 'brands/',

    product_list: {
      resize: true,
      resize_width: '500',
      resize_height: '500',
    },

    product_additionals:{
      resize:true,
      resize_width: '500',
      resize_height: '500',
    },


    product_detail:{
      resize:true,
      zoomable:true,
      resize_width: '500',
      resize_height: '500'
    },

    product_detail_scopes:{
      resize:true,
      resize_width: '170',
      resize_height: '160'
    },

    product_download:{
      resize:true,
      resize_width: '600',
      resize_height: '600'
    },
    zoom_modal: {  
      //viene concatenato subito dopo il campo base_url della firm 
      //--> se definito va a sostituire il valore resize service path
      intermediate_path: 'images/original',

      resize: true,
      resize_width: '1536',
      resize_height: '1536',
    }
  },

  users:{
    logo:{
      path: 'upload/NodesLogo', //percorso nel file system (riferito alla root dell'applicazione)
      //web_path: 'upload/', //percorso dalla root dell'applicazione web ( es: http://localhost/)
      web_path_prefix: 'upload/NodesLogo'  
    },
    registrationRequestMongoModel: {
        username:       {type:String, default: '' },
        password:       {type:String, default:''},
        division:       {type:Object, default:''},
        user_type:      {type:String, default:''},
        full_name:      {type:String, default:''},
        first_name:     {type:String, default:''},
        last_name:      {type:String, default:''},
        company_name:   {type:String, default:''},
        address:        {type:String, default:''},
        market_code:    {type:String, default:''},
        country:        {type:String, default:''},
        culture:        {type:String, default:''}, 
        city:           {type:String, default:''},
        phone:          {type:String, default:''},
        company_email:  {type:String, default:''},
        email:          {type:String, default:''},
        role_on_company: {type:String, default:''},
        tax_code:       {type: String, default: '' },
        vat_code:       {type: String, default: '' },
        registrationDate: {type: Date, default: Date.now}
      }
  },

  notifier:{
    mail:{/*
      protocol: "SMTP",
      service: "Gmail",
      auth: {
        user: "info@gastaldigomme.it",
        pass: "ciao123ciao"
      },*/
      protocol: "SMTP",
      host: "email-smtp.eu-west-1.amazonaws.com", // hostname
      secureConnection: true, // use SSL
      port: 465, // port for secure SMTP
      auth: {
          user: "AKIAJUULLHCABORQGMXA",
          pass: "AhonZjSZQesmKGPnq1BrHEjFhVa7++uTWnSPID3rdZma"
      },
      sender: 'info@gastaldigomme.it',
      registrationSupportEmail : "info@gastaldigomme.it"
    }
  },
  variant_service_url: 'http://95.110.199.41/',
  public_page_excluded_routes: 'division|cart|wishlist|comparison|userprofile|customers|documents|download|agent|signup|login|signup_expired|mydocuments|fast-search',
  seo: {
    is_active: true,
    analytics: { //Google Analytics
      gtag: true,
      id: 'UA-108237387-1' 
    },
    page_map: [
      {
        label: 'home',
        data: {'id': false, 'type': 'GENERAL_HOME', 'follow': true, 'index': false, 'track': false}
      },
      {
        label : 'division',
        data: {
          division: {'id': false, 'type': 'division', 'follow': true, 'index': false, 'track': true},
          category: {'id': false, 'type': 'category', 'follow': true, 'index': false, 'track': true}
        }
      },
      {
        label: 'item',
        data: {'id': false, 'type': 'product', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'cart',
        data: {'id': false, 'type': 'CART_QUEUE', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'comparison',
        data: {'id': false, 'type': 'COMPARE_QUEUE', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'wishlist',
        data: {'id': false, 'type': 'WISHLIST_QUEUE', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'userprofile',
        data: {'id': false, 'type': 'USER_PROFILE', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'documents',
        data: {'id': false, 'type': 'HOMEPAGE_DOCUMENTS', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'mydocuments',
        data: {'id': false, 'type': 'MYDOCUMENTS_TITLE', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'login',
        data: {'id': false, 'type': 'GENERAL_LOGIN_PAGE', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'signup',
        data: {'id': false, 'type': 'GENERAL_SIGN_IN', 'follow': true, 'index': false, 'track': true}
      },
      {
        label: 'fast-search',
        data: {'id': false, 'type': 'fast_search', 'follow': true, 'index': false, 'track': true}
      },
    ]
  }  
};
