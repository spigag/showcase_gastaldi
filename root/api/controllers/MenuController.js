var sequelize  = require('../../config/databases.js').databases.sequelize
	,utils = require('../services/utils');

var MenuController = {
	
	buildMenus: function(req, res, successCb, errorCb){  
		var menu = [
	   		{
				'name': 'Home'
				,'url': '/admin/'
				,'icon': 'fa fa-th'
			}
	   		,{
	   			'name': 'USER_MANAGEMENT'
	   			,'url': '#'
	   			,'icon': 'fa fa-user'
	   			,'subitems' : [
	   			{
						'name': 'USER_ROLES'
						,'url': '/admin/role'
						,'icon': 'fa fa-sitemap'
						,'allowif' : [
							{ "functionality" : 'role.create' }
						]
					},
					{
						'name': 'USER_GROUPS'
						,'url': '/admin/usergroup'
						,'icon': 'fa fa-globe'
						,'allowif' : [
							{ "functionality" : 'usergroup.create' }
						]
					},
					{
						'name': 'USER_PROFILES'
						,'url': '/admin/profile'
						,'icon': 'fa fa-users'
						,'allowif' : [
							{ "functionality" : 'users.profile.list' }
						]
					}
					,{
						'name': 'USER_USERS'
						,'url': '/admin/users'
						,'icon': 'fa fa-users'
						,'allowif' : [
							{ "functionality" : 'users.list' }
							,{ "functionality" : 'users.create' }
							,{ "functionality" : 'users.edit' }
						]
					}
					,{
	   					'name': 'USER_PRS'
	   					,'url': '/admin/pending_registration'
	   					,'icon': 'fa fa-sign-in'
	   					,'allowif' : [
							{ "functionality" : 'users.validate_registration' }
						]
					}
				]  
			}
			,{
				'name' : 'GENERAL_AGENTS'
				,'url' : '#'
				,'icon' : 'fa fa-suitcase'
				,'func_guid' : ''
				,'subitems' : [
					{
						'name': 'GENERAL_AGENTS'
						,'url': '/admin/agent'
						,'icon': 'fa fa-users'
						,'allowif' : [
							{ "functionality" : 'agent.management' }
						]
					},
					{
						'name': 'AGENT_COMMISSION_REASONS'
						,'url': '/admin/commission_reason'
						,'icon': 'fa fa-code'
						,'allowif' : [
							{ "functionality" : 'agent.management' }
						]
					},
					{
						'name': 'AGENT_COMMISSION_TYPES'
						,'url': '/admin/commission_type'
						,'icon': 'fa fa-code'
						,'allowif' : [
							{ "functionality" : 'agent.management' }
						]
					},
				]
			}
			,{
				'name' : 'GENERAL_UTILITIES'
				,'url' : '#'
				,'icon' : 'fa fa-wrench'
				,'func_guid' : ''
				,'subitems' : [
					{
						'name': 'MB_REINDEX'
						,'url': '/admin/reindex'
						,'icon': 'fa fa-retweet'
						,'allowif' : [
							{ "functionality" : 'metabase.create.reindex' }
						]
					},
					{
						'name': 'GENERAL_MANUAL_ACTIONS'
						,'url': '/admin/manual_action'
						,'icon': 'fa fa-retweet'
						,'allowif' : [
							{ "functionality" : 'settings.management' },
							{ "functionality" : 'settings.manual_action' }
						]
					},
					{
						'name': 'GENERAL_SPLASH_SCREEN'
						,'url': '/admin/settings/splash'
						,'icon': 'fa fa-picture-o'
						,'allowif' : [
							{ "functionality" : 'settings.splash_screen' }
						]
					},
					{
						'name': 'GENERAL_SETTINGS'
						,'url': '/admin/settings'
						,'icon': 'fa fa-wrench'
						,'allowif' : [
							{ "functionality" : 'settings.management' }
						]
					},
					{
						'name': 'GENERAL_SITEMAP'
						,'url': '/admin/sitemap'
						,'icon': 'fa fa-sitemap'
						,'allowif' : [
							{ "functionality" : 'settings.management' }
						]
					},
					{
						'name': 'TRANSLATION_TRANSLATIONS'
						,'url': '/admin/translations'
						,'icon': 'fa fa-globe'
						,'allowif' : [
							{ "functionality" : 'translations.list' }
						]
					},
					{
						'name': 'JOBSCHEDULER'
						,'url': '/admin/job_scheduler'
						,'icon': 'fa fa-retweet'
						,'allowif' : [
							{ "functionality" : 'settings.management' }
						]
					},
					{
						'name': 'LEGALTERMS'
						,'url': '/admin/legalterms'
						,'icon': 'fa fa-file-text-o'
						,'allowif' : [
							{ "functionality" : 'legalterms.global.edit' }
						]
					},
					{
						'name': 'GASTALDI_QS_REPORT'
						,'url': '/admin/quicksight'
						,'icon': 'fa fa-bar-chart-o'
						,'allowif' : [
							{ "functionality" : 'quicksight_report.access' }
						]
					}
				]
			}
			,{
				'name' : 'Metabase'
				,'url' : '#'
				,'icon' : 'fa fa-sitemap'
				,'func_guid' : ''
				,'subitems' : [ 
			   		{
						'name': 'MB_DATASCHEMAS'
						,'url': '/admin/metabase'
						,'icon': 'fa fa-flask'
						,'allowif' : [
							{ "functionality" : 'metabase.create.dataschema' }
						]
					},
					{
						'name': 'MB_LOOKUPS'
						,'url': '/admin/lookup'
						,'icon': 'fa fa-eye'
						,'allowif' : [
							{ "functionality" : 'metabase.create.lookup' }
						]
					},
					{
						'name': 'MB_POST_CHECKIN'
						,'url': '/admin/checkin'
						,'icon': 'fa  fa-check-circle-o'
						,'allowif' : [
							{ "functionality" : 'metabase.create.post_checkin_action' }
						]
					},
					{
						'name': 'MB_DICTIONARY'
						,'url': '/admin/dictionary'
						,'icon': 'fa fa-font'
						,'allowif' : [
							{ "functionality" : 'metabase.create.dictionary' }
						]
					},
					{
						'name': 'MB_TPLS'
						,'url': '/admin/template'
						,'icon': 'fa fa-file-text-o'
						,'allowif' : [
							{ "functionality" : 'metabase.create.template' }
						]
					}
				]
			},
			{
				'name' : 'GENERAL_CATALOG'
				,'url' : '#'
				,'icon' : 'fa fa-briefcase'
				,'subitems' : [
					{
						'name': 'MARKET_MARKETS'
						,'url': '/admin/market'
						,'icon': 'fa fa-shopping-cart'
						,'allowif' : [
							{ "functionality" : 'market.create' }
						]
			   	},
					{
		   			'name': 'GENERAL_CATEGORIES'
		   			,'url': '/admin/categories'
		   			,'icon': 'fa fa-sitemap'
		   			,'allowif' : [
							{ "functionality" : 'category.create' }
						]
		   		},
		   		{
		   			'name': 'BANNER_BANNERS'
		   			,'url': '/admin/banner'
						,'icon': 'fa fa-picture-o'
						,'allowif' : [
							{ "functionality" : 'banner.create' }
						]
		   		},
	   			{
		   			'name': 'AFFINITY_TYPES'
		   			,'url': '/admin/affinitytype'
						,'icon': 'fa fa-list'
						,'allowif' : [
							{ "functionality" : 'affinitytype.create' }
						]
		   		},
		   		{
		   			'name': 'FAQ_FAQS'
		   			,'url': '/admin/faq'
						,'icon': 'fa fa-comments-o'
						,'allowif' : [
							{ "functionality" : 'faq.management' }
						]
		   		},
		   		{
		   			'name': 'GENERAL_PRODUCTS'
		   			,'url': '/admin/entitycatalog'
						,'icon': 'fa fa-tags'
						,'allowif' : [
							{ "functionality" : 'product.view' }
							,{ "functionality" : 'product.create' }
						]
		   		},
		   		{
		   			'name': 'GENERAL_BRANDS'
		   			,'url': '/admin/brand'
						,'icon': 'fa fa-ticket'
						,'allowif' : [
							{ "functionality" : 'catalog.brand.management' }
						]
		   		},
					{
		   			'name': 'CATALOG_PROD_ALIAS'
		   			,'url': '/admin/customer_product_alias'
						,'icon': 'fa fa-tags'
						,'allowif' : [
							{ "functionality" : 'catalog.customer_product_alias.view' }
							,{ "functionality" : 'catalog.customer_product_alias.edit' }
						]
		   		},
					{
		   			'name': 'CATALOG_PALLET_RULES'
		   			,'url': '/admin/pallet_rule'
						,'icon': 'fa fa-archive'
						,'allowif' : [
							{ "functionality" : 'catalog.pallet_rule.management' }
						]
		   		},
					{
		   			'name': 'Ordinamento Custom'
		   			,'url': '/admin/catalog_sorting_rule'
						,'icon': 'fa fa-sort-amount-desc'
						,'allowif' : [
							{ "functionality" : 'catalog_sorting_rule.access' }
						]
		   		}
   			]
			},
			{
				'name' : 'TR_TRS'
				,'url' : '#'
				,'icon' : 'fa fa-trophy'
				,'subitems' : [
					{
						'name': 'TR_PROMOS'
						,'url': '/admin/promo'
						,'icon': 'fa fa-trophy'
						,'allowif' : [
							{ "functionality" : 'traderules.promo.management' }
						]
		   		},
					{
						'name': 'TR_STATS'
						,'url': '/admin/statistics'
						,'icon': 'fa fa-bar-chart-o'
						,'allowif' : [
							{ "functionality" : 'traderules.promo.statistics' }
						]
		   		},
		   		{
						'name': 'TR_DISCOUNT_TYPES'
						,'url': '/admin/discounttype'
						,'icon': 'fa fa-code'
						,'allowif' : [
							{ "functionality" : 'traderules.discountconfig.management' }
						]
		   		},{
						'name': 'TR_DISCOUNT_CONFIG'
						,'url': '/admin/discountconfiguration'
						,'icon': 'fa fa-sort-amount-desc '
						,'allowif' : [
							{ "functionality" : 'traderules.discountconfig.management' }
						]
		   		},{
						'name': 'TR_CR_TYPES'
						,'url': '/admin/contractruletype'
						,'icon': 'fa fa-code'
						,'allowif' : [
							{ "functionality" : 'traderules.contractrule.management' }
						]
		   		},{
						'name': 'TR_CRS'
						,'url': '/admin/contractrule'
						,'icon': 'fa fa-file-text-o'
						,'allowif' : [
							{ "functionality" : 'traderules.contractrule.management' }
						]
		   		},{
						'name': 'TR_CONDITIONS'
						,'url': '/admin/documentcondition'
						,'icon': 'fa fa-list-alt'
						,'allowif' : [
							{ "functionality" : 'traderules.documentcondition.management' }
						]
		   		},{
						'name': 'TR_TRADE_CONFIG'
						,'url': '/admin/tradeconfiguration'
						,'icon': 'fa fa-cogs'
						,'allowif' : [
							{ "functionality" : 'traderules.tradeconfig.management' }
						]
		   		},{
						'name': 'TR_ID_RESERVATIONS'
						,'url': '/admin/idreservations'
						,'icon': 'fa fa-book'
						,'allowif' : [
							{ "functionality" : 'traderules.promo.idreservations' }
						]
		   		},{
						'name': 'TR_TRADE_CONFIG'
						,'url': '/admin/importexport'
						,'icon': 'fa fa-exchange'
						,'allowif' : [
							{ "functionality" : 'traderules.importexport.management' }
						]
		   		},{
						'name': 'Prodotti esclusivi'
						,'url': '/admin/productreservation'
						,'icon': 'fa fa-bookmark'
						,'allowif' : [
							{ "functionality" : 'traderules.productreservation.management' }
						]
		   		}
   			]
			},

			{
				'name' : 'HISTORY'
				,'url' : '#'
				,'icon' : 'fa fa-book'
				,'subitems' : [
					{
						'name': 'TR_PROMOS'
						,'url': '/admin/history_promo'
						,'icon': 'fa fa-trophy'
						,'allowif' : [
							{ "functionality" : 'traderules.promo.history' }
						]
		   		}
   			]
			},

			{
				'name' : 'Competitors'
				,'url' : '#'
				,'icon' : 'fa fa-flag-checkered'
				,'subitems' : [
						{
							'name': 'Competitors'
							,'url': '/admin/competitor'
							,'icon': 'fa fa-flag-checkered'
							,'allowif' : [
								{ "functionality" : 'competitors.create' }
							]
				   	},
				   	{
							'name': 'Commercial Reports'
							,'url': '/admin/commercial_report'
							,'icon': 'fa fa-exclamation-circle'
							,'allowif' : [
								{ "functionality" : 'commercial_reports.create' }
							]
				   	},
						{
			   			'name': 'Types'
			   			,'url': '/admin/competitors_type'
			   			,'icon': 'fa fa-list-ul'
			   			,'allowif' : [
								{ "functionality" : 'competitors.create' }
						]
			   		},
			   		{
			   			'name': 'Notes types'
			   			,'url': '/admin/competitors_notetype'
							,'icon': 'fa fa-list-ul'
							,'allowif' : [
								{ "functionality" : 'competitors.create' }
							]
			   		},
			   		{
			   			'name': 'Relations types'
			   			,'url': '/admin/competitors_relationtype'
							,'icon': 'fa fa-list-ul'
							,'allowif' : [
								{ "functionality" : 'competitors.create' }
							]
			   		},
			   		{
			   			'name': 'Reports types'
			   			,'url': '/admin/competitors_reporttype'
							,'icon': 'fa fa-list-ul'
							,'allowif' : [
								{ "functionality" : 'competitors.create' }
							]
			   		}
	   			]
			},	
			{
				'name' : 'Gestione sito pubblico'
				,'url' : '#'
				,'icon' : 'fa fa-globe'
				,'subitems' : [
					{
						'name': 'Menu'
						,'url': '/admin/menu'
						,'icon': 'fa fa-list-ul'
						,'allowif' : [
							{ "functionality" : 'public.menu.create' }
						]
			   	},
					{
						'name': 'Pagine'
						,'url': '/admin/page'
						,'icon': 'fa fa-file'
						,'allowif' : [
							{ "functionality" : 'public.page.create' }
						]
			   	},
   			]
			}		
			/*,
			{
				'name' : 'Test'
				,'url' : '#'
				,'icon' : 'fa fa-flag-checkered'
				,'subitems' : [
					{
						'name': 'Camunda'
						,'url': '/admin/camunda'
						,'icon': 'fa fa-flag-checkered'				
				   	}
	   			]
				}*/
   		];

   		if (req.user) {
   			//sono loggato => controllo le funzionalità dell'utente
				sequelize
				.query("select * from user_get_funcionalities (" + req.user.user_id + ");")
				.success(function(result){
					//console.log('get user functionalities');
					//visito i rami del menu
					for (menuIdx=0; menuIdx<menu.length; menuIdx++) {
						recursiveVisitMenuItem(result, menu[menuIdx]);
					}
					//res.json(200, menu);
					//return menu;
					successCb(menu);
				}).error(function(error) {
					//utils.logError(res, error);
					//res.json(404, {});
					errorCb(error);
				});
   		} else {
   			//non loggato => nessuna voce di menù attiva
   			//res.json(200, {});
   			successCb({});
   		}
	},

   	getMenus: function(req, res, next) {
   		var err = null;

   		var succCb = function(result) {
				res.json(200, result);
   		};

  		var errCb = function(err) {
   			utils.logError(res, err);
   		};

   		var json = MenuController.buildMenus(req, res, succCb, errCb);
   	}
};

function functionalityAllowed(target_funct, user_funct) {
	for (funcIdx=0; funcIdx<user_funct.length; funcIdx++) {
		if (user_funct[funcIdx].guid == target_funct) {
			return true;
		}
	}
	return false;
};

function recursiveVisitMenuItem(user_funct, menuItem) {
	//console.log('visit menu item:' + menuItem.name);
	if (menuItem.allowif == undefined || menuItem.allowif.length == 0) {
		menuItem.allowed = true;
	} else {
		menuItem.allowed = false;
		for (var allIdx=0; allIdx<menuItem.allowif.length;allIdx++) {
			var curAllow = menuItem.allowif[allIdx];
			if (curAllow.functionality) {
				//cerco tra le funzionalità
				if (functionalityAllowed(curAllow.functionality, user_funct)) {
					menuItem.allowed = true;
					break;
				}
			}
		}
	}
	//visito ricorsivamente i sottoelementi
	if (menuItem.allowed && menuItem.subitems && menuItem.subitems.length > 0) {
		for (subIdx=0; subIdx<menuItem.subitems.length;subIdx++) {
			recursiveVisitMenuItem(user_funct, menuItem.subitems[subIdx]);
		}
		//se tutti i sottomenu sono disabilitati => disbilito il menù principale
		var almostOne = false;
		for (subIdx=0; subIdx<menuItem.subitems.length;subIdx++) {
			if (menuItem.subitems[subIdx].allowed) {
				almostOne = true;
				break;
			}
		}
		if (!almostOne) {
			menuItem.allowed = false;
		}
	}
};
 
module.exports = MenuController;