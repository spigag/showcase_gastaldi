/*
 * gastaldi_cart_alert
 *
 */

'use strict';

/*TODO: i percorsi sono chiaramente da testare e rivedere, aiuto  */
var moxutils = require('../modules/moxutils');
var databases = require('../config/databases');
var config = require('../../config/local.js');

// qui può avere un senso dare un occhio al noe4mailer utilizzato dallo scheduler di beretta... da rivedere
var mailnotifier = require('../../api/services/mailnotifier'); 

var Sequelize = require("sequelize");
var fs = require('fs');


String.prototype.formatMoney = function(c, d, t){
	var a = this;
	if (! isNaN(a)) {
		var n = parseFloat(a);
		return n.formatMoney(c, d, t);
	}
	return '';
};
		
Number.prototype.formatMoney = function(c, d, t){
  var n = this, 
      c = isNaN(c = Math.abs(c)) ? 2 : c, 
      d = d == undefined ? "." : d, 
      t = t == undefined ? "," : t, 
      s = n < 0 ? "-" : "", 
      i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
      j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


module.exports = function(grunt) { 
  grunt.registerMultiTask('gastaldi_cart_alert', 'Cart alert notifications', function() {
    // DB Connection
    var sequelize = databases.sequelize;

    grunt.log.writeln('GastaldiGomme Cart Alert');

    sequelize
    .authenticate()
    .then(function() {
      grunt.log.writeln('Connection has been established successfully.');  
    	grunt.log.writeln('Starting cart alert procedure...');


    	var jobs = [];
    	var carts = null;

    	// JOBS
    	// carts
    	jobs.push(function(jobDone) {
				sequelize.query("select * from gastaldi.scheduler_cart_alert_get_list()")
				.then(function(result) {
					carts = result; // [0] ?

	      	moxutils.asyncForEach(carts, function(cart, cartDone) {
	      		var cartID = cart.document_id;

            /* esempio di oggetto in arrivo
            {
              "document_id":2538,
              "user_id":1077,
              "username":"test_srl",
              "email":"FATTURE@RIGOMMA.IT",
              "culture_code":"BASE",
              "division_id":1,
              "vl_name":"SERVICE",
              "products":[{"product_code":"BR100020FGL216PR","quantity":1.000000,"price":null}]
            }
            */

            // template della mail
            var mailTemplate = 'cart_alert';
            // se un giorno sarà in lingua vedere i template multipli mettendo dietro il culture code.

            // dati da passare al template della mail
            var data = {
              cartURL: config.serviceHost + '/' + cart.culture_code + '/cart/' + cart.division_id,
              products: cart.products,
              division: cart.division_name
            };

            // oggetto
            var subject = 'GastaldiGomme - Reminder';

            // destinatari
            var recipients = [];
            // recipients.push(cart.email);
            recipients.push('fstefano@moxsolutions.it');

            // allegati.. in teoria no
            var attachmentsList = null;

            var notify = new mailnotifier();
            notify.send(data, subject, recipients, mailTemplate, attachmentsList, function(data) {
              grunt.log.writeln('Cart notification e-mail sent to user ' + cart.username + '.');
              cartDone();
            });
					}, function(aborted) {
						jobDone();
					});
        });
    	});

    	// end job
    	jobs.push(function(jobDone) {
				moxutils.asyncForEach(carts, function(cart, cartDone) {
      		sequelize.query("select * from gastaldi.scheduler_cart_alert_finalize(" + cart.document_id + ");")
      		.then(function(defaultDatas) {
						grunt.log.writeln('Cart ' + cart.document_id + ' successfully notified.');

						cartDone();
					});
				}, function(){
					grunt.log.writeln('Alert job done'); 
					jobDone();
				});
    	});

										
    	moxutils.asyncForEach(jobs, function(job, jobDone){
    		job(jobDone);
    	}, function(aborted){
    		done();
    	});     
      
    })
		.catch(function(err){
      grunt.log.writeln('Unable to connect to the database.');
      done();
    });


    var done = this.async();
  });
};
